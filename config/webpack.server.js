/**
 * @author: @AngularClass
 */
const helpers = require('./helpers');
const webpackMerge = require('webpack-merge'); // used to merge webpack configs
const webpackMergeDll = webpackMerge.strategy({
    plugins: 'replace'
});
const path = require("path")
const fs = require("fs")
const commonConfig = require('./webpack.common.js'); // the settings that are common to prod and dev
const webpack = require('webpack');

/**
 * Webpack Plugins
 */
const AddAssetHtmlPlugin = require('add-asset-html-webpack-plugin');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const NamedModulesPlugin = require('webpack/lib/NamedModulesPlugin');
const LoaderOptionsPlugin = require('webpack/lib/LoaderOptionsPlugin');

/**
 * Webpack Constants
 */
const ENV = process.env.ENV = process.env.NODE_ENV = 'development';
const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 3000;
const HMR = helpers.hasProcessFlag('hot');
const AOT = helpers.hasNpmFlag('aot');
const METADATA = webpackMerge(commonConfig({
    env: ENV
}).metadata, {
    host: HOST,
    port: PORT,
    ENV: ENV,
    HMR: HMR
});

// Default config
var defaultConfig = {
  // context: __dirname,
  // resolve: {
  //   modules: helpers.root('src')
  // },
  // output: {
  //   publicPath: path.resolve(__dirname),
  //   filename: 'index.js'
  // }
};

/**
 * Webpack configuration
 *
 * See: http://webpack.github.io/docs/configuration.html#cli
 */
module.exports = function(options) {
    console.log(helpers.root('src'));
    return webpackMerge(defaultConfig,  {
        // Application entry point
        entry: {
            'server': helpers.root('src/server')
        },

        // We build for node
        target: "node",

        // Node module dependencies should not be bundled
        externals: fs.readdirSync("node_modules")
            .reduce(function(acc, mod) {
                if (mod === ".bin") {
                    return acc
                }

                acc[mod] = "commonjs " + mod
                return acc
            }, {}),

        // We are outputting a real node app!
        node: {
            console: false,
            global: false,
            process: false,
            Buffer: false,
            __filename: false,
            __dirname: false,
        },

        // Output files in the build/ folder
        output: {
            path: helpers.root('dist'),
            filename: "[name].bundle.js",
        },

        module: {
            loaders: [
                // TypeScript
                {
                    test: /\.ts$/,
                    loaders: ['ts-loader', 'angular2-template-loader']
                }, {
                    test: /\.html$/,
                    loader: 'raw-loader'
                }, {
                    test: /\.css$/,
                    loader: 'raw-loader'
                }, {
                    test: /\.json$/,
                    loader: 'json-loader'
                }, {
                    test: /\.scss$/,
                    exclude: /node_modules/,
                    loaders: ['raw-loader', 'sass-loader']
                }
            ],
        },
            /*
     * Options affecting the resolving of modules.
     *
     * See: http://webpack.github.io/docs/configuration.html#resolve
     */
    resolve: {

      /*
       * An array of extensions that should be used to resolve modules.
       *
       * See: http://webpack.github.io/docs/configuration.html#resolve-extensions
       */
      extensions: ['.ts', '.js', '.json'],

      // An array of directory names to be resolved to the current directory
      modules: [helpers.root('src'), helpers.root('node_modules')],

    },
    stats: {
            colors: true,
            modules: true,
            reasons: true,
            errorDetails: true
          }

    })
}
