/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 32);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("jsonwebtoken");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// get an instance of mongoose and mongoose.Schema
var mongoose = __webpack_require__(0);
var Schema = mongoose.Schema;
var bcrypt = __webpack_require__(33), SALT_WORK_FACTOR = 10;
var User = new mongoose.Schema({
    _id: String,
    credentials: {
        username: String,
        password: String,
        email: String,
        admin: Boolean,
    },
    personal: {
        firstName: String,
        lastName: String,
        city: String,
        country: String,
        interests: [String]
    },
    account: {
        depositAccount: String,
        refId: { type: String, ref: 'users' },
        fundsAvailable: [{ type: String, ref: 'funds' }],
        credits: { type: Number, default: 0 },
    },
    filterPrefs: {
        Location: {
            lat: Number,
            long: Number
        }
    },
    events: [{ type: String, ref: 'event' }],
    favorites: [{ type: String, ref: 'event' }]
});
User.pre('save', function (next) {
    var user = this;
    // only hash the password if it has been modified (or is new)
    if (!user.isModified('credentials.password'))
        return next();
    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err)
            return next(err);
        // hash the password using our new salt
        bcrypt.hash(user.credentials.password, salt, function (err, hash) {
            if (err)
                return next(err);
            // override the cleartext password with the hashed one
            user.credentials.password = hash;
            next();
        });
    });
});
User.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.credentials.password, function (err, isMatch) {
        if (err)
            return cb(err);
        cb(null, isMatch);
    });
};
exports.default = mongoose.model('users', User);


/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("bluebird");

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var model_eventTicket_1 = __webpack_require__(8);
var _ = __webpack_require__(3);
// get an instance of mongoose and mongoose.Schema
var mongoose = __webpack_require__(0);
var Promise = __webpack_require__(6);
var Schema = mongoose.Schema;
var Event = new mongoose.Schema({
    _id: String,
    owner: String,
    status: String,
    basicDetails: {
        name: String,
        description: String,
        venue: String,
        image: String,
        thumbnail: String,
        category: String,
        startDate: String,
        endDate: String,
        startTime: String,
        endTime: String
    },
    location: {
        address: String,
        city: String,
        province: String,
        geo: {
            type: { type: String },
            coordinates: [Number],
        },
        latitude: Number,
        longitude: Number,
        postal: String,
        country: String
    },
    organizer: [{ type: String, ref: 'promoter' }],
    tickets: [{ type: String, ref: 'eventTickets' }],
    attendees: [Object],
    ticketsSold: Boolean,
    earnings: { type: Number, default: 0 },
});
Event.index({ 'basicDetails.name': 'text', 'basicDetails.description': 'text', 'basicDetails.venue': 'text', 'basicDetails.category': 'text', 'basicDetails.startTime': 'text', 'location.address': 'text', 'location.city': 'text', 'location.province': 'text', 'location.country': 'text' }, { 'name': 'eventIndex' });
Event.index({ 'location.geo': '2dsphere' });
Event.methods.updateTickets = function (tickets, cb) {
    var pro = [];
    function onInsert(err, docs) {
        if (err) {
            console.log(err);
        }
        else {
            console.info('%d potatoes were successfully stored.', docs.length);
            cb();
        }
    }
    console.log('t', this);
    console.log('tickets', tickets);
    console.log('et', model_eventTicket_1.default);
    //eventTicket.insertMany(tickets, onInsert).upsert(true);
    _.forEach(tickets, function (o, i) {
        var ticket = model_eventTicket_1.default.findOneAndUpdate({ '_id': o._id }, o, { upsert: true, new: true }, function (err, doc) {
            if (err)
                console.log(err);
            //res.json({ success: true, doc:doc });
        });
        pro.push(ticket);
    });
    Promise.all(pro).then(function () {
        cb();
    });
};
exports.default = mongoose.model('event', Event);


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// get an instance of mongoose and mongoose.Schema
var mongoose = __webpack_require__(0);
var Schema = mongoose.Schema;
var eventTicket = new mongoose.Schema({
    event_Id: String,
    cost: Number,
    description: String,
    qty: Number,
    currency: String,
    cat: String,
    _id: String,
    available: Number,
    sold: Number,
    active: { type: Boolean, default: true },
});
exports.default = mongoose.model('eventTickets', eventTicket);


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var moment = __webpack_require__(4);
var mongoose = __webpack_require__(0);
var Schema = mongoose.Schema;
var Transaction = new mongoose.Schema({
    transaction_type: String,
    client: { type: String, ref: 'users' },
    owner: { type: String, ref: 'users' },
    eventId: { type: String, ref: 'event' },
    ticketId: { type: String, ref: 'eventTickets' },
    creditsGiven: Number,
    funds: {
        p2p: Number,
        user: Number
    },
    date: { type: String, default: moment(new Date).format('MM-DD-YYYY') }
});
exports.default = mongoose.model('transaction', Transaction);


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var nodemailer = __webpack_require__(11);
var generator_1 = __webpack_require__(31);
var PartiMailer = (function () {
    function PartiMailer(to, type, details) {
        var _this = this;
        this.to = to;
        this.type = type;
        this.details = details;
        this.mailOptions = {
            from: '"p2p" <p2p@p2p.com>',
            to: this.to,
            subject: '',
            html: '' // html body
        };
        this.mailGenerator = new generator_1.MailGenerator();
        // create reusable transporter object using the default SMTP transport
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'norris@norrisfrancis.ca',
                pass: 'bigups911'
            }
        });
        this.setMailOptions = function (type) {
        };
        this.sendMail = function () {
            _this.transporter.sendMail(_this.mailOptions, function (error, info) {
                if (error) {
                    return console.log(error);
                }
                console.log('Message %s sent: %s', info.messageId, info.response);
            });
        };
        this.init = function () {
            if (_this.type === 'userInvite') {
                _this.mailOptions.html = _this.mailGenerator.generateInvite('Hello', 'this is an invitiation', 'Sign Up Now', 'http://localhost:8080?ref=' + _this.details.uid);
                _this.mailOptions.subject = 'P2P Inv';
                _this.sendMail();
            }
            if (_this.type === 'contact') {
                _this.mailOptions.html = _this.mailGenerator.generateContact(_this.details);
                _this.mailOptions.subject = 'P2P Contact Form';
                _this.mailOptions.from = '"' + _this.details.name + '" <' + _this.details.email + '>';
                _this.mailOptions.to = 'norris@norrisfrancis.ca';
                _this.sendMail();
            }
        };
        this.to = to;
        this.type = type;
        this.details = details;
        this.init();
        console.log(this);
    }
    return PartiMailer;
}());
exports.PartiMailer = PartiMailer;


/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("nodemailer");

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var express = __webpack_require__(1);
/*Models*/
var model_user_1 = __webpack_require__(5);
/*Routes*/
var route_user_1 = __webpack_require__(29);
var route_users_1 = __webpack_require__(30);
var route_events_1 = __webpack_require__(21);
var route_event_1 = __webpack_require__(20);
var route_tickets_1 = __webpack_require__(26);
var route_contact_1 = __webpack_require__(19);
var route_promoter_1 = __webpack_require__(24);
var router = express.Router();
exports.router = router;
var jwt = __webpack_require__(2); // used to create, sign, and verify tokens
router.post('/authenticate', function (req, res) {
    console.log(req.body.email);
    // find the user
    model_user_1.default.findOne({
        "credentials.username": req.body.email
    }, function (err, user) {
        if (err)
            throw err;
        if (!user) {
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        }
        else if (user) {
            // check if password matches
            user.comparePassword(req.body.password, function (err, isMatch) {
                if (err)
                    res.json({ success: false, message: 'Authentication failed. Wrong password.' });
                console.log(req.body.password, isMatch); // -> Password123: true
                // if user is found and password is right
                // create a token
                var token = jwt.sign(user, 'robblekabobblehuh', {
                    expiresIn: 360 // expires in 6 hours
                });
                // return the information including token as JSON
                res.json({
                    success: true,
                    message: 'Enjoy your token!',
                    token: token,
                    user: {
                        id: user._id,
                        refId: user.account.refId
                    }
                });
            });
        }
    });
});
// router.use(function(req, res, next) {
//   // check header or url parameters or post parameters for token
//   var token = req.body.token || req.query.token || req.headers['x-access-token'];
//   // decode token
//   if (token) {
//     // verifies secret and checks exp
//     jwt.verify(token,'robblekabobblehuh', function(err, decoded) {
//       if (err) {
//         return res.json({ success: false, message: 'Failed to authenticate token.' });
//       } else {
//         // if everything is good, save to request for use in other routes
//         req['decoded'] = decoded;
//         next();
//       }
//     });
//   } else {
//     // if there is no token
//     // return an error
//     return res.status(403).send({
//         success: false,
//         message: 'No token provided.'
//     });
//   }
// });
router.use('/user', route_user_1.router);
router.use('/users', route_users_1.router);
router.use('/events', route_events_1.router);
router.use('/event', route_event_1.router);
router.use('/tickets', route_tickets_1.router);
router.use('/contact', route_contact_1.router);
router.use('/promoter', route_promoter_1.router);


/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("compression");

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("cors");

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = require("express-fileupload");

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = require("multer");

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var express = __webpack_require__(1);
var mailer_1 = __webpack_require__(10);
var router = express.Router();
exports.router = router;
router.post('/', function (req, res) {
    var mailer = new mailer_1.PartiMailer('', 'contact', req.body.data);
    res.json({ success: req.body });
});


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var express = __webpack_require__(1);
var model_event_1 = __webpack_require__(7);
var router = express.Router();
exports.router = router;
var jwt = __webpack_require__(2); // used to create, sign, and verify tokens
router.post('/', function (req, res) {
    model_event_1.default.findOne({ '_id': req.body.id }, function (err, event) {
        res.json(event);
    }).populate('tickets');
});
//Create an Event
router.post('/create', function (req, res) {
    model_event_1.default.findOneAndUpdate({ '_id': req.body._id }, req.body, { upsert: true, new: true }, function (err, doc) {
        if (err)
            return res.send(500, { error: err });
        doc.updateTickets(req.body.tickets, function () {
            res.json({ success: true, doc: doc });
        });
        //res.json({ success: true, doc:doc });
    });
});
//Create an Event
router.post('/remove', function (req, res) {
    model_event_1.default.findOneAndRemove({ '_id': req.body._id }, function (err, doc) {
        if (err)
            return res.send(500, { error: err });
        res.json({ success: true });
    });
});
router.post('/ticket/remove', function (req, res) {
    model_event_1.default.findOneAndUpdate({ '_id': req.body._id }, req.body, { 'active': false }, function (err, doc) {
        if (err)
            return res.send(500, { error: err });
    });
});
router.route('/update/:id')
    .post(function (req, res) {
    console.log('id is', req.body.params);
    res.json({ success: true });
});


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var express = __webpack_require__(1);
var _ = __webpack_require__(3);
var model_event_1 = __webpack_require__(7);
var router = express.Router();
exports.router = router;
var jwt = __webpack_require__(2); // used to create, sign, and verify tokens
router.post('/', function (req, res, next) {
    console.log(req.body);
    var query = {};
    var options = {};
    var limit = req.body.limit || 0;
    var skip = req.body.offset || 0;
    var keywords = req.body.filters.keywords || undefined;
    var category = req.body.filters.category || undefined;
    var price = req.body.filters.price || undefined;
    var geo = req.body.filters.geo || undefined;
    query.status = 'published';
    if (!_.isUndefined(keywords) && keywords !== '') {
        if (!_.isUndefined(geo) && typeof geo === 'string') {
            keywords += ' ' + geo;
        }
        if (!_.isUndefined(geo) && typeof geo !== 'string') {
            query['$or'] = [
                { 'basicDetails.name': { "$regex": keywords, "$options": 'i' } },
                { 'basicDetails.venue': { "$regex": keywords, "$options": 'i' } },
                { 'basicDetails.description': { "$regex": keywords, "$options": 'i' } },
                { 'basicDetails.category': { "$regex": keywords, "$options": 'i' } },
                { 'basicDetails.startTime': { "$regex": keywords, "$options": 'i' } },
                { 'location.city': { "$regex": keywords, "$options": 'i' } },
                { 'location.province': { "$regex": keywords, "$options": 'i' } },
                { 'location.country': { "$regex": keywords, "$options": 'i' } }
            ];
        }
        else {
            query['$text'] = { '$search': keywords, '$caseSensitive': false };
        }
    }
    if (!_.isUndefined(category && category !== '')) {
        query['basicDetails.category'] = category;
    }
    if (!_.isUndefined(req.body.filters)) {
        if (!_.isUndefined(geo) && typeof geo === 'string') {
            if (!_.isUndefined(query.$text)) {
                query.$text.$search += ' ' + geo;
            }
            else {
                query['$text'] = { '$search': geo, '$caseSensitive': false };
            }
        }
        if (!_.isUndefined(geo) && typeof geo !== 'string') {
            console.log('opt');
            query['location.geo'] = {
                $geoNear: {
                    $geometry: {
                        type: "Point",
                        coordinates: [geo.lng, geo.lat]
                    },
                    $maxDistance: 20000
                }
            };
        }
    }
    //options
    if (!_.isUndefined(req.body.limit)) {
        options['limit'] = req.body.limit;
    }
    if (!_.isUndefined(req.body.offset)) {
        options['skip'] = req.body.offset;
    }
    options['sort'] = { 'basicDetails.startTime': -1 };
    console.log('q', query);
    model_event_1.default.find(query, {}, options, function (err, events) {
        if (err)
            console.log(err);
        model_event_1.default.count(query, function (err, count) {
            if (err) {
                return next(err);
            }
            res.send({ count: count, events: events });
        });
    });
});
router.post('/user', function (req, res) {
    model_event_1.default.find({ 'owner': req.body.id }, function (err, events) {
        res.json(events);
    }).populate('tickets');
});
router.post('/insert', function (req, res) {
    model_event_1.default.find({}, function (err, events) {
        if (err) {
            console.log(err);
        }
        _.forEach(events, function (o, i) {
            model_event_1.default.findOneAndUpdate({ '_id': o._id }, { 'location.geo.type': 'Point', 'location.geo.coordinates': [o.location.longitude, o.location.latitude] }, { upsert: true }, function (err, doc) {
                if (err) {
                    console.log(err);
                }
                //res.json({ success: true, doc:doc });
            });
        });
    });
    // 	let events = [
    //   {
    //     "_id": "32efiiyuf-e6c1-219b-534f-23145",
    //     "__v": 0,
    //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
    //     "status": "published",
    //     "basicDetails": {
    //       "endTime": "2017-02-22T16:00:59.966Z",
    //       "startTime": "2017-02-23T02:00:56.064Z",
    //       "endDate": "2017-03-03T05:00:00.000Z",
    //       "startDate": "2017-03-03T05:00:00.000Z",
    //       "category": "Dances",
    //       "thumbnail": "",
    //       "image": "create-event-small.jpeg",
    //       "venue": "Testing a venue name",
    //       "description": "This is a description for the above event this and that and some more",
    //       "name": "Testing an event and populating the site"
    //     },
    //     "location": {
    //       "country": "Canada",
    //       "longitude": -79.111807,
    //       "latitude": 43.812724,
    //       "postal": "L1W 3M3",
    //       "province": "Ontario",
    //       "city": "Pickering",
    //       "address": "775 Hampton Court"
    //     },
    //     "organizer": "700ce97b-50d9-b44a-e740-4dd257f9b541",
    //     "tickets": [
    //       "47835d12-74e5-1f3c-7f97-b537bf4c4d48"
    //     ],
    //     "ticketsSold": false,
    //     "earnings": 0,
    //     "attendees": []
    //   },
    //   {
    //     "_id": "ed49hjk5-2c0e-4613-d34f-23145",
    //     "__v": 0,
    //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
    //     "status": "published",
    //     "basicDetails": {
    //       "endTime": "2017-02-23T03:00:41.001Z",
    //       "startTime": "2017-02-22T23:00:35.359Z",
    //       "endDate": "2017-03-10T05:00:00.000Z",
    //       "startDate": "2017-03-10T05:00:00.000Z",
    //       "category": "Sports",
    //       "thumbnail": "",
    //       "image": "soccor-stadium.jpeg",
    //       "venue": "Some big soccer stadium",
    //       "description": "This is the second match up between these teams, Team 1 seeks revenge after a big loss during thier last meeting.",
    //       "name": "Team 1 vs Team 2"
    //     },
    //     "location": {
    //       "country": "Canada",
    //       "longitude": -79.38935320000002,
    //       "latitude": 43.6414378,
    //       "postal": "M5V 1J1",
    //       "province": "Ontario",
    //       "city": "Toronto",
    //       "address": "1 Blue Jays Way"
    //     },
    //     "organizer": "262078c2-7052-7a19-baf0-649e374e99df",
    //     "tickets": [
    //       "77756e33-79e5-57c4-7b6b-e46a1ef07c20",
    //       "05fd5d03-996e-fcbf-45b7-01989030ed8f",
    //       "c8cff8bc-9710-4e09-58ec-f35727b33668"
    //     ],
    //     "ticketsSold": false,
    //     "earnings": 0,
    //     "attendees": []
    //   },
    //   {
    //     "_id": "004626ee-940f-ejhkc-034f-23145",
    //     "__v": 0,
    //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
    //     "status": "published",
    //     "basicDetails": {
    //       "endTime": "2017-02-23T02:00:43.939Z",
    //       "startTime": "2017-02-22T22:00:36.730Z",
    //       "endDate": "2017-03-22T04:00:00.000Z",
    //       "startDate": "2017-03-22T04:00:00.000Z",
    //       "category": "Dances",
    //       "thumbnail": "",
    //       "image": "concert-image-2.jpeg",
    //       "venue": "Rileys Pub",
    //       "description": "DJ Something or Another spins hot tracks all night on the ones and twos, be prepared for the best concert ever.",
    //       "name": "DJ Something or Another"
    //     },
    //     "location": {
    //       "country": "Canada",
    //       "longitude": -78.8597891,
    //       "latitude": 43.898611,
    //       "postal": "L1H 1B6",
    //       "province": "Ontario",
    //       "city": "Oshawa",
    //       "address": "104 King Street East"
    //     },
    //     "organizer": "ac07e073-20b0-e0a8-55d3-04fe5a20a763",
    //     "tickets": [
    //       "750a0c2b-86c3-7bfb-b064-97ebeb5923b7"
    //     ],
    //     "ticketsSold": false,
    //     "earnings": 0,
    //     "attendees": []
    //   },
    //     {
    //     "_id": "32ebnm50f-e6c1-219b-534f-23145",
    //     "__v": 0,
    //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
    //     "status": "published",
    //     "basicDetails": {
    //       "endTime": "2017-02-22T16:00:59.966Z",
    //       "startTime": "2017-02-23T02:00:56.064Z",
    //       "endDate": "2017-03-03T05:00:00.000Z",
    //       "startDate": "2017-03-03T05:00:00.000Z",
    //       "category": "Dances",
    //       "thumbnail": "",
    //       "image": "create-event-small.jpeg",
    //       "venue": "Testing a venue name",
    //       "description": "This is a description for the above event this and that and some more",
    //       "name": "Testing an event and populating the site"
    //     },
    //     "location": {
    //       "country": "Canada",
    //       "longitude": -79.111807,
    //       "latitude": 43.812724,
    //       "postal": "L1W 3M3",
    //       "province": "Ontario",
    //       "city": "Pickering",
    //       "address": "775 Hampton Court"
    //     },
    //     "organizer": "700ce97b-50d9-b44a-e740-4dd257f9b541",
    //     "tickets": [
    //       "47835d12-74e5-1f3c-7f97-b537bf4c4d48"
    //     ],
    //     "ticketsSold": false,
    //     "earnings": 0,
    //     "attendees": []
    //   },
    //   {
    //     "_id": "evbna95-2c0e-4613-d34f-23145",
    //     "__v": 0,
    //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
    //     "status": "published",
    //     "basicDetails": {
    //       "endTime": "2017-02-23T03:00:41.001Z",
    //       "startTime": "2017-02-22T23:00:35.359Z",
    //       "endDate": "2017-03-10T05:00:00.000Z",
    //       "startDate": "2017-03-10T05:00:00.000Z",
    //       "category": "Sports",
    //       "thumbnail": "",
    //       "image": "soccor-stadium.jpeg",
    //       "venue": "Some big soccer stadium",
    //       "description": "This is the second match up between these teams, Team 1 seeks revenge after a big loss during thier last meeting.",
    //       "name": "Team 1 vs Team 2"
    //     },
    //     "location": {
    //       "country": "Canada",
    //       "longitude": -79.38935320000002,
    //       "latitude": 43.6414378,
    //       "postal": "M5V 1J1",
    //       "province": "Ontario",
    //       "city": "Toronto",
    //       "address": "1 Blue Jays Way"
    //     },
    //     "organizer": "262078c2-7052-7a19-baf0-649e374e99df",
    //     "tickets": [
    //       "77756e33-79e5-57c4-7b6b-e46a1ef07c20",
    //       "05fd5d03-996e-fcbf-45b7-01989030ed8f",
    //       "c8cff8bc-9710-4e09-58ec-f35727b33668"
    //     ],
    //     "ticketsSold": false,
    //     "earnings": 0,
    //     "attendees": []
    //   },
    //   {
    //     "_id": "004ersdee-940f-e3cc-034f-23145",
    //     "__v": 0,
    //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
    //     "status": "published",
    //     "basicDetails": {
    //       "endTime": "2017-02-23T02:00:43.939Z",
    //       "startTime": "2017-02-22T22:00:36.730Z",
    //       "endDate": "2017-03-22T04:00:00.000Z",
    //       "startDate": "2017-03-22T04:00:00.000Z",
    //       "category": "Dances",
    //       "thumbnail": "",
    //       "image": "concert-image-2.jpeg",
    //       "venue": "Rileys Pub",
    //       "description": "DJ Something or Another spins hot tracks all night on the ones and twos, be prepared for the best concert ever.",
    //       "name": "DJ Something or Another"
    //     },
    //     "location": {
    //       "country": "Canada",
    //       "longitude": -78.8597891,
    //       "latitude": 43.898611,
    //       "postal": "L1H 1B6",
    //       "province": "Ontario",
    //       "city": "Oshawa",
    //       "address": "104 King Street East"
    //     },
    //     "organizer": "ac07e073-20b0-e0a8-55d3-04fe5a20a763",
    //     "tickets": [
    //       "750a0c2b-86c3-7bfb-b064-97ebeb5923b7"
    //     ],
    //     "ticketsSold": false,
    //     "earnings": 0,
    //     "attendees": []
    //   },
    //     {
    //     "_id": "32ef67f-e6c1-219b-534f-23145",
    //     "__v": 0,
    //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
    //     "status": "published",
    //     "basicDetails": {
    //       "endTime": "2017-02-22T16:00:59.966Z",
    //       "startTime": "2017-02-23T02:00:56.064Z",
    //       "endDate": "2017-03-03T05:00:00.000Z",
    //       "startDate": "2017-03-03T05:00:00.000Z",
    //       "category": "Dances",
    //       "thumbnail": "",
    //       "image": "create-event-small.jpeg",
    //       "venue": "Testing a venue name",
    //       "description": "This is a description for the above event this and that and some more",
    //       "name": "Testing an event and populating the site"
    //     },
    //     "location": {
    //       "country": "Canada",
    //       "longitude": -79.111807,
    //       "latitude": 43.812724,
    //       "postal": "L1W 3M3",
    //       "province": "Ontario",
    //       "city": "Pickering",
    //       "address": "775 Hampton Court"
    //     },
    //     "organizer": "700ce97b-50d9-b44a-e740-4dd257f9b541",
    //     "tickets": [
    //       "47835d12-74e5-1f3c-7f97-b537bf4c4d48"
    //     ],
    //     "ticketsSold": false,
    //     "earnings": 0,
    //     "attendees": []
    //   },
    //   {
    //     "_id": "edqwea95-2c0e-4613-d34f-23145",
    //     "__v": 0,
    //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
    //     "status": "published",
    //     "basicDetails": {
    //       "endTime": "2017-02-23T03:00:41.001Z",
    //       "startTime": "2017-02-22T23:00:35.359Z",
    //       "endDate": "2017-03-10T05:00:00.000Z",
    //       "startDate": "2017-03-10T05:00:00.000Z",
    //       "category": "Sports",
    //       "thumbnail": "",
    //       "image": "soccor-stadium.jpeg",
    //       "venue": "Some big soccer stadium",
    //       "description": "This is the second match up between these teams, Team 1 seeks revenge after a big loss during thier last meeting.",
    //       "name": "Team 1 vs Team 2"
    //     },
    //     "location": {
    //       "country": "Canada",
    //       "longitude": -79.38935320000002,
    //       "latitude": 43.6414378,
    //       "postal": "M5V 1J1",
    //       "province": "Ontario",
    //       "city": "Toronto",
    //       "address": "1 Blue Jays Way"
    //     },
    //     "organizer": "262078c2-7052-7a19-baf0-649e374e99df",
    //     "tickets": [
    //       "77756e33-79e5-57c4-7b6b-e46a1ef07c20",
    //       "05fd5d03-996e-fcbf-45b7-01989030ed8f",
    //       "c8cff8bc-9710-4e09-58ec-f35727b33668"
    //     ],
    //     "ticketsSold": false,
    //     "earnings": 0,
    //     "attendees": []
    //   },
    //   {
    //     "_id": "0046oree-940f-e3cc-034f-23145",
    //     "__v": 0,
    //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
    //     "status": "published",
    //     "basicDetails": {
    //       "endTime": "2017-02-23T02:00:43.939Z",
    //       "startTime": "2017-02-22T22:00:36.730Z",
    //       "endDate": "2017-03-22T04:00:00.000Z",
    //       "startDate": "2017-03-22T04:00:00.000Z",
    //       "category": "Dances",
    //       "thumbnail": "",
    //       "image": "concert-image-2.jpeg",
    //       "venue": "Rileys Pub",
    //       "description": "DJ Something or Another spins hot tracks all night on the ones and twos, be prepared for the best concert ever.",
    //       "name": "DJ Something or Another"
    //     },
    //     "location": {
    //       "country": "Canada",
    //       "longitude": -78.8597891,
    //       "latitude": 43.898611,
    //       "postal": "L1H 1B6",
    //       "province": "Ontario",
    //       "city": "Oshawa",
    //       "address": "104 King Street East"
    //     },
    //     "organizer": "ac07e073-20b0-e0a8-55d3-04fe5a20a763",
    //     "tickets": [
    //       "750a0c2b-86c3-7bfb-b064-97ebeb5923b7"
    //     ],
    //     "ticketsSold": false,
    //     "earnings": 0,
    //     "attendees": []
    //   },  {
    //     "_id": "32effgh0f-e6c1-219b-534f-23145",
    //     "__v": 0,
    //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
    //     "status": "published",
    //     "basicDetails": {
    //       "endTime": "2017-02-22T16:00:59.966Z",
    //       "startTime": "2017-02-23T02:00:56.064Z",
    //       "endDate": "2017-03-03T05:00:00.000Z",
    //       "startDate": "2017-03-03T05:00:00.000Z",
    //       "category": "Dances",
    //       "thumbnail": "",
    //       "image": "create-event-small.jpeg",
    //       "venue": "Testing a venue name",
    //       "description": "This is a description for the above event this and that and some more",
    //       "name": "Testing an event and populating the site"
    //     },
    //     "location": {
    //       "country": "Canada",
    //       "longitude": -79.111807,
    //       "latitude": 43.812724,
    //       "postal": "L1W 3M3",
    //       "province": "Ontario",
    //       "city": "Pickering",
    //       "address": "775 Hampton Court"
    //     },
    //     "organizer": "700ce97b-50d9-b44a-e740-4dd257f9b541",
    //     "tickets": [
    //       "47835d12-74e5-1f3c-7f97-b537bf4c4d48"
    //     ],
    //     "ticketsSold": false,
    //     "earnings": 0,
    //     "attendees": []
    //   },
    //   {
    //     "_id": "ed49csdf5-2c0e-4613-d34f-23145",
    //     "__v": 0,
    //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
    //     "status": "published",
    //     "basicDetails": {
    //       "endTime": "2017-02-23T03:00:41.001Z",
    //       "startTime": "2017-02-22T23:00:35.359Z",
    //       "endDate": "2017-03-10T05:00:00.000Z",
    //       "startDate": "2017-03-10T05:00:00.000Z",
    //       "category": "Sports",
    //       "thumbnail": "",
    //       "image": "soccor-stadium.jpeg",
    //       "venue": "Some big soccer stadium",
    //       "description": "This is the second match up between these teams, Team 1 seeks revenge after a big loss during thier last meeting.",
    //       "name": "Team 1 vs Team 2"
    //     },
    //     "location": {
    //       "country": "Canada",
    //       "longitude": -79.38935320000002,
    //       "latitude": 43.6414378,
    //       "postal": "M5V 1J1",
    //       "province": "Ontario",
    //       "city": "Toronto",
    //       "address": "1 Blue Jays Way"
    //     },
    //     "organizer": "262078c2-7052-7a19-baf0-649e374e99df",
    //     "tickets": [
    //       "77756e33-79e5-57c4-7b6b-e46a1ef07c20",
    //       "05fd5d03-996e-fcbf-45b7-01989030ed8f",
    //       "c8cff8bc-9710-4e09-58ec-f35727b33668"
    //     ],
    //     "ticketsSold": false,
    //     "earnings": 0,
    //     "attendees": []
    //   },
    //   {
    //     "_id": "004626ee-9sdff-e3cc-034f-23145",
    //     "__v": 0,
    //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
    //     "status": "published",
    //     "basicDetails": {
    //       "endTime": "2017-02-23T02:00:43.939Z",
    //       "startTime": "2017-02-22T22:00:36.730Z",
    //       "endDate": "2017-03-22T04:00:00.000Z",
    //       "startDate": "2017-03-22T04:00:00.000Z",
    //       "category": "Dances",
    //       "thumbnail": "",
    //       "image": "concert-image-2.jpeg",
    //       "venue": "Rileys Pub",
    //       "description": "DJ Something or Another spins hot tracks all night on the ones and twos, be prepared for the best concert ever.",
    //       "name": "DJ Something or Another"
    //     },
    //     "location": {
    //       "country": "Canada",
    //       "longitude": -78.8597891,
    //       "latitude": 43.898611,
    //       "postal": "L1H 1B6",
    //       "province": "Ontario",
    //       "city": "Oshawa",
    //       "address": "104 King Street East"
    //     },
    //     "organizer": "ac07e073-20b0-e0a8-55d3-04fe5a20a763",
    //     "tickets": [
    //       "750a0c2b-86c3-7bfb-b064-97ebeb5923b7"
    //     ],
    //     "ticketsSold": false,
    //     "earnings": 0,
    //     "attendees": []
    //   }
    // ];
    // Event.insertMany(events);
});


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var moment = __webpack_require__(4);
var mongoose = __webpack_require__(0);
var Schema = mongoose.Schema;
var Comment = new mongoose.Schema({
    promoterId: String,
    name: String,
    email: String,
    message: String,
    date: { type: String, default: moment(new Date).format('MM-DD-YYYY') }
});
exports.default = mongoose.model('comment', Comment);


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var moment = __webpack_require__(4);
var mongoose = __webpack_require__(0);
var Schema = mongoose.Schema;
var Rating = new mongoose.Schema({
    promoterId: String,
    userId: String,
    rating: Number,
    date: { type: String, default: moment(new Date).format('MM-DD-YYYY') }
});
exports.default = mongoose.model('rating', Rating);


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var express = __webpack_require__(1);
var _ = __webpack_require__(3);
var model_comment_1 = __webpack_require__(22);
var model_rating_1 = __webpack_require__(23);
var router = express.Router();
exports.router = router;
router.post('/comments', function (req, res, next) {
    var limit = req.body.limit || 0;
    var offset = req.body.offset || 0;
    model_comment_1.default.find({ 'promoterId': req.body.promoterId }, {}, { limit: limit, skip: offset, sort: { date: -1 } }, function (err, comments) {
        if (err) {
            return next(err);
        }
        model_comment_1.default.count({}, function (err, count) {
            if (err) {
                return next(err);
            }
            res.send({ count: count, comments: comments });
        });
    });
});
router.post('/comments/add', function (req, res) {
    var comment = new model_comment_1.default(req.body.data);
    comment.save(function (err) {
        if (err)
            res.json({ success: false, error: 'Something went wrong. Please contact a system administrator' });
        res.json({ success: true });
    });
});
router.post('/ratings', function (req, res) {
    console.log(req.body);
    model_rating_1.default.find({ 'promoterId': req.body.promoterId }, function (err, ratings) {
        if (err)
            return res.send(500, { error: err });
        var rating = 0;
        console.log('rate', ratings);
        if (!_.isEmpty(ratings)) {
            ratings.map(function (x) {
                rating += x.rating;
            });
            rating = rating / ratings.length;
        }
        res.json({ success: true, rating: rating });
    });
});
router.post('/rate', function (req, res) {
    model_rating_1.default.findOneAndUpdate({ 'promoterId': req.body.promoterId, userId: req.body.userId }, req.body, { upsert: true }, function (err, doc) {
        if (err)
            return res.send(500, { error: err });
        res.json({ success: true, doc: doc });
    });
});


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// get an instance of mongoose and mongoose.Schema
var moment = __webpack_require__(4);
var mongoose = __webpack_require__(0);
var Schema = mongoose.Schema;
var TicketModel = new mongoose.Schema({
    client: [{ type: String, ref: 'users' }],
    event: [{ type: String, ref: 'event' }],
    ticket: [{ type: String, ref: 'eventTickets' }],
    qty: Number,
    date: { type: String, default: moment(new Date).format('MM-DD-YYYY') }
});
var Ticket = mongoose.model('tickets', TicketModel);
exports.Ticket = Ticket;


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var express = __webpack_require__(1);
var model_tickets_1 = __webpack_require__(25);
var model_user_1 = __webpack_require__(5);
var model_funds_1 = __webpack_require__(9);
var model_eventTicket_1 = __webpack_require__(8);
var _ = __webpack_require__(3);
var router = express.Router();
exports.router = router;
var jwt = __webpack_require__(2); // used to create, sign, and verify tokens
var Promise = __webpack_require__(6);
router.post('/', function (req, res) {
    var limit = req.body.limit || 0;
    model_tickets_1.Ticket.find({ 'client': req.body.uid }, {}, { limit: limit }, function (err, tickets) {
        res.json({ sucess: true, tickets: tickets });
    }).populate('event').populate('ticket');
});
router.post('/purchased', function (req, res) {
    model_tickets_1.Ticket.find({ 'event': req.body.eventId, }, function (err, tickets) {
        res.json({ sucess: true, tickets: tickets });
    }).populate('event').populate('ticket');
});
router.post('/add', function (req, res) {
    var tickets = [];
    var funds = [];
    var qty = req.body.qty;
    var errors = [];
    console.log(typeof qty);
    var calcCredits = function (cost) {
        var b = (cost / 100) * 2;
        return b;
    };
    var calcFunds = function (cost) {
        return {
            p2p: (cost / 100) * 5,
            user: cost - (cost / 100) * 2
        };
    };
    for (var i = 0; i < qty; i++) {
        var ticket = new model_tickets_1.Ticket(req.body);
        var tickett = ticket.save(function (err) {
            if (err)
                errors.push({ 'ticket': err });
        });
        tickets.push(tickett);
        var transaction = new model_funds_1.default({
            transaction_type: 'Deposit',
            client: req.body.client,
            owner: req.body.owner,
            eventId: req.body.event,
            ticketId: req.body.ticket,
            creditsGiven: calcCredits(req.body.cost),
            funds: calcFunds(req.body.cost)
        });
        var transactions = transaction.save(function (err) {
            if (err)
                errors.push({ 'transaction': err });
        });
        tickets.push(transactions);
    }
    var updateTicket = model_eventTicket_1.default.update({ '_id': req.body.ticket }, { $inc: { 'available': -Math.abs(req.body.qty), sold: Math.abs(req.body.qty) } }, function (err, doc) {
        if (err)
            errors.push({ 'tUpdate': err });
    });
    tickets.push(updateTicket);
    var updateUser = model_user_1.default.update({ '_id': req.body.client }, { $inc: { 'account.credits': Math.abs(calcCredits(req.body.cost * req.body.qty)) } }, function (err, doc) {
        if (err)
            errors.push({ 'user': err });
    });
    tickets.push(updateUser);
    Promise.all(tickets).then(function () {
        if (!_.isEmpty(errors)) {
            res.json({ sucess: false, err: errors });
        }
        else {
            res.json({ sucess: true });
        }
    });
});
router.post('/test', function (req, res) {
    var ref = req.body.ref;
    var cost = req.body.cost;
    model_user_1.default.find({ '_id': req.body.ref }, function (err, user) {
    });
});


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// get an instance of mongoose and mongoose.Schema
var mongoose = __webpack_require__(0);
var Schema = mongoose.Schema;
var Notification = new mongoose.Schema({
    _id: String,
    recipient_id: String,
    sender_id: String,
    conversation: Boolean,
    conversation_id: String,
    messages: String,
    created: String,
    last_updated: String,
    type: String,
    read: Boolean
});
exports.default = mongoose.model('notification', Notification);


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// get an instance of mongoose and mongoose.Schema
var mongoose = __webpack_require__(0);
var Schema = mongoose.Schema;
var Promoter = new mongoose.Schema({
    _id: String,
    first: String,
    owner: String,
    last: String,
    email: String,
    description: String,
    phone: String,
    organization: String,
    affiliation: String,
    socialFb: String,
    socialTw: String,
    socialLi: String,
    socialG: String,
    socialIns: String
});
exports.default = mongoose.model('promoter', Promoter);


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var express = __webpack_require__(1);
var _ = __webpack_require__(3);
var moment = __webpack_require__(4);
var router = express.Router();
exports.router = router;
var jwt = __webpack_require__(2); // used to create, sign, and verify tokens
var model_user_1 = __webpack_require__(5);
var model_funds_1 = __webpack_require__(9);
var model_notification_1 = __webpack_require__(27);
var Promise = __webpack_require__(6);
var mailer_1 = __webpack_require__(10);
router.post('/', function (req, res) {
    model_user_1.default.find({ "credentials.username": req.body.email }, function (err, user) {
        res.json(user);
    });
});
router.post('/details', function (req, res) {
    console.log(req.body);
    model_user_1.default.findOne({ '_id': req.body.uid }, req.body.details, function (err, docs) {
        console.log(docs);
        res.json(docs);
    });
});
router.post('/details/update', function (req, res) {
    console.log(req.body);
    model_user_1.default.findOneAndUpdate({ '_id': req.body.uid }, req.body.details, function (err, doc) {
        if (err)
            return res.json({ success: false, error: 'Unable to update details. Please try again later.' });
        res.json({ success: true });
    });
});
router.post('/password', function (req, res) {
    model_user_1.default.findOne({
        "_id": req.body.uid
    }, function (err, user) {
        if (err)
            throw err;
        if (!user) {
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        }
        else if (user) {
            // check if password matches
            user.comparePassword(req.body.details.previousPass, function (err, isMatch) {
                if (err)
                    res.json({ success: false, message: 'Authentication failed. Wrong password.' });
                user.credentials.password = req.body.details.newPass;
                user.save(function (err) {
                    if (err)
                        res.json({ success: false, error: 'Something went wrong. Please contact a system administrator' });
                    res.json({ success: true });
                });
            });
        }
    });
});
router.post('/favorites/set', function (req, res) {
    model_user_1.default.findOneAndUpdate({ '_id': req.body.uid }, { $push: { "favorites": req.body.eid } }, { safe: true, upsert: true }, function (err, favorites) {
        if (err)
            res.json({ sucess: false, error: err });
        res.json({ sucess: true, message: 'Event has been saved to favorites' });
    });
});
router.post('/favorites/remove', function (req, res) {
    model_user_1.default.findOneAndUpdate({ '_id': req.body.uid }, { $pull: { "favorites": req.body.eid } }, function (err, favorites) {
        if (err)
            res.json({ sucess: false, error: 'Unable to remove favorite' });
        res.json({ sucess: true, message: 'Saved Item has been removed' });
    });
});
router.post('/favorites/get', function (req, res) {
    console.log(req.body.uid);
    var limit = req.body.limit || 0;
    model_user_1.default.findOne({ "_id": req.body.uid }, {}, { limit: limit }, function (err, favorites) {
        console.log(favorites);
        res.json({ success: true, favorites: favorites.favorites });
    }).populate('favorites');
});
router.post('/notifications', function (req, res) {
    var limit = req.body.limit || 0;
    model_notification_1.default.find({ "recipient_id": req.body.uid }, {}, { limit: limit }, function (err, notifications) {
        res.json({ success: true, notifications: notifications });
    });
});
router.post('/notifications/create', function (req, res) {
    console.log(req.body);
    var note = new model_notification_1.default(req.body.notification);
    //save user
    note.save(function (err) {
        if (err)
            res.json({ success: false, error: 'Something went wrong. Please contact a system administrator' });
    });
});
router.post('/notifications/mark', function (req, res) {
    // Notification.findOneAndUpdate({ "id": req.body.nid }, { $set: { read: true } }, function(err, notification) {
    //     res.json({ success: true, status:'updated' });
    // });
});
router.post('/notifications/delete', function (req, res) {
    // Notification.remove({ "id": req.body.nid }, function(err, notification) {
    //     res.json({ success: true, status:'updated' });
    // });
});
router.post('/funds', function (req, res) {
    var promises = [];
    var funds = {
        available: 0,
        holding: 0,
    };
    var credit = null;
    var funding = model_funds_1.default.find({ "owner": req.body.uid }, 'transaction_type date funds.user eventId', function (err, transactions) {
        _.forEach(transactions, function (o, i) {
            if (moment(o.eventId.basicDetails.endDate).add(7, 'days').format('MM-DD-YYYY') <= moment(new Date).format('MM-DD-YYYY')) {
                funds.available += o.funds.user;
            }
            else {
                funds.holding += o.funds.user;
            }
        });
        funds.holding = parseFloat(funds.holding.toFixed(2));
        funds.available = parseFloat(funds.available.toFixed(2));
    }).populate('eventId', 'basicDetails.endDate');
    promises.push(funding);
    var credits = model_user_1.default.findOne({ "_id": req.body.uid }, 'account.credits', function (err, cred) {
        credit = cred.account.credits;
        console.log(cred);
    });
    promises.push(credits);
    Promise.all(promises).then(function () {
        res.json({ success: true, transactions: funds, credits: credit });
    });
});
router.post('/invite', function (req, res) {
    _.forEach(req.body.emails, function (o, i) {
        var mailer = new mailer_1.PartiMailer(o, 'userInvite', { uid: req.body.uid });
    });
    res.json({ success: req.body });
});


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var express = __webpack_require__(1);
var model_user_1 = __webpack_require__(5);
var model_promoter_1 = __webpack_require__(28);
var router = express.Router();
exports.router = router;
var jwt = __webpack_require__(2); // used to create, sign, and verify tokens
router.post('/create/', function (req, res) {
    // create new user from request
    var user = new model_user_1.default(req.body);
    //save user
    user.save(function (err) {
        if (err)
            res.json({ success: false, error: 'Something went wrong. Please contact a system administrator' });
        //set token
        var token = jwt.sign(user, 'robblekabobblehuh', {
            expiresIn: 360 // expires in 6 hours
        });
        res.json({ success: true, token: token });
    });
});
//Create a Promoter
router.post('/promoter/', function (req, res) {
    model_promoter_1.default.find({ '_id': req.body.id }, function (err, promoter) {
        if (err)
            return res.json({ success: false, error: 'Unable to retrive promoters' });
        res.json({ success: true, promoter: promoter });
    });
});
//Create a Promoter
router.post('/promoters/', function (req, res) {
    model_promoter_1.default.find({ 'owner': req.body.uid }, function (err, promoters) {
        if (err)
            return res.json({ success: false, error: 'Unable to retrive promoters' });
        res.json({ success: true, promoter: promoters });
    });
});
//Create a Promoter
router.post('/promoters/create', function (req, res) {
    model_promoter_1.default.findOneAndUpdate({ '_id': req.body._id }, req.body, { upsert: true }, function (err, doc) {
        if (err)
            return res.json({ success: false, error: 'Unable to create promoter' });
        res.json({ success: true });
    });
});


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var nodemailer = __webpack_require__(11);
var MailGenerator = (function () {
    function MailGenerator() {
        var _this = this;
        this.generateInvite = function (greeting, body, linkText, link) {
            var bodyTemplate = "\n        <p>" + greeting + "</p>\n        <p>" + body + "</p>\n        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"btn btn-primary\">\n          <tbody>\n            <tr>\n              <td align=\"left\">\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                  <tbody>\n                    <tr>\n                      <td> <a href=\"" + link + "\" target=\"_blank\">" + linkText + "</a> </td>\n                    </tr>\n                  </tbody>\n                </table>\n              </td>\n            </tr>\n          </tbody>\n        </table>";
            return _this.returnMailTemplate(bodyTemplate);
        };
        this.generateContact = function (details) {
            console.log('ddddsssss', details);
            var bodyTemplate = "\n        <p>Contact request from " + details.name + "</p>\n        <p>name: " + details.name + "</p>\n        <p>email: " + details.email + "</p>\n        <p>message: " + details.message + "</p>";
            return _this.returnMailTemplate(bodyTemplate);
        };
        this.returnMailTemplate = function (body) {
            var template = "\n                       <!doctype html>\n<html>\n  <head>\n    <meta name=\"viewport\" content=\"width=device-width\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n    <title>Simple Transactional Email</title>\n    <style>\n      /* -------------------------------------\n          GLOBAL RESETS\n      ------------------------------------- */\n      img {\n        border: none;\n        -ms-interpolation-mode: bicubic;\n        max-width: 100%; }\n\n      body {\n        background-color: #f6f6f6;\n        font-family: sans-serif;\n        -webkit-font-smoothing: antialiased;\n        font-size: 14px;\n        line-height: 1.4;\n        margin: 0;\n        padding: 0;\n        -ms-text-size-adjust: 100%;\n        -webkit-text-size-adjust: 100%; }\n\n      table {\n        border-collapse: separate;\n        mso-table-lspace: 0pt;\n        mso-table-rspace: 0pt;\n        width: 100%; }\n        table td {\n          font-family: sans-serif;\n          font-size: 14px;\n          vertical-align: top; }\n\n      /* -------------------------------------\n          BODY & CONTAINER\n      ------------------------------------- */\n\n      .body {\n        background-color: #f6f6f6;\n        width: 100%; }\n\n      /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n      .container {\n        display: block;\n        Margin: 0 auto !important;\n        /* makes it centered */\n        max-width: 580px;\n        padding: 10px;\n        width: 580px; }\n\n      /* This should also be a block element, so that it will fill 100% of the .container */\n      .content {\n        box-sizing: border-box;\n        display: block;\n        Margin: 0 auto;\n        max-width: 580px;\n        padding: 10px; }\n\n      /* -------------------------------------\n          HEADER, FOOTER, MAIN\n      ------------------------------------- */\n      .main {\n        background: #fff;\n        border-radius: 3px;\n        width: 100%; }\n\n      .wrapper {\n        box-sizing: border-box;\n        padding: 20px; }\n\n      .footer {\n        clear: both;\n        padding-top: 10px;\n        text-align: center;\n        width: 100%; }\n        .footer td,\n        .footer p,\n        .footer span,\n        .footer a {\n          color: #999999;\n          font-size: 12px;\n          text-align: center; }\n\n      /* -------------------------------------\n          TYPOGRAPHY\n      ------------------------------------- */\n      h1,\n      h2,\n      h3,\n      h4 {\n        color: #000000;\n        font-family: sans-serif;\n        font-weight: 400;\n        line-height: 1.4;\n        margin: 0;\n        Margin-bottom: 30px; }\n\n      h1 {\n        font-size: 35px;\n        font-weight: 300;\n        text-align: center;\n        text-transform: capitalize; }\n\n      p,\n      ul,\n      ol {\n        font-family: sans-serif;\n        font-size: 14px;\n        font-weight: normal;\n        margin: 0;\n        Margin-bottom: 15px; }\n        p li,\n        ul li,\n        ol li {\n          list-style-position: inside;\n          margin-left: 5px; }\n\n      a {\n        color: #3498db;\n        text-decoration: underline; }\n\n      /* -------------------------------------\n          BUTTONS\n      ------------------------------------- */\n      .btn {\n        box-sizing: border-box;\n        width: 100%; }\n        .btn > tbody > tr > td {\n          padding-bottom: 15px; }\n        .btn table {\n          width: auto; }\n        .btn table td {\n          background-color: #ffffff;\n          border-radius: 5px;\n          text-align: center; }\n        .btn a {\n          background-color: #ffffff;\n          border: solid 1px #3498db;\n          border-radius: 5px;\n          box-sizing: border-box;\n          color: #3498db;\n          cursor: pointer;\n          display: inline-block;\n          font-size: 14px;\n          font-weight: bold;\n          margin: 0;\n          padding: 12px 25px;\n          text-decoration: none;\n          text-transform: capitalize; }\n\n      .btn-primary table td {\n        background-color: #3498db; }\n\n      .btn-primary a {\n        background-color: #3498db;\n        border-color: #3498db;\n        color: #ffffff; }\n\n      /* -------------------------------------\n          OTHER STYLES THAT MIGHT BE USEFUL\n      ------------------------------------- */\n      .last {\n        margin-bottom: 0; }\n\n      .first {\n        margin-top: 0; }\n\n      .align-center {\n        text-align: center; }\n\n      .align-right {\n        text-align: right; }\n\n      .align-left {\n        text-align: left; }\n\n      .clear {\n        clear: both; }\n\n      .mt0 {\n        margin-top: 0; }\n\n      .mb0 {\n        margin-bottom: 0; }\n\n      .preheader {\n        color: transparent;\n        display: none;\n        height: 0;\n        max-height: 0;\n        max-width: 0;\n        opacity: 0;\n        overflow: hidden;\n        mso-hide: all;\n        visibility: hidden;\n        width: 0; }\n\n      .powered-by a {\n        text-decoration: none; }\n\n      hr {\n        border: 0;\n        border-bottom: 1px solid #f6f6f6;\n        Margin: 20px 0; }\n\n      /* -------------------------------------\n          RESPONSIVE AND MOBILE FRIENDLY STYLES\n      ------------------------------------- */\n      @media only screen and (max-width: 620px) {\n        table[class=body] h1 {\n          font-size: 28px !important;\n          margin-bottom: 10px !important; }\n        table[class=body] p,\n        table[class=body] ul,\n        table[class=body] ol,\n        table[class=body] td,\n        table[class=body] span,\n        table[class=body] a {\n          font-size: 16px !important; }\n        table[class=body] .wrapper,\n        table[class=body] .article {\n          padding: 10px !important; }\n        table[class=body] .content {\n          padding: 0 !important; }\n        table[class=body] .container {\n          padding: 0 !important;\n          width: 100% !important; }\n        table[class=body] .main {\n          border-left-width: 0 !important;\n          border-radius: 0 !important;\n          border-right-width: 0 !important; }\n        table[class=body] .btn table {\n          width: 100% !important; }\n        table[class=body] .btn a {\n          width: 100% !important; }\n        table[class=body] .img-responsive {\n          height: auto !important;\n          max-width: 100% !important;\n          width: auto !important; }}\n\n      /* -------------------------------------\n          PRESERVE THESE STYLES IN THE HEAD\n      ------------------------------------- */\n      @media all {\n        .ExternalClass {\n          width: 100%; }\n        .ExternalClass,\n        .ExternalClass p,\n        .ExternalClass span,\n        .ExternalClass font,\n        .ExternalClass td,\n        .ExternalClass div {\n          line-height: 100%; }\n        .apple-link a {\n          color: inherit !important;\n          font-family: inherit !important;\n          font-size: inherit !important;\n          font-weight: inherit !important;\n          line-height: inherit !important;\n          text-decoration: none !important; }\n        .btn-primary table td:hover {\n          background-color: #34495e !important; }\n        .btn-primary a:hover {\n          background-color: #34495e !important;\n          border-color: #34495e !important; } }\n\n    </style>\n  </head>\n  <body class=\"\">\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body\">\n      <tr>\n        <td>&nbsp;</td>\n        <td class=\"container\">\n          <div class=\"content\">\n\n            <!-- START CENTERED WHITE CONTAINER -->\n            <span class=\"preheader\">This is preheader text. Some clients will show this text as a preview.</span>\n            <table class=\"main\">\n\n              <!-- START MAIN CONTENT AREA -->\n              <tr>\n                <td class=\"wrapper\">\n                  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                    <tr>\n                      <td> " +
                body
                + "\n                      </td>\n                    </tr>\n                  </table>\n                </td>\n              </tr>\n\n              <!-- END MAIN CONTENT AREA -->\n              </table>\n\n            <!-- START FOOTER -->\n            <div class=\"footer\">\n              <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                <tr>\n                  <td class=\"content-block\">\n                    <span class=\"apple-link\">Paid2Parti</span>\n                    <br> Don't like these emails? <a href=\"http://localhost:8080/unsubscribe\">Unsubscribe</a>.\n                  </td>\n                </tr>\n              </table>\n            </div>\n\n            <!-- END FOOTER -->\n\n<!-- END CENTERED WHITE CONTAINER --></div>\n        </td>\n        <td>&nbsp;</td>\n      </tr>\n    </table>\n  </body>\n</html>\n";
            return template;
        };
        console.log(this);
    }
    return MailGenerator;
}());
exports.MailGenerator = MailGenerator;


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var path = __webpack_require__(18);
var express = __webpack_require__(1);
var bodyParser = __webpack_require__(13);
var route_api_ts_1 = __webpack_require__(12);
var fileUpload = __webpack_require__(16);
var cors = __webpack_require__(15);
var multer = __webpack_require__(17);
var mongoose = __webpack_require__(0);
var compression = __webpack_require__(14);
var config = {
    "database": "mongodb://localhost:27017/p2p",
    "secret": "robblekabobblehuh"
};
// mongoose.connection.collections['events'].drop( function(err) {
//     console.log('collection dropped');
// });
// enable prod for faster renders
//enableProdMode();
var app = express();
app.use(compression());
app.use(fileUpload());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var ROOT = path.join(path.resolve(__dirname, '..'));
app.use(cors());
app.post('/upload', function (req, res) {
    var sampleFile;
    if (!req.files) {
        res.send('No files were uploaded.');
        return;
    }
    sampleFile = req.files.file;
    console.log('sample', req.files.file);
    sampleFile.mv('./uploads/' + sampleFile.name, function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            res.json({ success: true, image: sampleFile.name });
        }
    });
});
// Serve static files
app.use('', express.static(path.join(__dirname, ''), {
    maxAge: 30
}));
app.use('/assets', express.static(path.join(__dirname, 'assets'), {
    maxAge: 30
}));
app.use('/uploads', express.static(path.join(__dirname, 'uploads'), {
    maxAge: 30
}));
app.use('/bower_components', express.static(path.join(__dirname, 'bower_components'), {
    maxAge: 30
}));
app.use('/dist', express.static(path.join(__dirname, 'dist'), {
    maxAge: 30
}));
app.use('/vendor', express.static(path.join(__dirname, 'vendor'), {
    maxAge: 30
}));
app.use('/api', route_api_ts_1.router);
//import { serverApi } from './backend/api';
// Our API for demos only
// app.get('/data.json', serverApi);
mongoose.connect(config.database); // connect to database
// Catch all other routes and return the index file
app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});
// Server
var server = app.listen(process.env.PORT || 8080, function () {
    console.log("Listening on: http://localhost:" + server.address().port);
});


/***/ }),
/* 33 */
/***/ (function(module, exports) {

module.exports = require("bcrypt");

/***/ })
/******/ ]);