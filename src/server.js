import * as path from 'path';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import { router } from './api/route_api.ts';
var fileUpload = require('express-fileupload');
var cors = require('cors');
var multer = require('multer');
var mongoose = require('mongoose');
var compression = require('compression');
var config = {
    "database": "mongodb://localhost:27017/p2p",
    "secret": "robblekabobblehuh"
};
// mongoose.connection.collections['events'].drop( function(err) {
//     console.log('collection dropped');
// });
// enable prod for faster renders
//enableProdMode();
var app = express();
app.use(compression());
app.use(fileUpload());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var ROOT = path.join(path.resolve(__dirname, '..'));
app.use(cors());
app.post('/upload', function (req, res) {
    var sampleFile;
    if (!req.files) {
        res.send('No files were uploaded.');
        return;
    }
    sampleFile = req.files.file;
    console.log('sample', req.files.file);
    sampleFile.mv('./uploads/' + sampleFile.name, function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            res.json({ success: true, image: sampleFile.name });
        }
    });
});
// Serve static files
app.use('', express.static(path.join(__dirname, ''), {
    maxAge: 30
}));
app.use('/assets', express.static(path.join(__dirname, 'assets'), {
    maxAge: 30
}));
app.use('/uploads', express.static(path.join(__dirname, 'uploads'), {
    maxAge: 30
}));
app.use('/bower_components', express.static(path.join(__dirname, 'bower_components'), {
    maxAge: 30
}));
app.use('/dist', express.static(path.join(__dirname, 'dist'), {
    maxAge: 30
}));
app.use('/vendor', express.static(path.join(__dirname, 'vendor'), {
    maxAge: 30
}));
app.use('/api', router);
//import { serverApi } from './backend/api';
// Our API for demos only
// app.get('/data.json', serverApi);
mongoose.connect(config.database); // connect to database
// Catch all other routes and return the index file
app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});
// Server
var server = app.listen(process.env.PORT || 8080, function () {
    console.log("Listening on: http://localhost:" + server.address().port);
});
//# sourceMappingURL=server.js.map