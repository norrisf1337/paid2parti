import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Menus } from './components/menu/menu.component';
import { Footer } from './components/footer/footer.component';
import { AlertComponent } from './components/alerts/alert.component';
import { UserService } from './components/shared/services/user.service';
import { NotificationService } from './components/shared/services/notification.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import { CookieService } from 'angular2-cookie/core';

var jwt = require('jwt-decode');

@Component({
	selector: 'app',
	templateUrl: './app.template.html',
	encapsulation: ViewEncapsulation.None,
	styleUrls: [
		'app.style.scss'
	],
	entryComponents: [Menus, Footer, AlertComponent]
})
export class App {
	public subscription;
	public home: boolean = false;
	public loading = true;
	constructor(public userService: UserService, public router: Router, public route: ActivatedRoute, public noteService: NotificationService, public CookieService: CookieService) {
		//this.init();

	}

	ngOnInit() {
		let hasRef = this.CookieService.get('ref');
		this.router.events.subscribe((evt) => {
			if (!(evt instanceof NavigationEnd)) {
				return;
			}
			document.body.scrollTop = 0;
		});

		if (localStorage.getItem('currentUser')) {
			let currentUser = JSON.parse(localStorage.getItem('currentUser'));
			if (!_.isUndefined(currentUser.token)) {
				let decoded: any = jwt(currentUser.token);
				if (decoded.exp < new Date().getTime()) {
					console.log('this');
					this.userService.user.uid = currentUser.user.id;
					this.userService.user.isLogged = true;

					// this.noteService.create({
					// 	id: this.userService.generateUid(),
					// 	recipient_id: this.userService.user.uid,
					// 	sender_id: 'p2p',
					// 	conversation: false,
					// 	conversation_id: '',
					// 	messages: 'This is a test message, testing, testing',
					// 	created: moment().format('MMM DD YYYY hh:mm A'),
					// 	last_updated: moment().format('MMM DD YYYY hh:mm A'),
					// 	type: 'General',
					// 	read: false
					// })
					// 	.subscribe(
					// 	data => {
					// 		if (data.success) {

					// 		}
					// 	},
					// 	error => {

					// 	});

				} else {

					this.router.navigate(['/']);
				}
			}
		} else {
			console.log('refs', hasRef);
			this.subscription = this.route
				.queryParams
				.subscribe(params => {
					if (!_.isUndefined(params['ref'])) {
						console.log(params['ref'])
						this.CookieService.put('ref', params['ref']);
						localStorage.setItem('refId', params['ref']);
					}
				});
		}

	}

}
