import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { AboutComponent } from './about';
import { NoContentComponent } from './no-content';

import { DataResolver } from './app.resolver';

// export const ROUTES: Routes = [
//   { path: '',      component: HomeComponent },
//   { path: 'home',  component: HomeComponent },
//   { path: 'about', component: AboutComponent },
//   { path: 'detail', loadChildren: './+detail#DetailModule'},
//   { path: 'barrel', loadChildren: './+barrel#BarrelModule'},
//   { path: '**',    component: NoContentComponent },
// ];

import { AuthGuard } from './components/guards/index';
import { Home } from './components/home/home.component';
import { EventPage } from './components/events/event-page/event.page.component';
import { EventDetailsPage } from './components/events/event-details-page/event.details.page.component';
import { EventRegistrationPage } from './components/events/event-registration-page/event.registration.page.component';
import { EventConfirmationPage } from './components/events/event-confirmation-page/event.confirmation.page.component';
import { PromoterDetailsPage } from './components/promoter/promoter-details-page/promoter.details.page.component';
import { AccountDashboardPage } from './components/account-dashboard/account-dashboard-page/account-dashboard-page.component';
import { EventDashboardPage } from './components/events/event-dashboard-page/event-dashboard-page.component';
import { EventCreatePage } from './components/events/event-create-page/event-create-page.component';
import { EventPromoterPage } from './components/events/event-promoter-page/event-promoter-page.component';
import { ProfileDashboardPage } from './components/profile-dashboard/profile-dashboard-page/profile-dashboard-page.component';
import { TicketsPage } from './components/tickets/tickets-page/tickets-page.component';
import { FavoritesPage } from './components/favorites/favorites-page/favorites-page.component';
import { NotificationsPage } from './components/notifications/notifications-page/notifications-page.component';
import { InterestsPage } from './components/profile-dashboard/interests-page/interests-page.component';
import { DepositsPage } from './components/profile-dashboard/deposits-page/deposits-page.component';
import { PasswordPage } from './components/profile-dashboard/change-password-page/change-password-page.component';
import { PersonalDetailsPage } from './components/profile-dashboard/personal-details-page/personal-details-page.component';
import { HowItWorksPage } from './components/how-it-works/how-it-works-page/how-it-works-page.component';
import { FaqPage } from './components/faq/faq-page/faq-page.component';
import { ContactPage } from './components/contact/contact-page/contact-page.component';
import { CashOutPage } from './components/account-dashboard/cash-out-page/cash-out-page.component';
import { TicketPage } from './components/tickets/ticket-page/ticket-page.component';
import { MyNetworkPage } from './components/my-network/my-network-page/my-network-page.component';

export const ROUTES: Routes = [
      { path: '', component: Home, pathMatch: 'full' },
      { path: 'register', component: Home, pathMatch: 'full' },
      { path: 'events', component: EventPage, pathMatch: 'full' },
      { path: 'events/:id', component: EventPage, pathMatch: 'full' },
      { path: 'event/details/:id', component: EventDetailsPage, pathMatch: 'full' },
      { path: 'event/details/:id/register/:tid/:tqty', component: EventRegistrationPage, pathMatch: 'full' },
      { path: 'event/details/:id/confirmation', component: EventConfirmationPage, pathMatch: 'full' },
      { path: 'promoter/:pid/details', component: PromoterDetailsPage, pathMatch: 'full' },
      { path: 'account/dashboard', component: AccountDashboardPage, pathMatch: 'full'},
      { path: 'account/my-tickets', component: TicketsPage, pathMatch: 'full' },
      { path: 'account/my-tickets/:id', component: TicketPage, pathMatch: 'full' },
      { path: 'account/favorites', component: FavoritesPage, pathMatch: 'full' },
      { path: 'account/notifications', component: NotificationsPage, pathMatch: 'full' },
      { path: 'account/cash-out', component: CashOutPage, pathMatch: 'full' },
      { path: 'my-events/dashboard', component: EventDashboardPage, pathMatch: 'full' },
      { path: 'my-events/create', component: EventCreatePage, pathMatch: 'full' },
      { path: 'my-events/edit/:id', component: EventCreatePage, pathMatch: 'full' },
      { path: 'my-events/attendees/:id', component: EventCreatePage, pathMatch: 'full' },
      { path: 'my-events/notifications/:id', component: NotificationsPage, pathMatch: 'full' },
      { path: 'my-events/promoters', component: EventPromoterPage, pathMatch: 'full' },
      { path: 'profile/dashboard', component: ProfileDashboardPage, pathMatch: 'full' },
      { path: 'profile/interests', component: InterestsPage, pathMatch: 'full' },
      { path: 'profile/deposit-info', component: DepositsPage, pathMatch: 'full' },
      { path: 'profile/change-password', component: PasswordPage, pathMatch: 'full' },
      { path: 'profile/personal-info', component: PersonalDetailsPage, pathMatch: 'full' },
      { path: 'profile/my-network', component: MyNetworkPage, pathMatch: 'full' },
      { path: 'help/how-it-works', component: HowItWorksPage, pathMatch: 'full' },
      { path: 'help/faq', component: FaqPage, pathMatch: 'full' },
      { path: 'help/contact', component: ContactPage, pathMatch: 'full' },
];
