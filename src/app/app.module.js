import * as tslib_1 from "tslib";
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { CookieService, CookieOptions } from 'angular2-cookie/core';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, PreloadAllModules } from '@angular/router';
/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { App } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { MaterialModule } from '@angular/material';
import { Ng2UploaderModule } from 'ng2-uploader';
import { ShareButtonsModule } from "ng2-sharebuttons";
import { MomentModule } from 'angular2-moment';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
import { GooglePlaceModule } from './components/places-two';
//import { QRCodeModule } from 'angular2-qrcode';
import { EventList } from './components/events/event-listing/event.listing.component';
import { CategoriesComponent } from './components/categories/category.component';
import { HowToModule } from './components/how-it-works/how-it-works/howto.module';
import { Header } from './components/header/header.component';
import { EventShare } from './components/events/event-share/event.share.component';
import { EventTicketList } from './components/events/event-tickets/event.ticket.list.component';
import { EventTicketListDashboardsModule } from './components/events/event-tickets-dashboards/event-ticket-list-dashboards.module';
import { EventOrganizer } from './components/events/event-organizer/event.organizer.component';
import { EventBlock } from './components/events/event-block/event.block.component';
import { EventListGrid } from './components/events/event-listing-grid/event.listing.grid.component';
import { EventBlockDashboards } from './components/events/event-block-dashboards/event-block-dashboards.component';
import { EventListGridDashboards } from './components/events/event-listing-grid-dashboards/event-listing-grid-dashboards.component';
import { Comments } from './components/comments/comments/comments.component';
import { CommentsForm } from './components/comments/comments-form/comments-form.component';
import { ContactForm } from './components/contact/contact-form/contact-form.component';
import { FaqItem } from './components/faq/faq-item/faq-item.component';
import { FundsBlock } from './components/account-dashboard/funds-block/funds.block.component';
import { InviteFriends } from './components/invite-friends/invite-friends.component';
import { MyDownline } from './components/my-downline/my-downline.component';
import { Paginator } from './components/paginator/paginator/paginator.component';
import { PaginatorControls } from './components/paginator/paginator-controls/paginator-controls.component';
//Page components
import { Home } from './components/home/home.component';
import { EventPage } from './components/events/event-page/event.page.component';
import { EventDetailsPage } from './components/events/event-details-page/event.details.page.component';
import { EventRegistrationPage } from './components/events/event-registration-page/event.registration.page.component';
import { EventConfirmationPage } from './components/events/event-confirmation-page/event.confirmation.page.component';
import { PromoterDetailsPage } from './components/promoter/promoter-details-page/promoter.details.page.component';
import { AccountDashboardPage } from './components/account-dashboard/account-dashboard-page/account-dashboard-page.component';
import { TicketsDashboard } from './components/account-dashboard/tickets-dashboard-page/tickets-dashboard-page.component';
import { SavedEventDash } from './components/account-dashboard/saved-events-dashboard-page/saved-events-dashboard-page.component';
import { CashOutPage } from './components/account-dashboard/cash-out-page/cash-out-page.component';
import { ProfileDashboardPage } from './components/profile-dashboard/profile-dashboard-page/profile-dashboard-page.component';
import { TicketsPage } from './components/tickets/tickets-page/tickets-page.component';
import { TicketPage } from './components/tickets/ticket-page/ticket-page.component';
import { FavoritesPage } from './components/favorites/favorites-page/favorites-page.component';
import { NotificationsPage } from './components/notifications/notifications-page/notifications-page.component';
import { InterestsPage } from './components/profile-dashboard/interests-page/interests-page.component';
import { DepositsPage } from './components/profile-dashboard/deposits-page/deposits-page.component';
import { PasswordPage } from './components/profile-dashboard/change-password-page/change-password-page.component';
import { PersonalDetailsPage } from './components/profile-dashboard/personal-details-page/personal-details-page.component';
import { HowItWorksPage } from './components/how-it-works/how-it-works-page/how-it-works-page.component';
import { FaqPage } from './components/faq/faq-page/faq-page.component';
import { ContactPage } from './components/contact/contact-page/contact-page.component';
import { EventDashboardPage } from './components/events/event-dashboard-page/event-dashboard-page.component';
import { EventCreatePage } from './components/events/event-create-page/event-create-page.component';
import { EventPromoterPage } from './components/events/event-promoter-page/event-promoter-page.component';
import { MyNetworkPage } from './components/my-network/my-network-page/my-network-page.component';
import { LoginModal } from './components/modals/login-register/login-register.component';
import { AlertComponent, AlertService } from './components/alerts/index';
import { Notifications } from './components/notifications/notifications/notifications.component';
import { Notification } from './components/notifications/notification/notification.component';
import { AuthGuard } from './components/guards/index';
import { AuthenticationService } from './components/authentication/index';
import { UserService } from './components/shared/services/user.service';
import { EventService } from './components/shared/services/events.service';
import { API } from './components/shared/services/api.service';
import { HeaderService } from './components/shared/services/header.service';
import { TicketService } from './components/shared/services/ticket.service';
import { FormService } from './components/shared/services/form.service';
import { NotificationService } from './components/shared/services/notification.service';
import { Emitter } from './components/shared/services/emitter.service';
//Other
import { Menus } from './components/menu/menu.component';
import { Footer } from './components/footer/footer.component';
import { CSSCarouselComponent } from './components/carousel/carousel.component';
import { TruncatePipe } from './components/pipes/truncate';
import '../styles/styles.scss';
import '../styles/headings.css';
// export function loadQR() {
//   return QRCodeModule;
// }
// Application wide providers
var APP_PROVIDERS = APP_RESOLVER_PROVIDERS.concat([
    { provide: CookieOptions, useValue: {} }
]);
// type StoreType = {
//   state: InternalStateType,
//   restoreInputValues: () => void,
//   disposeOldHosts: () => void
// };
/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = tslib_1.__decorate([
    NgModule({
        bootstrap: [App],
        declarations: [
            App, Header, TruncatePipe, LoginModal, Menus, Footer, Home, CategoriesComponent, EventPage, EventList, EventListGrid, EventBlock, EventDetailsPage, EventRegistrationPage, EventConfirmationPage, EventBlockDashboards, EventListGridDashboards, PromoterDetailsPage, AccountDashboardPage, TicketsDashboard, SavedEventDash, ProfileDashboardPage, TicketsPage, FavoritesPage, NotificationsPage, Notification, Notifications, InterestsPage, DepositsPage, PasswordPage, PersonalDetailsPage, HowItWorksPage, FaqPage, ContactPage, CashOutPage, EventDashboardPage, EventCreatePage, EventPromoterPage, AlertComponent, EventOrganizer, CSSCarouselComponent, EventShare, TicketPage, FundsBlock, MyNetworkPage, InviteFriends, MyDownline, FaqItem, ContactForm, Comments, CommentsForm, Paginator, EventTicketList, PaginatorControls
        ],
        imports: [
            BrowserModule,
            BrowserAnimationsModule,
            FormsModule,
            HttpModule,
            ShareButtonsModule.forRoot(),
            ReactiveFormsModule,
            MomentModule,
            CommonModule,
            EventTicketListDashboardsModule,
            HowToModule,
            Ng2UploaderModule,
            GooglePlaceModule,
            RouterModule,
            //loadQR(),
            ModalModule.forRoot(),
            MaterialModule.forRoot(),
            BootstrapModalModule,
            AgmCoreModule.forRoot({
                apiKey: 'AIzaSyBLgf36ocMu2WzervS3o-eaUViWcHIg6uw'
            }),
            NKDatetimeModule,
            RouterModule.forRoot(ROUTES, { useHash: false, preloadingStrategy: PreloadAllModules })
        ],
        providers: [
            ENV_PROVIDERS,
            APP_PROVIDERS,
            AuthGuard,
            AlertService,
            AuthenticationService,
            UserService,
            EventService,
            API,
            HeaderService,
            TicketService,
            FormService,
            NotificationService,
            Emitter,
            CookieService
        ],
        entryComponents: [
            LoginModal
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }),
    tslib_1.__metadata("design:paramtypes", [])
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map