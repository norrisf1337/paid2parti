import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';

@Component({
  selector: 'cash-out',
  entryComponents: [],
  styleUrls: [
    'cash-out-page.style.scss'
  ],
  providers: [],
  templateUrl: 'cash-out-page.template.html'
})
export class CashOutPage {
	constructor (public hs: HeaderService){
		this.hs.resetHeader();
		this.hs.setHeader('single', 'Withdraw Funds', '');
	}
}
