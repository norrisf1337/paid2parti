import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
var CashOutPage = (function () {
    function CashOutPage(hs) {
        this.hs = hs;
        this.hs.resetHeader();
        this.hs.setHeader('single', 'Withdraw Funds', '');
    }
    return CashOutPage;
}());
CashOutPage = tslib_1.__decorate([
    Component({
        selector: 'cash-out',
        entryComponents: [],
        styleUrls: [
            'cash-out-page.style.scss'
        ],
        providers: [],
        templateUrl: 'cash-out-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService])
], CashOutPage);
export { CashOutPage };
//# sourceMappingURL=cash-out-page.component.js.map