import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
var SavedEventDash = (function () {
    function SavedEventDash(route, router) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.cols = false;
        this.viewEvent = function () {
            _this.router.navigate(['/event/details', _this.favorite._id]);
        };
    }
    SavedEventDash.prototype.ngOnInit = function () {
        console.log(this.favorite);
    };
    SavedEventDash.prototype.getBestPrice = function (tickets) {
        return '$25';
    };
    return SavedEventDash;
}());
tslib_1.__decorate([
    Input('favorite'),
    tslib_1.__metadata("design:type", Object)
], SavedEventDash.prototype, "favorite", void 0);
tslib_1.__decorate([
    Input('cols'),
    tslib_1.__metadata("design:type", Object)
], SavedEventDash.prototype, "cols", void 0);
SavedEventDash = tslib_1.__decorate([
    Component({
        selector: 'saved-events-dashboard',
        entryComponents: [],
        styleUrls: [
            'saved-events-dashboard-page.style.scss'
        ],
        providers: [],
        templateUrl: 'saved-events-dashboard-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [ActivatedRoute, Router])
], SavedEventDash);
export { SavedEventDash };
//# sourceMappingURL=saved-events-dashboard-page.component.js.map