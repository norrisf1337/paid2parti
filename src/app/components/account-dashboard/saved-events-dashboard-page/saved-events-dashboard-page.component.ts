import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'saved-events-dashboard',
  entryComponents: [],
  styleUrls: [
    'saved-events-dashboard-page.style.scss'
  ],
  providers: [],
  templateUrl: 'saved-events-dashboard-page.template.html'
})
export class SavedEventDash {
  @Input('favorite') favorite;
  @Input('cols') cols = false;
  public placeholder;
  public event;
  constructor (private route: ActivatedRoute, private router: Router) {

  }
	ngOnInit() {
    console.log(this.favorite);
	}

  getBestPrice(tickets) {
    return '$25';
  }

  public viewEvent = () => {
    this.router.navigate(['/event/details', this.favorite._id]);
  }
}
