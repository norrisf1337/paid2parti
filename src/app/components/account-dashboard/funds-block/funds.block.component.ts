import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../shared/services/user.service';

@Component({
	selector: 'funds-block',
	templateUrl: 'funds.block.template.html',
	styleUrls:[
		'funds.block.style.scss'
	]
})
export class FundsBlock {
	public funds;
	public loaded: boolean = false;
	constructor(public userService: UserService) {

	}

	ngOnInit() {
		this.userService.getFunds()
			.subscribe(
			data => {
				this.funds = {
					available: data.transactions.available,
					holding: data.transactions.holding,
					credits: data.credits
				};
				this.loaded = true;
			},
			error => {
			})
	}
}
