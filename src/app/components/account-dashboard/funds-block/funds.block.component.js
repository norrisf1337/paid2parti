import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
var FundsBlock = (function () {
    function FundsBlock(userService) {
        this.userService = userService;
        this.funds = {
            available: 0,
            holding: 0,
            credits: 0
        };
    }
    FundsBlock.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.getFunds()
            .subscribe(function (data) {
            _this.funds = {
                available: data.transactions.available,
                holding: data.transactions.holding,
                credits: data.credits
            };
        }, function (error) {
        });
    };
    return FundsBlock;
}());
FundsBlock = tslib_1.__decorate([
    Component({
        selector: 'funds-block',
        templateUrl: 'funds.block.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [UserService])
], FundsBlock);
export { FundsBlock };
//# sourceMappingURL=funds.block.component.js.map