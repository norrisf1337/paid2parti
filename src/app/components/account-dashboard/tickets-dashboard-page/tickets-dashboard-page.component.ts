import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'tickets-dashboard',
  entryComponents: [],
  styleUrls: [
    'tickets-dashboard-page.style.scss'
  ],
  providers: [],
  templateUrl: 'tickets-dashboard-page.template.html'
})
export class TicketsDashboard {
  @Input('ticket') ticket;
  @Input('count') count;
  @Input('type') type = false;
  @Input('tempTicket') tempTicket;
	public eventdetails = null;
  constructor(private route: ActivatedRoute, private router: Router){

  }
	ngOnInit() {

	}

  public viewTicket = () => {
    this.router.navigate(['/account/my-tickets', this.ticket.event[0]._id]);
  }

  public viewEvent = () => {
    this.router.navigate(['/event/details', this.ticket.event[0]._id]);
  }
}
