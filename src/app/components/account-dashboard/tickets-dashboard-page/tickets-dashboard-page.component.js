import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
var TicketsDashboard = (function () {
    function TicketsDashboard(route, router) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.type = false;
        this.eventdetails = null;
        this.viewTicket = function () {
            _this.router.navigate(['/account/my-tickets', _this.ticket.event[0]._id]);
        };
        this.viewEvent = function () {
            _this.router.navigate(['/event/details', _this.ticket.event[0]._id]);
        };
    }
    TicketsDashboard.prototype.ngOnInit = function () {
    };
    return TicketsDashboard;
}());
tslib_1.__decorate([
    Input('ticket'),
    tslib_1.__metadata("design:type", Object)
], TicketsDashboard.prototype, "ticket", void 0);
tslib_1.__decorate([
    Input('count'),
    tslib_1.__metadata("design:type", Object)
], TicketsDashboard.prototype, "count", void 0);
tslib_1.__decorate([
    Input('type'),
    tslib_1.__metadata("design:type", Object)
], TicketsDashboard.prototype, "type", void 0);
tslib_1.__decorate([
    Input('tempTicket'),
    tslib_1.__metadata("design:type", Object)
], TicketsDashboard.prototype, "tempTicket", void 0);
TicketsDashboard = tslib_1.__decorate([
    Component({
        selector: 'tickets-dashboard',
        entryComponents: [],
        styleUrls: [
            'tickets-dashboard-page.style.scss'
        ],
        providers: [],
        templateUrl: 'tickets-dashboard-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [ActivatedRoute, Router])
], TicketsDashboard);
export { TicketsDashboard };
//# sourceMappingURL=tickets-dashboard-page.component.js.map