import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TicketService } from '../../shared/services/ticket.service';
import { UserService } from '../../shared/services/user.service';
import { HeaderService } from '../../shared/services/header.service';
import * as _ from 'lodash';
import * as Promise from 'bluebird';
@Component({
	selector: 'account-dashboard',
	entryComponents: [],
	styleUrls: [
		'account-dashboard-page.style.scss'
	],
	providers: [],
	templateUrl: 'account-dashboard-page.template.html'
})
export class AccountDashboardPage {
	public ticketsLoaded: boolean = false;
	public favoritesLoaded: boolean = false;
	public hasData = false;
	public favorites: any = [];
	public user: any;
	public tickets = [];
	public ticketArray;
	constructor(private route: ActivatedRoute, private router: Router, public ticketService: TicketService, public userService: UserService, public hs: HeaderService, private ref: ChangeDetectorRef) {
		this.hs.resetHeader();
		this.hs.setHeader('single', 'Account Management Dashboard', '');
	}

	ngOnInit() {
		 this.ticketService.getTickets()
			.subscribe(
			data => {
				this.tickets = this.ticketService.setTickets(data.tickets);
				this.ticketsLoaded = true;
			},
			error => {
			});


			this.userService.getFavorites(6)
			.subscribe(
			data => {
				this.favorites = data.favorites;
				this.favoritesLoaded = true;
			},
			error => {
			});
	   }

}
