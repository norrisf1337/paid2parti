import * as tslib_1 from "tslib";
import { Component, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TicketService } from '../../shared/services/ticket.service';
import { UserService } from '../../shared/services/user.service';
import { HeaderService } from '../../shared/services/header.service';
var AccountDashboardPage = (function () {
    function AccountDashboardPage(route, router, ticketService, userService, hs, ref) {
        this.route = route;
        this.router = router;
        this.ticketService = ticketService;
        this.userService = userService;
        this.hs = hs;
        this.ref = ref;
        this.hasData = false;
        this.favorites = [];
        this.tickets = [];
        this.hs.resetHeader();
        this.hs.setHeader('single', 'Account Management Dashboard', '');
    }
    AccountDashboardPage.prototype.ngOnInit = function () {
        var _this = this;
        this.ticketService.getTickets()
            .subscribe(function (data) {
            _this.tickets = _this.ticketService.setTickets(data.tickets);
        }, function (error) {
        });
        this.userService.getFavorites(6)
            .subscribe(function (data) {
            _this.favorites = data.favorites;
        }, function (error) {
        });
        this.userService.getDetails('account')
            .subscribe(function (data) {
            _this.user = data.account;
        }, function (error) {
        });
    };
    return AccountDashboardPage;
}());
AccountDashboardPage = tslib_1.__decorate([
    Component({
        selector: 'account-dashboard',
        entryComponents: [],
        styleUrls: [
            'account-dashboard-page.style.scss'
        ],
        providers: [],
        templateUrl: 'account-dashboard-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [ActivatedRoute, Router, TicketService, UserService, HeaderService, ChangeDetectorRef])
], AccountDashboardPage);
export { AccountDashboardPage };
//# sourceMappingURL=account-dashboard-page.component.js.map