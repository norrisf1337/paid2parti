import { Component, ViewContainerRef, ViewEncapsulation, OnInit } from '@angular/core';
import {MdDialog, MdDialogRef} from '@angular/material';
import { LoginModal } from './../modals/login-register/login-register.component';
import { UserService } from '../shared/services/user.service';
import { Emitter } from '../shared/services/emitter.service';

@Component({
  selector: 'menu',
  styleUrls: [
    'menu.style.scss'
  ],
  templateUrl: 'menu.template.html'
})
export class Menus {
  constructor(public modal: MdDialog, public userService: UserService, public emitter: Emitter) {

  }

  ngOnInit() {
    this.emitter.listen('register', (data) => {
      this.openCustom();
    });

  }

  public isLogged = () => {
    return this.userService.isLogged();
  }
  public openCustom = () => {
    return this.modal.open(LoginModal, {
      height: 'auto',
      width: '700px',
    });
  }

  public logout = () => {
    this.userService.logout();
  }
}
