import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MdDialog } from '@angular/material';
import { LoginModal } from './../modals/login-register/login-register.component';
import { UserService } from '../shared/services/user.service';
import { Emitter } from '../shared/services/emitter.service';
var Menus = (function () {
    function Menus(modal, userService, emitter) {
        var _this = this;
        this.modal = modal;
        this.userService = userService;
        this.emitter = emitter;
        this.isLogged = function () {
            return _this.userService.isLogged();
        };
        this.openCustom = function () {
            return _this.modal.open(LoginModal, {
                height: '700px',
                width: '700px',
            });
        };
        this.logout = function () {
            _this.userService.logout();
        };
    }
    Menus.prototype.ngOnInit = function () {
        var _this = this;
        this.emitter.listen('register', function (data) {
            _this.openCustom();
        });
    };
    return Menus;
}());
Menus = tslib_1.__decorate([
    Component({
        selector: 'menu',
        styleUrls: [
            'menu.style.scss'
        ],
        templateUrl: 'menu.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [MdDialog, UserService, Emitter])
], Menus);
export { Menus };
//# sourceMappingURL=menu.component.js.map