import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var CSSCarouselComponent = (function () {
    function CSSCarouselComponent() {
        //images data to be bound to the template
        this.images = IMAGES;
    }
    return CSSCarouselComponent;
}());
CSSCarouselComponent = tslib_1.__decorate([
    Component({
        selector: 'css-carousel',
        styleUrls: [
            'carousel.style.scss'
        ],
        providers: [],
        templateUrl: 'carousel.template.html'
    })
], CSSCarouselComponent);
export { CSSCarouselComponent };
//IMAGES array implementing Image interface
var IMAGES = [
    { "title": "We are covered", "caption": "abc", "imageUrl": "assets/images/header.jpg", "pageUrl": "/", "buttonCopy": "temp" },
    { "title": "Generation Gap", "caption": "abc", "imageUrl": "assets/images/header.jpg", "pageUrl": "/", "buttonCopy": "temp" },
    { "title": "Potter Me", "caption": "abc", "imageUrl": "assets/images/header.jpg", "pageUrl": "/", "buttonCopy": "temp" },
    { "title": "Pre-School Kids", "caption": "abc", "imageUrl": "assets/images/header.jpg", "pageUrl": "/", "buttonCopy": "temp" },
    { "title": "Young Peter Cech", "caption": "abc", "imageUrl": "assets/images/header.jpg", "pageUrl": "/", "buttonCopy": "temp" }
];
//# sourceMappingURL=carousel.component.js.map