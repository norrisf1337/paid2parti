import { Component, Input, OnInit } from '@angular/core';
import { HeaderService } from '../shared/services/header.service';
import { UserService } from '../shared/services/user.service';
import { Emitter } from '../shared/services/emitter.service';

@Component({
  selector: 'pp-header',
  entryComponents: [],
  styleUrls: [
    'header.style.scss'
  ],
  providers: [],
  templateUrl: 'header.template.html'
})
export class Header {
  public headerType: string;
  public headerTitle: string;
  public subTitle: string;

  constructor (public hs: HeaderService, public userService: UserService, public emitter: Emitter) {

  }

  public isLogged = () => {
    return this.userService.isLogged();
  }
  public openCustom = () => {
   this.emitter.emit('register',{});
  }
  // public setHeader(headerType, headerTitle, subTitle){
  //   this.headerType = headerType;
  //   this.headerTitle = headerTitle;
  //   this.subTitle = subTitle;
  //   console.log(this);

  // }
}
