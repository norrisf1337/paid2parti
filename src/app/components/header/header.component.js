import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HeaderService } from '../shared/services/header.service';
import { UserService } from '../shared/services/user.service';
import { Emitter } from '../shared/services/emitter.service';
var Header = (function () {
    function Header(hs, userService, emitter) {
        var _this = this;
        this.hs = hs;
        this.userService = userService;
        this.emitter = emitter;
        this.isLogged = function () {
            return _this.userService.isLogged();
        };
        this.openCustom = function () {
            _this.emitter.emit('register', {});
        };
    }
    return Header;
}());
Header = tslib_1.__decorate([
    Component({
        selector: 'pp-header',
        entryComponents: [],
        styleUrls: [
            'header.style.scss'
        ],
        providers: [],
        templateUrl: 'header.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService, UserService, Emitter])
], Header);
export { Header };
//# sourceMappingURL=header.component.js.map