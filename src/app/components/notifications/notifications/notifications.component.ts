import { Component, Input } from '@angular/core';
import { NotificationService } from '../../shared/services/notification.service';
@Component({
	selector: 'notifications',
	entryComponents: [],
	styleUrls: [
		'notifications.style.scss'
	],
	providers: [],
	templateUrl: 'notifications.template.html'
})
export class Notifications {
	@Input('noteType') noteType = 'default';
	@Input('limit') limit = 0;
	public notifications: any = [];
	constructor(public noteService: NotificationService) {

	}
	ngOnInit() {
		this.noteService.get(null, this.limit)
			.subscribe(
			data => {
				if (data.success) {
					this.notifications = data.notifications;
				}
			},
			error => {

			});
	}
}
