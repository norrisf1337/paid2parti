import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { NotificationService } from '../../shared/services/notification.service';
var Notifications = (function () {
    function Notifications(noteService) {
        this.noteService = noteService;
        this.noteType = 'default';
        this.limit = 0;
        this.notifications = [];
    }
    Notifications.prototype.ngOnInit = function () {
        var _this = this;
        this.noteService.get(null, this.limit)
            .subscribe(function (data) {
            if (data.success) {
                _this.notifications = data.notifications;
            }
        }, function (error) {
        });
    };
    return Notifications;
}());
tslib_1.__decorate([
    Input('noteType'),
    tslib_1.__metadata("design:type", Object)
], Notifications.prototype, "noteType", void 0);
tslib_1.__decorate([
    Input('limit'),
    tslib_1.__metadata("design:type", Object)
], Notifications.prototype, "limit", void 0);
Notifications = tslib_1.__decorate([
    Component({
        selector: 'notifications',
        entryComponents: [],
        styleUrls: [
            'notifications.style.scss'
        ],
        providers: [],
        templateUrl: 'notifications.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [NotificationService])
], Notifications);
export { Notifications };
//# sourceMappingURL=notifications.component.js.map