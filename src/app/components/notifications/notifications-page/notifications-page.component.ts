import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'notifications-page',
  entryComponents: [],
  styleUrls: [
    'notifications-page.style.scss'
  ],
  providers: [],
  templateUrl: 'notifications-page.template.html'
})
export class NotificationsPage {
	constructor (public hs: HeaderService, public noteService: NotificationService){
		this.hs.resetHeader();
		this.hs.setHeader('single', 'Notifications', '');
	}
}
