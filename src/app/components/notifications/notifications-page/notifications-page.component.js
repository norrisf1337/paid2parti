import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
import { NotificationService } from '../../shared/services/notification.service';
var NotificationsPage = (function () {
    function NotificationsPage(hs, noteService) {
        this.hs = hs;
        this.noteService = noteService;
        this.hs.resetHeader();
        this.hs.setHeader('single', 'Notifications', '');
    }
    return NotificationsPage;
}());
NotificationsPage = tslib_1.__decorate([
    Component({
        selector: 'notifications-page',
        entryComponents: [],
        styleUrls: [
            'notifications-page.style.scss'
        ],
        providers: [],
        templateUrl: 'notifications-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService, NotificationService])
], NotificationsPage);
export { NotificationsPage };
//# sourceMappingURL=notifications-page.component.js.map