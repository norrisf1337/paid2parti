import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
var Notification = (function () {
    function Notification() {
    }
    Notification.prototype.ngOnInit = function () {
        console.log('note', this.notification);
    };
    return Notification;
}());
tslib_1.__decorate([
    Input('note'),
    tslib_1.__metadata("design:type", Object)
], Notification.prototype, "notification", void 0);
Notification = tslib_1.__decorate([
    Component({
        selector: 'notification',
        entryComponents: [],
        styleUrls: [
            'notification.style.scss'
        ],
        providers: [],
        templateUrl: 'notification.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [])
], Notification);
export { Notification };
//# sourceMappingURL=notification.component.js.map