import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'notification',
  entryComponents: [],
  styleUrls: [
    'notification.style.scss'
  ],
  providers: [],
  templateUrl: 'notification.template.html'
})
export class Notification {
	@Input('note') notification;
	constructor() {

	}

	ngOnInit() {
		console.log('note', this.notification);
	}
}
