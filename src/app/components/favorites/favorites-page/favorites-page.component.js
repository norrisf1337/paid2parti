import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TicketService } from '../../shared/services/ticket.service';
import { UserService } from '../../shared/services/user.service';
import { HeaderService } from '../../shared/services/header.service';
var FavoritesPage = (function () {
    function FavoritesPage(route, router, ticketService, userService, hs) {
        this.route = route;
        this.router = router;
        this.ticketService = ticketService;
        this.userService = userService;
        this.hs = hs;
        this.hs.resetHeader();
        this.hs.setHeader('single', 'Saved Events', '');
    }
    FavoritesPage.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.getFavorites()
            .subscribe(function (data) {
            _this.favorites = data.favorites;
        }, function (error) {
        });
    };
    return FavoritesPage;
}());
FavoritesPage = tslib_1.__decorate([
    Component({
        selector: 'favorites-page',
        entryComponents: [],
        styleUrls: [
            'favorites-page.style.scss'
        ],
        providers: [],
        templateUrl: 'favorites-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [ActivatedRoute, Router, TicketService, UserService, HeaderService])
], FavoritesPage);
export { FavoritesPage };
//# sourceMappingURL=favorites-page.component.js.map