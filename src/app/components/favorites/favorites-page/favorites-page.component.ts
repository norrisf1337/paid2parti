import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TicketService } from '../../shared/services/ticket.service';
import { UserService } from '../../shared/services/user.service';

@Component({
  selector: 'favorites-page',
  entryComponents: [],
  styleUrls: [
    'favorites-page.style.scss'
  ],
  providers: [],
  templateUrl: 'favorites-page.template.html'
})
export class FavoritesPage {
	public favorites:any;
	public loaded: boolean = false;

	constructor(private route: ActivatedRoute, private router: Router, public ticketService: TicketService, public userService: UserService) {
	}

		ngOnInit() {
			this.userService.getFavorites()
			.subscribe(
			data => {
				this.favorites = data.favorites;
				this.loaded = true;
			},
			error => {
			})

	   }

}
