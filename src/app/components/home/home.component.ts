import { Component } from '@angular/core';
import { CSSCarouselComponent } from '../carousel/carousel.component';
import { HeaderService } from '../shared/services/header.service';



@Component({
  selector: 'home',
  entryComponents: [CSSCarouselComponent],
  styleUrls: [
    'home.style.scss'
  ],
  providers: [],
  templateUrl: 'home.template.html'
})
export class Home {

  constructor (public hs: HeaderService) {
    this.hs.resetHeader();
    this.hs.setHeader('car', '', '');
  }
}
