import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CSSCarouselComponent } from '../carousel/carousel.component';
import { HeaderService } from '../shared/services/header.service';
var Home = (function () {
    function Home(hs) {
        this.hs = hs;
        this.hs.resetHeader();
        this.hs.setHeader('car', '', '');
    }
    return Home;
}());
Home = tslib_1.__decorate([
    Component({
        selector: 'home',
        entryComponents: [CSSCarouselComponent],
        styleUrls: [
            'home.style.scss'
        ],
        providers: [],
        templateUrl: 'home.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService])
], Home);
export { Home };
//# sourceMappingURL=home.component.js.map