import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { TicketService } from '../../shared/services/ticket.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HeaderService } from '../../shared/services/header.service';
var TicketPage = (function () {
    function TicketPage(ticketService, route, router, hs) {
        this.ticketService = ticketService;
        this.route = route;
        this.router = router;
        this.hs = hs;
    }
    TicketPage.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this);
        var id = this.route.snapshot.params['id'];
        this.ticketService.getTicket(id)
            .subscribe(function (data) {
            _this.tickets = data.tickets;
        }, function (error) {
        });
    };
    return TicketPage;
}());
TicketPage = tslib_1.__decorate([
    Component({
        selector: 'ticket-page',
        entryComponents: [],
        styleUrls: [
            'ticket-page.style.scss'
        ],
        providers: [],
        templateUrl: 'ticket-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [TicketService, ActivatedRoute, Router, HeaderService])
], TicketPage);
export { TicketPage };
//# sourceMappingURL=ticket-page.component.js.map