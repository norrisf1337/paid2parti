import { Component, OnInit } from '@angular/core';
import { TicketService } from '../../shared/services/ticket.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HeaderService } from '../../shared/services/header.service';


@Component({
	selector: 'ticket-page',
	entryComponents: [],
	styleUrls: [
		'ticket-page.style.scss'
	],
	providers: [],
	templateUrl: 'ticket-page.template.html'
})
export class TicketPage {
	public tickets: any;
	public loaded: boolean = true;
	constructor(public ticketService: TicketService, private route: ActivatedRoute, private router: Router, public hs: HeaderService) {

	}

	ngOnInit() {
		console.log(this)
		let id = this.route.snapshot.params['id'];
		this.ticketService.getTicket(id)
			.subscribe(
			data => {
				this.tickets = data.tickets;
				this.loaded = true;
			},
			error => {
			})
	}

}
