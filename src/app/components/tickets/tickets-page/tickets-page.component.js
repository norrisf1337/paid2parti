import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { TicketService } from '../../shared/services/ticket.service';
import { HeaderService } from '../../shared/services/header.service';
var TicketsPage = (function () {
    function TicketsPage(ticketService, hs) {
        this.ticketService = ticketService;
        this.hs = hs;
        this.tickets = [];
        this.hs.resetHeader();
        this.hs.setHeader('single', 'My Tickets', '');
    }
    TicketsPage.prototype.ngOnInit = function () {
        var _this = this;
        this.ticketService.getTickets()
            .subscribe(function (data) {
            _this.tickets = _this.ticketService.setTickets(data.tickets);
        }, function (error) {
        });
    };
    return TicketsPage;
}());
TicketsPage = tslib_1.__decorate([
    Component({
        selector: 'tickets-page',
        entryComponents: [],
        styleUrls: [
            'tickets-page.style.scss'
        ],
        providers: [],
        templateUrl: 'tickets-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [TicketService, HeaderService])
], TicketsPage);
export { TicketsPage };
//# sourceMappingURL=tickets-page.component.js.map