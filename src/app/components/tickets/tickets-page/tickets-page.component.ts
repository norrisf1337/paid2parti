import { Component, OnInit } from '@angular/core';
import { TicketService } from '../../shared/services/ticket.service';
import { HeaderService } from '../../shared/services/header.service';

@Component({
	selector: 'tickets-page',
	entryComponents: [],
	styleUrls: [
		'tickets-page.style.scss'
	],
	providers: [],
	templateUrl: 'tickets-page.template.html'
})
export class TicketsPage {
	public tickets: any = [];
	public loaded: boolean = false;
	constructor (public ticketService: TicketService) {
	}

	ngOnInit() {
		this.ticketService.getTickets()
			.subscribe(
			data => {
				this.tickets = this.ticketService.setTickets(data.tickets);
				this.loaded = true;
			},
			error => {
			})
	}
}
