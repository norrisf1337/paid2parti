import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
var API = (function () {
    function API(http) {
        var _this = this;
        this.http = http;
        this.get = function (route, params, headers) {
            if (params === void 0) { params = {}; }
            return _this.http.post(route, JSON.stringify(params), _this.jwt()).map(function (response) { return response.json(); });
        };
        // private helper methods
        this.jwt = function () {
            // create authorization header with jwt token
            var currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (currentUser && currentUser.token) {
                var headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token, 'Content-Type': 'application/json' });
                return new RequestOptions({ headers: headers });
            }
            else {
                var headers = new Headers({ 'Content-Type': 'application/json' });
                return new RequestOptions({ headers: headers });
            }
        };
    }
    return API;
}());
API = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [Http])
], API);
export { API };
new Headers({ 'Content-Type': 'application/json' });
//# sourceMappingURL=api.service.js.map