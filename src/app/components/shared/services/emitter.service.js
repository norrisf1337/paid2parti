import * as tslib_1 from "tslib";
import * as Rx from 'rxjs';
import { Injectable } from '@angular/core';
var Emitter = (function () {
    function Emitter() {
        var _this = this;
        this.hasOwnProp = {}.hasOwnProperty;
        this.subjects = {};
        this.createName = function (name) {
            return '$' + name;
        };
        this.emit = function (name, data) {
            var fnName = _this.createName(name);
            _this.subjects[fnName] || (_this.subjects[fnName] = new Rx.Subject());
            _this.subjects[fnName].next(data);
        };
        this.listen = function (name, handler) {
            var fnName = _this.createName(name);
            _this.subjects[fnName] || (_this.subjects[fnName] = new Rx.Subject());
            return _this.subjects[fnName].subscribe(handler);
        };
        this.dispose = function (name) {
            _this.subjects[name].dispose();
        };
        this.disposeAll = function () {
            var subjects = _this.subjects;
            for (var prop in subjects) {
                if (_this.hasOwnProp.call(subjects, prop)) {
                    subjects[prop].dispose();
                }
            }
            _this.subjects = {};
        };
    }
    return Emitter;
}());
Emitter = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [])
], Emitter);
export { Emitter };
//# sourceMappingURL=emitter.service.js.map