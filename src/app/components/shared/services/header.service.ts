import { Injectable } from '@angular/core';

@Injectable()
export class HeaderService {
    public headerType = '';
    public headerTitle = '';
    public subTitle = '';
    constructor() { }

    public setHeader= (headerType = 'single', headerTitle = 'Paid2Parti', subTitle = '') => {
        this.headerType = headerType;
        this.headerTitle = headerTitle;
        this.subTitle = subTitle;
    }
      public resetHeader = () => {
        this.headerType = '';
        this.headerTitle = '';
        this.subTitle = '';
    }

    public getHeaderType = () => {
        return this.headerType;
    }

    public getHeaderTitle = () => {
        return this.headerTitle;
    }

    public getSubTitle = () => {
        return this.subTitle;
    }

}

