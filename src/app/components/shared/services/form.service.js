import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
var FormService = (function () {
    function FormService() {
        this.matchingPasswords = function (passwordKey, confirmPasswordKey) {
            return function (group) {
                var password = group.controls[passwordKey];
                var confirmPassword = group.controls[confirmPasswordKey];
                if (password.value !== confirmPassword.value) {
                    return {
                        mismatchedPasswords: true
                    };
                }
            };
        };
        this.validateEmailString = function (control) {
            var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var result = control.value.replace(/\s/g, "").split(/,|;/);
            for (var i = 0; i < result.length; i++) {
                if (!regex.test(result[i])) {
                    return { "incorrectMailFormat": true };
                }
            }
            return null;
        };
    }
    FormService.prototype.mailFormat = function (control) {
        var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (control.value != "" && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
            return { "incorrectMailFormat": true };
        }
        return null;
    };
    FormService.prototype.nameFormat = function (control) {
        var NAME_REGEXP = /^[a-zA-Z ]{1,30}$/;
        if (control.value != "" && (control.value.length <= 2 || !NAME_REGEXP.test(control.value))) {
            return { "incorrectName": true };
        }
        return null;
    };
    FormService.prototype.passFormat = function (control) {
        var NAME_REGEXP = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/;
        if (control.value != "" && (control.value.length <= 7)) {
            return { "passFormat": true };
        }
        return null;
    };
    return FormService;
}());
FormService = tslib_1.__decorate([
    Injectable()
], FormService);
export { FormService };
//# sourceMappingURL=form.service.js.map