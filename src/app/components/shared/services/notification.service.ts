import { Injectable } from '@angular/core';
import { API } from './api.service';
import { UserService } from './user.service';

@Injectable()
export class NotificationService {
    constructor (public api: API, public userService: UserService) {

    }

    public get = (type = "all", limit = 0) => {
        return this.api.get('/api/user/notifications', {uid: this.userService.user.uid, limit:limit});
    }

    public create = (data) => {
        data._id = data.id;
        return this.api.get('/api/user/notifications/create', {notification: data});
    }

    public markRead = (nid) => {
        return this.api.get('/api/user/notifications/mark', {nid: nid});
    }

    public delete = (nid) => {
        return this.api.get('/api/user/notifications/delete', {nid: nid});
    }
}

