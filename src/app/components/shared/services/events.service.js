import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { API } from './api.service';
import { UserService } from './user.service';
var EventService = (function () {
    function EventService(http, api, userService) {
        var _this = this;
        this.http = http;
        this.api = api;
        this.userService = userService;
        this.getAll = function (options) {
            if (options === void 0) { options = {}; }
            return _this.api.get('/api/events', options);
        };
        this.getById = function (id) {
            return _this.api.get('/api/event', { id: id });
        };
        this.getMyEvents = function () {
            return _this.api.get('/api/events/user', { id: _this.userService.user.uid });
        };
        this.create = function (event, edit) {
            console.log('hit');
            if (edit) {
                return _this.api.get('/api/event/create', event);
            }
            else {
                return _this.api.get('/api/event/create', event);
            }
        };
        this.update = function (event) {
            return _this.api.get('/api/event/update/' + event._id, event);
        };
        this.deleteEvent = function (id) {
            return _this.api.get('/api/event/remove', { _id: id });
        };
        this.addPromoter = function (data) {
            return _this.api.get('/api/user/addPromoter', data);
        };
    }
    return EventService;
}());
EventService = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [Http, API, UserService])
], EventService);
export { EventService };
//# sourceMappingURL=events.service.js.map