import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { API } from './api.service';
import { UserService } from './user.service';
import * as _ from 'lodash';
var TicketService = (function () {
    function TicketService(http, api, router, userService) {
        var _this = this;
        this.http = http;
        this.api = api;
        this.router = router;
        this.userService = userService;
        this.user = {
            uid: null,
            refId: null,
            isLogged: false
        };
        this.initUser = function () {
        };
        this.generateUid = function () {
            function guid() {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
                }
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                    s4() + '-' + s4() + s4() + s4();
            }
            return guid();
        };
        this.purchaseTickets = function (eventId, uid, ticketId, qty, cost, owner) {
            return _this.api.get('/api/tickets/add', {
                client: uid,
                event: eventId,
                qty: qty,
                ticket: ticketId,
                cost: cost,
                owner: owner,
                ref: _this.userService.user.refId
            });
        };
        this.setTickets = function (tickets) {
            var ticketArray = [];
            _.forEach(tickets, function (ticket) {
                ticketArray[ticket.ticket._id] = (!_.isUndefined(ticketArray[ticket.ticket._id])) ? ticketArray[ticket.ticket._id] : [];
                ticketArray[ticket.ticket._id].push(ticket);
            });
            return _.values(ticketArray);
        };
        this.getTickets = function (limit) {
            if (limit === void 0) { limit = 0; }
            return _this.api.get('/api/tickets/', { uid: _this.userService.user.uid, limit: limit });
        };
        this.getTicket = function (id) {
            return _this.api.get('/api/tickets/purchased', { eventId: id });
        };
    }
    return TicketService;
}());
TicketService = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [Http, API, Router, UserService])
], TicketService);
export { TicketService };
//# sourceMappingURL=ticket.service.js.map