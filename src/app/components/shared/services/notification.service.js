import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { API } from './api.service';
import { UserService } from './user.service';
var NotificationService = (function () {
    function NotificationService(api, userService) {
        var _this = this;
        this.api = api;
        this.userService = userService;
        this.get = function (type, limit) {
            if (type === void 0) { type = "all"; }
            if (limit === void 0) { limit = 0; }
            return _this.api.get('/api/user/notifications', { uid: _this.userService.user.uid, limit: limit });
        };
        this.create = function (data) {
            data._id = data.id;
            return _this.api.get('/api/user/notifications/create', { notification: data });
        };
        this.markRead = function (nid) {
            return _this.api.get('/api/user/notifications/mark', { nid: nid });
        };
        this.delete = function (nid) {
            return _this.api.get('/api/user/notifications/delete', { nid: nid });
        };
    }
    return NotificationService;
}());
NotificationService = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [API, UserService])
], NotificationService);
export { NotificationService };
//# sourceMappingURL=notification.service.js.map