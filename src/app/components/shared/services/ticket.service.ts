import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Router } from '@angular/router'
import { API } from './api.service';
import { UserService } from './user.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Injectable()
export class TicketService {
    constructor(private http: Http, public api: API, public router: Router, public userService: UserService) {
    }

    public user: any = {
        uid: null,
        refId: null,
        isLogged: false
    };

    public initUser = () => {

    }

    public generateUid = (): string => {
        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }

        return guid();
    }

    public purchaseTickets = (eventId: string, uid: string, ticketId: any, qty: number, cost: number, owner: string) => {
        return this.api.get('/api/tickets/add', {
            client: uid,
            event: eventId,
            qty: qty,
            ticket: ticketId,
            cost: cost,
            owner: owner,
            ref: this.userService.user.refId
        });
    }

    public setTickets = (tickets) => {
        let ticketArray = [];
        _.forEach(tickets, (ticket) => {
            ticketArray[ticket.ticket._id] = (!_.isUndefined(ticketArray[ticket.ticket._id])) ? ticketArray[ticket.ticket._id] : [];
            ticketArray[ticket.ticket._id].push(ticket);
        });

        return _.values(ticketArray);
    };

    public getTickets = (limit = 0) => {
        return this.api.get('/api/tickets/', { uid: this.userService.user.uid, limit: limit });
    }

    public getTicket = (id) => {
        return this.api.get('/api/tickets/purchased', { eventId: id });
    }

}
