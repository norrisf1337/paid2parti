import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

@Injectable()
export class API {
    constructor(private http: Http) { }

    public get = (route: string, params: Object = {}, headers?: Object) => {
        return this.http.post(route, JSON.stringify(params), this.jwt()).map((response: Response) => response.json());
    }

    // private helper methods
    private jwt = () => {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token, 'Content-Type': 'application/json' });
            return new RequestOptions({ headers: headers });
        } else {
            let headers = new Headers({ 'Content-Type': 'application/json' });
            return new RequestOptions({ headers: headers })
        }
    }
}
new Headers({ 'Content-Type': 'application/json' })
