import { Injectable } from '@angular/core';

@Injectable()
export class FormService {

    public matchingPasswords = (passwordKey: string, confirmPasswordKey: string) => {

        return (group) => {
            let password = group.controls[passwordKey];
            let confirmPassword = group.controls[confirmPasswordKey];

            if (password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        }
    }

    public mailFormat(control) {

        var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (control.value != "" && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
            return { "incorrectMailFormat": true };
        }

        return null;
    }

    public nameFormat(control) {

        var NAME_REGEXP = /^[a-zA-Z ]{1,30}$/;

        if (control.value != "" && (control.value.length <= 2 || !NAME_REGEXP.test(control.value))) {
            return { "incorrectName": true };
        }

        return null;
    }

    public passFormat(control) {

        var NAME_REGEXP = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/;

        if (control.value != "" && (control.value.length <= 7)) {
            return { "passFormat": true };
        }

        return null;
    }

    public validateEmailString = (control) => {
        let regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        let result = control.value.replace(/\s/g, "").split(/,|;/);
        for(let i = 0;i < result.length;i++) {
            if(!regex.test(result[i])) {
                return { "incorrectMailFormat": true };
            }
        }
        return null;
    }

}

