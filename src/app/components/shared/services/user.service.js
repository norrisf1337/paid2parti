import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { API } from './api.service';
import * as _ from 'lodash';
var UserService = (function () {
    function UserService(http, api, router) {
        var _this = this;
        this.http = http;
        this.api = api;
        this.router = router;
        this.user = {
            uid: null,
            refid: null,
            isLogged: false
        };
        this.initUser = function () {
        };
        this.generateUid = function () {
            function guid() {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
                }
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                    s4() + '-' + s4() + s4() + s4();
            }
            return guid();
        };
        this.getUser = function () {
            return _this.user;
        };
        this.isLogged = function () {
            return _this.user.isLogged;
        };
        this.updateUser = function (key, val) {
            if (!_.isUndefined(_this.user[key])) {
                _this.user.key = val;
            }
            else {
                _this.user[key] = val;
            }
        };
        this.login = function (username, password) {
            return _this.api.get('/api/authenticate', { email: username, password: password });
        };
        this.create = function (user) {
            return _this.api.get('/api/users/create/', user);
        };
        this.createPromoter = function (promoter) {
            return _this.api.get('/api/users/promoters/create', promoter);
        };
        this.getPromoters = function () {
            var data = { uid: _this.user.uid };
            return _this.api.get('/api/users/promoters', data);
        };
        this.getPromoter = function (id) {
            var data = { id: id };
            return _this.api.get('/api/users/promoter', data);
        };
        this.getById = function (id) {
            return _this.api.get('/api/users/', id);
        };
        this.getPreferences = function (userID) {
            return _this.api.get('/api/user/preferences/get', userID);
        };
        this.setPreferences = function (prefs) {
            prefs.uid = _this.user.uid;
            return _this.api.get('/api/user/preferences/set', prefs);
        };
        this.getDeposit = function (userID) {
            return _this.api.get('/api/user/deposit/get', userID);
        };
        this.setDeposit = function (prefs) {
            prefs.uid = _this.user.uid;
            return _this.api.get('/api/user/deposit/set', prefs);
        };
        this.getContactInfo = function (userID) {
            return _this.api.get('/api/user/contact/get', userID);
        };
        this.setContactInfo = function (prefs) {
            prefs.uid = _this.user.uid;
            return _this.api.get('/api/user/contact/set', prefs);
        };
        this.getCredits = function (userID) {
            return _this.api.get('/api/user/credits/get', userID);
        };
        this.setCredits = function (prefs) {
            prefs.uid = _this.user.uid;
            return _this.api.get('/api/user/credits/set', prefs);
        };
        this.logout = function () {
            // remove user from local storage to log user out
            localStorage.removeItem('currentUser');
            localStorage.removeItem('refId');
            _this.user.isLogged = false;
            _this.router.navigate(['/']);
        };
        this.getFavorites = function (limit) {
            if (limit === void 0) { limit = 0; }
            return _this.api.get('/api/user/favorites/get', { uid: _this.user.uid, limit: limit });
        };
        this.setFavorite = function (event) {
            var data = { uid: _this.user.uid, eid: event._id };
            return _this.api.get('/api/user/favorites/set', data);
        };
        this.removeFavorite = function (event) {
            var data = { uid: _this.user.uid, eid: event._id };
            return _this.api.get('/api/user/favorites/remove', data);
        };
        this.getDetails = function (keys) {
            var data = { uid: _this.user.uid, details: keys };
            return _this.api.get('/api/user/details', data);
        };
        this.setDetails = function (details) {
            var data = { uid: _this.user.uid, details: details };
            return _this.api.get('/api/user/details/update', data);
        };
        this.setPassword = function (details) {
            var data = { uid: _this.user.uid, details: details };
            return _this.api.get('/api/user/password', data);
        };
        this.getFunds = function (limit) {
            if (limit === void 0) { limit = 0; }
            return _this.api.get('/api/user/funds', { uid: _this.user.uid });
        };
        this.inviteFriends = function (data) {
            return _this.api.get('/api/user/invite', { uid: _this.user.uid, emails: data });
        };
        this.contact = function (data) {
            return _this.api.get('/api/contact', { data: data });
        };
        this.addComment = function (data) {
            return _this.api.get('/api/promoter/comments/add', { data: data });
        };
        this.ratePromoter = function (data) {
            return _this.api.get('/api/promoter/rate', data);
        };
        this.getRating = function (data) {
            return _this.api.get('/api/promoter/ratings', data);
        };
    }
    return UserService;
}());
UserService = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [Http, API, Router])
], UserService);
export { UserService };
//# sourceMappingURL=user.service.js.map