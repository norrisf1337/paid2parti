import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Router } from '@angular/router'
import { API } from './api.service';
import * as _ from 'lodash';

@Injectable()
export class UserService {
    constructor(private http: Http, public api: API, public router: Router) {

    }

    public user: any = {
        uid: null,
        refid: null,
        isLogged: false
    };

    public initUser = () => {

    }

    public generateUid = (): string => {
        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }

        return guid();
    }

    public getUser = () => {
        return this.user;
    }

    public isLogged = () => {
        return this.user.isLogged;
    }

    public updateUser = (key: string, val: any) => {

        if (!_.isUndefined(this.user[key])) {
            this.user.key = val;
        } else {
            this.user[key] = val;
        }

    }

    public login = (username: string, password: string) => {
        return this.api.get('/api/authenticate', { email: username, password: password });
    }

    public create = (user) => {
        return this.api.get('/api/users/create/', user);
    }

    public createPromoter = (promoter) => {
        return this.api.get('/api/users/promoters/create', promoter);
    }

    public getPromoters = () => {
        let data = {uid: this.user.uid};
        return this.api.get('/api/users/promoters', data);
    }

    public getPromoter = (id) => {
        let data = {id: id};
        return this.api.get('/api/users/promoter', data);
    }

    public getById = (id) => {
        return this.api.get('/api/users/', id);
    }

    public getPreferences = (userID) => {
        return this.api.get('/api/user/preferences/get', userID);
    }

    public setPreferences = (prefs) => {
        prefs.uid = this.user.uid;
        return this.api.get('/api/user/preferences/set', prefs);
    }

    public getDeposit = (userID) => {
        return this.api.get('/api/user/deposit/get', userID);
    }

    public setDeposit = (prefs) => {
        prefs.uid = this.user.uid;
        return this.api.get('/api/user/deposit/set', prefs);
    }

    public getContactInfo = (userID) => {
        return this.api.get('/api/user/contact/get', userID);
    }

    public setContactInfo = (prefs) => {
        prefs.uid = this.user.uid;
        return this.api.get('/api/user/contact/set', prefs);
    }

    public getCredits = (userID) => {
        return this.api.get('/api/user/credits/get', userID);
    }

    public setCredits = (prefs) => {
        prefs.uid = this.user.uid;
        return this.api.get('/api/user/credits/set', prefs);
    }

    public logout = () => {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('refId');
        this.user.isLogged = false;
        this.router.navigate(['/']);
    }

    public getFavorites = (limit = 0) => {
        return this.api.get('/api/user/favorites/get', {uid: this.user.uid, limit:limit});
    }

    public setFavorite = (event) => {
        let data = {uid: this.user.uid, eid: event._id}
        return this.api.get('/api/user/favorites/set', data);
    }

    public removeFavorite = (event) => {
        let data = {uid: this.user.uid, eid: event._id}
        return this.api.get('/api/user/favorites/remove', data);
    }

    public getDetails = (keys) => {
        let data = {uid: this.user.uid, details:keys}
        return this.api.get('/api/user/details', data);
    }

    public setDetails = (details) => {
        let data = {uid: this.user.uid, details:details}
        return this.api.get('/api/user/details/update', data);
    }

    public setPassword = (details) => {
        let data = {uid: this.user.uid, details:details}
        return this.api.get('/api/user/password', data);
    }

    public getFunds = (limit = 0) => {
        return this.api.get('/api/user/funds', {uid: this.user.uid});
    }

    public inviteFriends = (data) => {
        return this.api.get('/api/user/invite', {uid: this.user.uid, emails:data});
    }

    public contact = (data) => {
        return this.api.get('/api/contact', {data:data});
    }

    public addComment = (data) => {
        return this.api.get('/api/promoter/comments/add', {data:data});
    }

    public ratePromoter = (data) => {
        return this.api.get('/api/promoter/rate', data);
    }

    public getRating = (data) => {
        return this.api.get('/api/promoter/ratings', data);
    }

}
