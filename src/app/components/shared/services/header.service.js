import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
var HeaderService = (function () {
    function HeaderService() {
        var _this = this;
        this.headerType = '';
        this.headerTitle = '';
        this.subTitle = '';
        this.setHeader = function (headerType, headerTitle, subTitle) {
            if (headerType === void 0) { headerType = 'single'; }
            if (headerTitle === void 0) { headerTitle = 'Paid2Parti'; }
            if (subTitle === void 0) { subTitle = ''; }
            _this.headerType = headerType;
            _this.headerTitle = headerTitle;
            _this.subTitle = subTitle;
        };
        this.resetHeader = function () {
            _this.headerType = '';
            _this.headerTitle = '';
            _this.subTitle = '';
        };
        this.getHeaderType = function () {
            return _this.headerType;
        };
        this.getHeaderTitle = function () {
            return _this.headerTitle;
        };
        this.getSubTitle = function () {
            return _this.subTitle;
        };
    }
    return HeaderService;
}());
HeaderService = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [])
], HeaderService);
export { HeaderService };
//# sourceMappingURL=header.service.js.map