import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { API } from './api.service';
import { UserService } from './user.service';
import * as _ from 'lodash';

@Injectable()
export class EventService {
    constructor(private http: Http, public api: API, public userService: UserService) { }

    public getAll = (options = {}) => {
        return this.api.get('/api/events', options);
    }

    public getById = (id: string) => {
        return this.api.get('/api/event', {id: id});
    }

    public getMyEvents = () => {
        return this.api.get('/api/events/user', {id: this.userService.user.uid});
    }

    public create = (event, edit) => {
        console.log('hit');
        if (edit) {
            return this.api.get('/api/event/create', event);
        }else {
            return this.api.get('/api/event/create', event);
        }

    }

    public update = (event) => {
        return this.api.get('/api/event/update/' + event._id, event);
    }

    public deleteEvent = (id: number) => {
        return this.api.get('/api/event/remove', {_id:id});
    }

    public addPromoter = (data: organizer) => {
        return this.api.get('/api/user/addPromoter', data);
    }


}
