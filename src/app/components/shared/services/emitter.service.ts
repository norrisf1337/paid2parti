import * as Rx from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class Emitter {

    public hasOwnProp = {}.hasOwnProperty;
    public subjects = {};

    constructor() {

    }

    public createName = (name) => {
        return '$' + name;
    }

    public emit = (name, data) => {
        let fnName = this.createName(name);
        this.subjects[fnName] || (this.subjects[fnName] = new Rx.Subject());
        this.subjects[fnName].next(data);
    };

    public listen = (name, handler) => {
        let fnName = this.createName(name);
        this.subjects[fnName] || (this.subjects[fnName] = new Rx.Subject());
        return this.subjects[fnName].subscribe(handler);
    };

    public dispose = (name) => {
        this.subjects[name].dispose();
    };

    public disposeAll = () => {
        let subjects = this.subjects;
        for (let prop in subjects) {
            if (this.hasOwnProp.call(subjects, prop)) {
                subjects[prop].dispose();
            }
        }
        this.subjects = {};
    };

}
