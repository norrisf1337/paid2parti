import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { API } from '../../shared/services/api.service';
var PaginatorControls = (function () {
    function PaginatorControls(userService, api) {
        var _this = this;
        this.userService = userService;
        this.pageItems = [];
        this.setPage = function () {
            var pageDivision = _this.options.total / _this.options.maxPerPage;
            _this.makePages(Math.ceil(pageDivision));
        };
        this.makePages = function (count) {
            for (var i = 0; i < count; i++) {
                _this.pageItems.push({ 'label': i + 1 });
            }
        };
        this.gotoPage = function (page) {
            console.log('pil', _this.pageItems.length);
            if (page === 'forward' && _this.options.currentPage !== _this.pageItems.length) {
                _this.options.callEndpoint(_this.options.currentPage + 1);
            }
            else if (page === 'back' && _this.options.currentPage !== 1) {
                _this.options.callEndpoint(_this.options.currentPage - 1);
            }
            else if ((page === 'forward' && _this.options.currentPage === _this.pageItems.length) || (page === 'back' && _this.options.currentPage === 1)) {
                return false;
            }
            else {
                console.log('page', page);
                _this.options.callEndpoint(page);
            }
        };
        console.log('pag', this.options);
    }
    PaginatorControls.prototype.ngOnInit = function () {
        this.setPage();
    };
    return PaginatorControls;
}());
tslib_1.__decorate([
    Input('options'),
    tslib_1.__metadata("design:type", Object)
], PaginatorControls.prototype, "options", void 0);
PaginatorControls = tslib_1.__decorate([
    Component({
        selector: 'paginator-controls',
        styleUrls: [
            'paginator-controls.style.scss'
        ],
        templateUrl: 'paginator-controls.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [UserService, API])
], PaginatorControls);
export { PaginatorControls };
//# sourceMappingURL=paginator-controls.component.js.map