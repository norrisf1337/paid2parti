import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../shared/services/user.service';
import { API } from '../../shared/services/api.service';
import * as _ from 'lodash';

@Component({
	selector: 'paginator-controls',
	styleUrls: [
		'paginator-controls.style.scss'
	],
	templateUrl: 'paginator-controls.template.html'
})
export class PaginatorControls {
	@Input('options') options;
	public pageItems = [];
	constructor(public userService: UserService, api: API) {
		console.log('pag', this.options)
	}


	public setPage = () => {
		let pageDivision = this.options.total / this.options.maxPerPage;
		this.makePages(Math.ceil(pageDivision));
	}

	public makePages = (count) => {
		for (var i = 0; i < count; i++) {
			this.pageItems.push({'label': i + 1})
		}
	}

	public gotoPage = (page) => {
		console.log('pil', this.pageItems.length);
		if(page === 'forward' && this.options.currentPage !== this.pageItems.length) {
			this.options.callEndpoint(this.options.currentPage + 1);
		} else if (page === 'back' && this.options.currentPage !== 1 ) {
			this.options.callEndpoint(this.options.currentPage - 1);
		} else if ((page === 'forward' && this.options.currentPage === this.pageItems.length) || (page === 'back' && this.options.currentPage === 1)) {
			return false;
		} else {
			console.log('page', page);
			this.options.callEndpoint(page);
		}
	}

	ngOnInit() {
		this.setPage();
	}
}
