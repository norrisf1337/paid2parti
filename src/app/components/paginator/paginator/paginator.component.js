import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
var Paginator = (function () {
    function Paginator(userService) {
        this.userService = userService;
    }
    Paginator.prototype.ngOnInit = function () {
        console.log(this.level);
    };
    return Paginator;
}());
tslib_1.__decorate([
    Input('levelData'),
    tslib_1.__metadata("design:type", Object)
], Paginator.prototype, "level", void 0);
Paginator = tslib_1.__decorate([
    Component({
        selector: 'paginator',
        styleUrls: [
            'paginator.style.scss'
        ],
        templateUrl: 'paginator.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [UserService])
], Paginator);
export { Paginator };
//# sourceMappingURL=paginator.component.js.map