import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../shared/services/user.service';

@Component({
	selector: 'paginator',
	styleUrls: [
		'paginator.style.scss'
	],
	templateUrl: 'paginator.template.html'
})
export class Paginator {
	@Input('levelData') level;
	constructor(public userService: UserService) {

	}

	ngOnInit() {
		console.log(this.level);
	}
}
