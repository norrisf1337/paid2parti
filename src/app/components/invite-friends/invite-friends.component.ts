import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../shared/services/user.service';
import { FormService } from '../shared/services/form.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
	selector: 'invite-friends',
	styleUrls: [
		'invite-friends.style.scss'
	],
	templateUrl: 'invite-friends.template.html'
})
export class InviteFriends {
	public inviteForm;
	constructor(public userService: UserService, public _fb: FormBuilder, private formService: FormService) {

	}

	ngOnInit() {
		this.inviteForm = this._fb.group({
			emailString: new FormControl("", Validators.compose([Validators.required, this.formService.validateEmailString]))
		});
	}

	public inviteFriends = (values, valid) => {
		console.log(values.emailString.replace(/ /g, '').split(','), valid);
		if (valid) {
			this.userService.inviteFriends(values.emailString.replace(/ /g, '').split(','))
				.subscribe(
				data => {
				console.log('data', data);

				},
				error => {
				})
		}
	}
}
