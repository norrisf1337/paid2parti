import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { UserService } from '../shared/services/user.service';
import { FormService } from '../shared/services/form.service';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
var InviteFriends = (function () {
    function InviteFriends(userService, _fb, formService) {
        var _this = this;
        this.userService = userService;
        this._fb = _fb;
        this.formService = formService;
        this.inviteFriends = function (values, valid) {
            console.log(values.emailString.replace(/ /g, '').split(','), valid);
            if (valid) {
                _this.userService.inviteFriends(values.emailString.replace(/ /g, '').split(','))
                    .subscribe(function (data) {
                    console.log('data', data);
                }, function (error) {
                });
            }
        };
    }
    InviteFriends.prototype.ngOnInit = function () {
        this.inviteForm = this._fb.group({
            emailString: new FormControl("", Validators.compose([Validators.required, this.formService.validateEmailString]))
        });
    };
    return InviteFriends;
}());
InviteFriends = tslib_1.__decorate([
    Component({
        selector: 'invite-friends',
        styleUrls: [
            'invite-friends.style.scss'
        ],
        templateUrl: 'invite-friends.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [UserService, FormBuilder, FormService])
], InviteFriends);
export { InviteFriends };
//# sourceMappingURL=invite-friends.component.js.map