import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../shared/services/user.service';

@Component({
	selector: 'my-downline',
	styleUrls: [
		'my-downline.style.scss'
	],
	templateUrl: 'my-downline.template.html'
})
export class MyDownline {
	@Input('levelData') level;
	constructor(public userService: UserService) {

	}

	ngOnInit() {
		console.log(this.level);
	}
}
