import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { UserService } from '../shared/services/user.service';
var MyDownline = (function () {
    function MyDownline(userService) {
        this.userService = userService;
    }
    MyDownline.prototype.ngOnInit = function () {
        console.log(this.level);
    };
    return MyDownline;
}());
tslib_1.__decorate([
    Input('levelData'),
    tslib_1.__metadata("design:type", Object)
], MyDownline.prototype, "level", void 0);
MyDownline = tslib_1.__decorate([
    Component({
        selector: 'my-downline',
        styleUrls: [
            'my-downline.style.scss'
        ],
        templateUrl: 'my-downline.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [UserService])
], MyDownline);
export { MyDownline };
//# sourceMappingURL=my-downline.component.js.map