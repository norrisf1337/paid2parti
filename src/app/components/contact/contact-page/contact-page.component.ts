import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';

@Component({
  selector: 'faq',
  entryComponents: [],
  styleUrls: [
    'contact-page.style.scss'
  ],
  providers: [],
  templateUrl: 'contact-page.template.html'
})
export class ContactPage {
	constructor (public hs: HeaderService) {
		this.hs.resetHeader();
		this.hs.setHeader('single', 'Contact Us', '');
	}
}
