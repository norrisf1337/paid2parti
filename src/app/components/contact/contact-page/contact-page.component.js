import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
var ContactPage = (function () {
    function ContactPage(hs) {
        this.hs = hs;
        this.hs.resetHeader();
        this.hs.setHeader('single', 'Contact Us', '');
    }
    return ContactPage;
}());
ContactPage = tslib_1.__decorate([
    Component({
        selector: 'faq',
        entryComponents: [],
        styleUrls: [
            'contact-page.style.scss'
        ],
        providers: [],
        templateUrl: 'contact-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService])
], ContactPage);
export { ContactPage };
//# sourceMappingURL=contact-page.component.js.map