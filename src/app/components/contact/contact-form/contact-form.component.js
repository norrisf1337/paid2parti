import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { HeaderService } from '../../shared/services/header.service';
import { FormService } from '../../shared/services/form.service';
import { UserService } from '../../shared/services/user.service';
import { AlertService } from '../../alerts/';
var ContactForm = (function () {
    function ContactForm(hs, _fb, formService, userService, alertService) {
        var _this = this;
        this.hs = hs;
        this._fb = _fb;
        this.formService = formService;
        this.userService = userService;
        this.alertService = alertService;
        this.contact = function (data, valid) {
            if (valid) {
                _this.userService.contact(data)
                    .subscribe(function (data) {
                    console.log(data);
                    _this.alertService.success('Message submitted successfully');
                }, function (error) {
                    _this.alertService.error('Unable to deliver message, please try again later');
                });
            }
        };
    }
    ContactForm.prototype.ngOnInit = function () {
        this.contactForm = this._fb.group({
            name: new FormControl("", Validators.compose([Validators.required, this.formService.nameFormat])),
            email: new FormControl("", Validators.compose([Validators.required, this.formService.mailFormat])),
            message: new FormControl("", Validators.compose([Validators.required])),
        });
    };
    return ContactForm;
}());
ContactForm = tslib_1.__decorate([
    Component({
        selector: 'contact-form',
        entryComponents: [],
        styleUrls: [
            'contact-form.style.scss'
        ],
        providers: [],
        templateUrl: 'contact-form.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService, FormBuilder, FormService, UserService, AlertService])
], ContactForm);
export { ContactForm };
//# sourceMappingURL=contact-form.component.js.map