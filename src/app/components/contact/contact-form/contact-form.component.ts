import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HeaderService } from '../../shared/services/header.service';
import { FormService } from '../../shared/services/form.service';
import { UserService } from '../../shared/services/user.service';
import { AlertService } from '../../alerts/'

@Component({
  selector: 'contact-form',
  entryComponents: [],
  styleUrls: [
    'contact-form.style.scss'
  ],
  providers: [],
  templateUrl: 'contact-form.template.html'
})
export class ContactForm {
	public contactForm: FormGroup;
		constructor (public hs: HeaderService, public _fb: FormBuilder, public formService: FormService, public userService: UserService, public alertService: AlertService) {
	}

	ngOnInit () {
	this.contactForm = this._fb.group({
      name: new FormControl("", Validators.compose([Validators.required, this.formService.nameFormat])),
      email: new FormControl("", Validators.compose([Validators.required, this.formService.mailFormat])),
      message: new FormControl("", Validators.compose([Validators.required])),
    });
	}

	 public contact = (data: any, valid: boolean) => {
    if (valid) {
      this.userService.contact(data)
        .subscribe(
        data => {
          console.log(data);
          this.alertService.success('Message submitted successfully');
        },
        error => {
          this.alertService.error('Unable to deliver message, please try again later');
        })
    }
  }
}
