import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TicketService } from '../../shared/services/ticket.service';
import { UserService } from '../../shared/services/user.service';
import * as _ from 'lodash';

@Component({
	selector: 'my-network',
	entryComponents: [],
	styleUrls: [
		'my-network-page.style.scss'
	],
	providers: [],
	templateUrl: 'my-network-page.template.html'
})
export class MyNetworkPage {
	public loaded = false;
	public levels = [
		{ name: 'Level 1', people: 23, credits: 1000},
		{ name: 'Level 2', people: 44, credits: 1000},
		{ name: 'Level 3', people: 55, credits: 1000},
		{ name: 'Level 4', people: 77, credits: 1000},
		{ name: 'Level 5', people: 88, credits: 1000}
	];

	constructor(private route: ActivatedRoute, private router: Router, public ticketService: TicketService, public userService: UserService, private ref: ChangeDetectorRef) {

	}

	ngOnInit() {
		this.loaded = true;
	}

}
