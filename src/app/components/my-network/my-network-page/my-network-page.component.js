import * as tslib_1 from "tslib";
import { Component, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TicketService } from '../../shared/services/ticket.service';
import { UserService } from '../../shared/services/user.service';
import { HeaderService } from '../../shared/services/header.service';
var MyNetworkPage = (function () {
    function MyNetworkPage(route, router, ticketService, userService, hs, ref) {
        this.route = route;
        this.router = router;
        this.ticketService = ticketService;
        this.userService = userService;
        this.hs = hs;
        this.ref = ref;
        this.levels = [
            { name: 'Level 1', people: 23, credits: 1000 },
            { name: 'Level 2', people: 44, credits: 1000 },
            { name: 'Level 3', people: 55, credits: 1000 },
            { name: 'Level 4', people: 77, credits: 1000 },
            { name: 'Level 5', people: 88, credits: 1000 }
        ];
        this.hs.resetHeader();
        this.hs.setHeader('single', 'My Network', '');
    }
    MyNetworkPage.prototype.ngOnInit = function () {
    };
    return MyNetworkPage;
}());
MyNetworkPage = tslib_1.__decorate([
    Component({
        selector: 'my-network',
        entryComponents: [],
        styleUrls: [
            'my-network-page.style.scss'
        ],
        providers: [],
        templateUrl: 'my-network-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [ActivatedRoute, Router, TicketService, UserService, HeaderService, ChangeDetectorRef])
], MyNetworkPage);
export { MyNetworkPage };
//# sourceMappingURL=my-network-page.component.js.map