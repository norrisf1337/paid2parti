import * as tslib_1 from "tslib";
import { Component, trigger, state, animate, style, transition } from '@angular/core';
import { AlertService } from './alert.service';
var AlertComponent = (function () {
    function AlertComponent(alertService) {
        this.alertService = alertService;
    }
    AlertComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.alertService.getMessage().subscribe(function (message) {
            _this.message = message;
            _this.visibility = 'shown';
            console.log('Watched');
            setTimeout(function () {
                _this.visibility = 'hidden';
                console.log('False');
            }, 5000);
        });
    };
    return AlertComponent;
}());
AlertComponent = tslib_1.__decorate([
    Component({
        selector: 'alert',
        styleUrls: [
            'alert.style.scss'
        ],
        templateUrl: 'alert.component.html',
        animations: [
            trigger('visibilityChanged', [
                state('shown', style({ opacity: 1 })),
                state('hidden', style({ opacity: 0 })),
                transition('shown => hidden', animate('600ms')),
                transition('hidden => shown', animate('300ms')),
            ])
        ]
    }),
    tslib_1.__metadata("design:paramtypes", [AlertService])
], AlertComponent);
export { AlertComponent };
//# sourceMappingURL=alert.component.js.map