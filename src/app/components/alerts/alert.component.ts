import { Component, OnInit, trigger, state, animate, style, transition } from '@angular/core';
import { AlertService } from './alert.service';

@Component({
    selector: 'alert',
    styleUrls: [
        'alert.style.scss'
    ],
    templateUrl: 'alert.component.html',
    animations: [
        trigger('visibilityChanged', [
            state('shown', style({ opacity: 1 })),
            state('hidden', style({ opacity: 0 })),
            transition('shown => hidden', animate('600ms')),
            transition('hidden => shown', animate('300ms')),
        ])
    ]
})

export class AlertComponent {
    message: any;
    visibility: any;
    constructor(private alertService: AlertService) { }

    ngOnInit() {
        this.alertService.getMessage().subscribe(message => {
            this.message = message;
            this.visibility = 'shown';
            console.log('Watched');
            setTimeout(() => {
                this.visibility = 'hidden';
                console.log('False');
            }, 5000)
        });
    }
}
