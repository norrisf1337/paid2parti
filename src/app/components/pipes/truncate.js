import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
var TruncatePipe = (function () {
    function TruncatePipe() {
    }
    TruncatePipe.prototype.transform = function (value, args) {
        var limit = parseInt(args, 10);
        var trail = '...';
        return value.length > limit ? value.substring(0, limit) + trail : value;
    };
    return TruncatePipe;
}());
TruncatePipe = tslib_1.__decorate([
    Pipe({ name: 'truncate' })
], TruncatePipe);
export { TruncatePipe };
//# sourceMappingURL=truncate.js.map