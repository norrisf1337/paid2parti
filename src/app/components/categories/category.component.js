import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { UserService } from '../shared/services/user.service';
import { AlertService } from '../alerts/';
import * as _ from 'lodash';
var CategoriesComponent = (function () {
    function CategoriesComponent(userService, alertService) {
        var _this = this;
        this.userService = userService;
        this.alertService = alertService;
        this.catType = 'list';
        this.categories = [
            { name: 'Dances & Parties', value: 'Dances', selected: true, image: 'assets/images/category-dances.jpg' },
            { name: 'Sports', value: 'Sports', selected: true, image: 'assets/images/category-sports.jpg' },
            { name: 'Theatre', value: 'Theatre', selected: true, image: 'assets/images/category-theatre.jpg' },
            { name: 'Business & Networking', value: 'Business', selected: true, image: 'assets/images/category-bus.jpg' },
            { name: 'Concerts & Music', value: 'Concerts', selected: true, image: 'assets/images/category-concert.jpg' },
            { name: 'Other', value: 'Other', selected: true, image: 'assets/images/category-other.jpg' },
        ];
        this.interests = [];
        this.toggleSelected = function (category) {
            category.selected = (category.selected) ? false : true;
            _this.setSelected();
        };
        this.setAfterData = function (data) {
            var dataMatch = data.join(' ');
            _this.categories = _this.categories.map(function (x) {
                x.selected = (data.indexOf(x.value) > -1) ? true : false;
                return x;
            });
            _this.setSelected();
        };
        this.setSelected = function () {
            _this.interests = _this.categories.map(function (x) {
                if (x.selected) {
                    return x.value;
                }
                else {
                    return undefined;
                }
            });
            _this.interests = _.without(_this.interests, undefined);
            console.log(_this.interests);
        };
        this.setInterests = function () {
            if (true) {
                _this.userService.setDetails({ personal: { interests: _this.interests } })
                    .subscribe(function (data) {
                    _this.alertService.success('Interests have been updated.');
                }, function (error) {
                    _this.alertService.error('There was a problem updating interests, Please contact support or try again later.');
                });
            }
        };
    }
    CategoriesComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.catType !== 'list') {
            this.userService.getDetails('personal.interests')
                .subscribe(function (data) {
                _this.setAfterData(data.personal.interests);
            }, function (error) {
            });
        }
    };
    return CategoriesComponent;
}());
tslib_1.__decorate([
    Input('catType'),
    tslib_1.__metadata("design:type", Object)
], CategoriesComponent.prototype, "catType", void 0);
CategoriesComponent = tslib_1.__decorate([
    Component({
        selector: 'categories',
        styleUrls: [
            'categories.style.scss'
        ],
        templateUrl: 'category.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [UserService, AlertService])
], CategoriesComponent);
export { CategoriesComponent };
//# sourceMappingURL=category.component.js.map