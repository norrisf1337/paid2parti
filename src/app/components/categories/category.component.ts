import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../shared/services/user.service';
import { AlertService } from '../alerts/';
import * as _ from 'lodash';

@Component({
	selector: 'categories',
	styleUrls: [
		'categories.style.scss'
	],
	templateUrl: 'category.template.html'
})
export class CategoriesComponent {
	@Input('catType') catType = 'list';

	public categories = [
		{ name: 'Dances & Parties', value: 'Dances', selected: true, image: 'assets/images/category-dances.jpg' },
		{ name: 'Sports', value: 'Sports', selected: true, image: 'assets/images/category-sports.jpg' },
		{ name: 'Theatre', value: 'Theatre', selected: true, image: 'assets/images/category-theatre.jpg' },
		{ name: 'Business & Networking', value: 'Business', selected: true, image: 'assets/images/category-bus.jpg' },
		{ name: 'Concerts & Music', value: 'Concerts', selected: true, image: 'assets/images/category-concert.jpg' },
		{ name: 'Other', value: 'Other', selected: true, image: 'assets/images/category-other.jpg' },
	];

	public interests = [];

	constructor(public userService: UserService, public alertService: AlertService) {

	}

	ngOnInit() {
		if (this.catType !== 'list') {
		this.userService.getDetails('personal.interests')
			.subscribe(
			data => {
				this.setAfterData(data.personal.interests);
			},
			error => {
			})
		}

	}

	public toggleSelected = (category): void => {
		category.selected = (category.selected) ? false : true;
		this.setSelected();
	}

	public setAfterData = (data): void => {
		let dataMatch = data.join(' ');
		this.categories = this.categories.map((x) => {
			x.selected = (data.indexOf(x.value) > -1) ? true : false;
			return x;
		});
		this.setSelected();
	}

	public setSelected = (): void => {
		this.interests = this.categories.map((x): string => {
			if (x.selected) {
				return x.value;
			} else {
				return undefined;
			}
		});

		this.interests = _.without(this.interests, undefined);
		console.log(this.interests)
	}

	public setInterests = (): void => {
		if (true) {
			this.userService.setDetails({ personal: { interests: this.interests } })
				.subscribe(
				data => {
					this.alertService.success('Interests have been updated.');
				},
				error => {
					this.alertService.error('There was a problem updating interests, Please contact support or try again later.');
				})
		}
	}
}
