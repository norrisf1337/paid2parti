import { Component } from '@angular/core';
import { UserService } from '../shared/services/user.service';

@Component({
	selector: 'footer',
	styleUrls: [
		'footer.style.scss'
	],
	templateUrl: 'footer.template.html'
})
export class Footer {
	constructor(public userService: UserService) {

	}

	public isLogged = () => {
		return this.userService.isLogged();
	}
}
