import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { UserService } from '../shared/services/user.service';
var Footer = (function () {
    function Footer(userService) {
        var _this = this;
        this.userService = userService;
        this.isLogged = function () {
            return _this.userService.isLogged();
        };
    }
    return Footer;
}());
Footer = tslib_1.__decorate([
    Component({
        selector: 'footer',
        styleUrls: [
            'footer.style.scss'
        ],
        templateUrl: 'footer.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [UserService])
], Footer);
export { Footer };
//# sourceMappingURL=footer.component.js.map