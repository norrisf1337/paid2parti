import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GooglePlaceDirective } from './ng2-google-place.directive';
import { GooglePlaceService } from './ng2-google-place.service';
var GooglePlaceModule = (function () {
    function GooglePlaceModule() {
    }
    return GooglePlaceModule;
}());
GooglePlaceModule = tslib_1.__decorate([
    NgModule({
        imports: [CommonModule],
        providers: [GooglePlaceService],
        declarations: [GooglePlaceDirective],
        exports: [CommonModule, GooglePlaceDirective]
    })
], GooglePlaceModule);
export { GooglePlaceModule };
//# sourceMappingURL=ng2-google-place.module.js.map