import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
import { UserService } from '../../shared/services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'promoter-details-page',
  entryComponents: [],
  styleUrls: [
    'promoter.details.page.style.scss'
  ],
  providers: [],
  templateUrl: 'promoter.details.page.template.html'
})
export class PromoterDetailsPage {
  public promoter = undefined;
	constructor(public userService: UserService, private route: ActivatedRoute, private router: Router) {
	}

  ngOnInit() {
    let pid: string = this.route.snapshot.params['pid'];

    this.userService.getPromoter(pid)
      .subscribe(
      data => {
        this.promoter = data.promoter[0];
        console.log('p', this.promoter);
      },
      error => {
      })

  }

}
