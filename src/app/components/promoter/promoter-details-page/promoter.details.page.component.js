import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
var PromoterDetailsPage = (function () {
    function PromoterDetailsPage(userService, route, router) {
        this.userService = userService;
        this.route = route;
        this.router = router;
        this.promoter = undefined;
    }
    PromoterDetailsPage.prototype.ngOnInit = function () {
        var _this = this;
        var pid = this.route.snapshot.params['pid'];
        this.userService.getPromoter(pid)
            .subscribe(function (data) {
            _this.promoter = data.promoter[0];
            console.log('p', _this.promoter);
        }, function (error) {
        });
    };
    return PromoterDetailsPage;
}());
PromoterDetailsPage = tslib_1.__decorate([
    Component({
        selector: 'promoter-details-page',
        entryComponents: [],
        styleUrls: [
            'promoter.details.page.style.scss'
        ],
        providers: [],
        templateUrl: 'promoter.details.page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [UserService, ActivatedRoute, Router])
], PromoterDetailsPage);
export { PromoterDetailsPage };
//# sourceMappingURL=promoter.details.page.component.js.map