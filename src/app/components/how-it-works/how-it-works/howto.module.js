import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HowToComponent } from './howto.component';
var HowToModule = (function () {
    function HowToModule() {
    }
    return HowToModule;
}());
HowToModule = tslib_1.__decorate([
    NgModule({
        imports: [CommonModule],
        declarations: [
            HowToComponent
        ],
        exports: [HowToComponent]
    })
], HowToModule);
export { HowToModule };
//# sourceMappingURL=howto.module.js.map