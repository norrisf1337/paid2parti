import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HowToComponent } from './howto.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    HowToComponent
  ],
  exports: [HowToComponent]
})
export class HowToModule {}
