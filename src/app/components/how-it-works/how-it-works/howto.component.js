import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var HowToComponent = (function () {
    function HowToComponent() {
    }
    return HowToComponent;
}());
HowToComponent = tslib_1.__decorate([
    Component({
        selector: 'howto',
        styleUrls: [
            'howto.style.scss'
        ],
        templateUrl: 'howto.template.html'
    })
], HowToComponent);
export { HowToComponent };
//# sourceMappingURL=howto.component.js.map