import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';

@Component({
  selector: 'how-it-works',
  entryComponents: [],
  styleUrls: [
    'how-it-works-page.style.scss'
  ],
  providers: [],
  templateUrl: 'how-it-works-page.template.html'
})
export class HowItWorksPage {
	constructor (public hs: HeaderService){
		this.hs.resetHeader();
		this.hs.setHeader('single', 'How It Works', '');
	}
}
