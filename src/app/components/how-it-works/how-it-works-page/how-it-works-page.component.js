import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
var HowItWorksPage = (function () {
    function HowItWorksPage(hs) {
        this.hs = hs;
        this.hs.resetHeader();
        this.hs.setHeader('single', 'How It Works', '');
    }
    return HowItWorksPage;
}());
HowItWorksPage = tslib_1.__decorate([
    Component({
        selector: 'how-it-works',
        entryComponents: [],
        styleUrls: [
            'how-it-works-page.style.scss'
        ],
        providers: [],
        templateUrl: 'how-it-works-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService])
], HowItWorksPage);
export { HowItWorksPage };
//# sourceMappingURL=how-it-works-page.component.js.map