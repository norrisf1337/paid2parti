import { Component, OnInit, ContentChildren, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HeaderService } from '../../shared/services/header.service';
import { EventService } from '../../shared/services/events.service';
import * as moment from 'moment';

@Component({
  selector: 'event-details-page',
  styleUrls: [
    'event.details.page.style.scss'
  ],
  providers: [],
  templateUrl: 'event.details.page.template.html'
})
export class EventDetailsPage {
  public loaded: boolean = false;
  public event: event;
  public showMap: boolean = false;
  constructor(private route: ActivatedRoute, private router: Router, private eventService: EventService, public hs: HeaderService) {
  }

  ngOnInit() {
    this.hs.resetHeader();
    let id: string = this.route.snapshot.params['id'];

    this.eventService.getById(id)
      .subscribe(
      data => {
        this.event = data;
        this.loaded = true;
      },
      error => {
      });

  }

  getBestPrice(tickets) {
    return '$25';
  }

  public toggleMap = () => {
    this.showMap = (this.showMap) ? false : true;
  }
}
