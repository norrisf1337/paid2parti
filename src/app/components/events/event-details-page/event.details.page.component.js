import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HeaderService } from '../../shared/services/header.service';
import { EventService } from '../../shared/services/events.service';
var EventDetailsPage = (function () {
    function EventDetailsPage(route, router, eventService, hs) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.eventService = eventService;
        this.hs = hs;
        this.showMap = false;
        this.toggleMap = function () {
            _this.showMap = (_this.showMap) ? false : true;
        };
    }
    EventDetailsPage.prototype.ngOnInit = function () {
        var _this = this;
        this.hs.resetHeader();
        var id = this.route.snapshot.params['id'];
        this.eventService.getById(id)
            .subscribe(function (data) {
            _this.event = data;
        }, function (error) {
        });
    };
    EventDetailsPage.prototype.getBestPrice = function (tickets) {
        return '$25';
    };
    return EventDetailsPage;
}());
EventDetailsPage = tslib_1.__decorate([
    Component({
        selector: 'event-details-page',
        styleUrls: [
            'event.details.page.style.scss'
        ],
        providers: [],
        templateUrl: 'event.details.page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [ActivatedRoute, Router, EventService, HeaderService])
], EventDetailsPage);
export { EventDetailsPage };
//# sourceMappingURL=event.details.page.component.js.map