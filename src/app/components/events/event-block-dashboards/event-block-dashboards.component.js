import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EventService } from '../../shared/services/events.service';
import * as _ from 'lodash';
var EventBlockDashboards = (function () {
    function EventBlockDashboards(route, router, eventService) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.eventService = eventService;
        this.event = {};
        this.totalEarnings = 0;
        this.getEarnings = function (event) {
            if (!_.isEmpty(_this.event.tickets)) {
                var earnings = _this.event.tickets.reduce(function (x, y) {
                    console.log(x.earnings, y.earnings);
                    return x.earnings + y.earnings;
                });
                return earnings;
            }
            else {
                return 0;
            }
        };
        this.editEvent = function (event) {
            _this.router.navigate(['../edit/', event._id,], { relativeTo: _this.route });
        };
        this.adminEvent = function (event) {
            _this.router.navigate(['../administer/', event._id,], { relativeTo: _this.route });
        };
        this.attendees = function (event) {
            _this.router.navigate(['../attendees/', event._id,], { relativeTo: _this.route });
        };
        this.notifications = function (event) {
            _this.router.navigate(['../notifications/', event._id,], { relativeTo: _this.route });
        };
        this.deleteEvent = function (event) {
            _this.eventService.deleteEvent(event._id)
                .subscribe(function (data) {
                console.log('event', data);
            }, function (error) {
            });
        };
    }
    EventBlockDashboards.prototype.ngOnInit = function () {
        var _this = this;
        _.forEach(this.event.tickets, function (j, k) {
            j.sold = j.qty - j.available;
            j.earnings = j.cost * j.sold;
            _this.totalEarnings += j.earnings;
            console.log('o', j);
        });
    };
    return EventBlockDashboards;
}());
tslib_1.__decorate([
    Input('event'),
    tslib_1.__metadata("design:type", Object)
], EventBlockDashboards.prototype, "event", void 0);
EventBlockDashboards = tslib_1.__decorate([
    Component({
        selector: 'event-block-dashboards',
        styleUrls: [
            'event-block-dashboards.style.scss'
        ],
        templateUrl: 'event-block-dashboards.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [ActivatedRoute, Router, EventService])
], EventBlockDashboards);
export { EventBlockDashboards };
//# sourceMappingURL=event-block-dashboards.component.js.map