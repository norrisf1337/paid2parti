import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { EventService } from '../../shared/services/events.service';
import * as _ from 'lodash';

@Component({
  selector: 'event-block-dashboards',
  styleUrls: [
    'event-block-dashboards.style.scss'
  ],
  templateUrl: 'event-block-dashboards.template.html'
})
export class EventBlockDashboards {
  @Input('event') event: any = {};
  public totalEarnings: number = 0;
  constructor(private route: ActivatedRoute, private router: Router, public eventService: EventService) {

  }

  ngOnInit() {
    _.forEach(this.event.tickets, (j, k) => {
      j.sold = j.qty - j.available;
      j.earnings = j.cost * j.sold;
      this.totalEarnings += j.earnings;
      console.log('o', j);
    });
  }

  public getEarnings = (event) => {
    if(!_.isEmpty(this.event.tickets)){
      let earnings = this.event.tickets.reduce((x, y) => {
      console.log(x.earnings, y.earnings)
      return x.earnings + y.earnings;
    });
    return earnings;
    } else {
      return 0;
    }

  }

  public editEvent = (event) => {
    this.router.navigate(['../edit/', event._id,], { relativeTo: this.route });
  }

  public adminEvent = (event) => {
    this.router.navigate(['../administer/', event._id,], { relativeTo: this.route });
  }

  public attendees = (event) => {
    this.router.navigate(['../attendees/', event._id,], { relativeTo: this.route });
  }
  public notifications = (event) => {
    this.router.navigate(['../notifications/', event._id,], { relativeTo: this.route });
  }
  public deleteEvent = (event) => {
    this.eventService.deleteEvent(event._id)
         .subscribe(
         data => {
            console.log('event', data);
         },
         error => {
         });
  }
}
