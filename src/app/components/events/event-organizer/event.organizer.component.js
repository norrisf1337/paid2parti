import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../shared/services/user.service';
import { AlertService } from '../../alerts/alert.service';
import * as _ from 'lodash';
var EventOrganizer = (function () {
    function EventOrganizer(_fb, userService, alertService) {
        var _this = this;
        this._fb = _fb;
        this.userService = userService;
        this.alertService = alertService;
        this.promoter = '';
        this.loaded = undefined;
        this.hideRating = false;
        this.rating = [
            { 'name': '5', 'value': '5' },
            { 'name': '4', 'value': '4' },
            { 'name': '3', 'value': '3' },
            { 'name': '2', 'value': '2' },
            { 'name': '1', 'value': '1' }
        ];
        this.rateOrganizer = function () {
            console.log(_this.ratingForm.value);
            _this.userService.ratePromoter({ userId: _this.userService.user.uid, rating: _this.ratingForm.value.ratings.value, promoterId: _this.promoter._id })
                .subscribe(function (data) {
                _this.alertService.success('Rating has been made');
            }, function (error) {
                _this.alertService.error('There has been an error');
            });
        };
        console.log('prom', this);
    }
    EventOrganizer.prototype.ngOnInit = function () {
        var _this = this;
        this.ratingForm = this._fb.group({
            ratings: new FormControl("", Validators.compose([Validators.required]))
        });
        if (!_.isUndefined(this.loaded)) {
            this.promoter = this.loaded;
            console.log('ld', this.loaded);
            this.userService.getRating({ promoterId: this.loaded._id })
                .subscribe(function (data) {
                console.log('d', data);
                _this.proRating = data.rating;
            }, function (error) {
            });
        }
        if (_.isUndefined(this.loaded)) {
            this.userService.getPromoter(this.promoter)
                .subscribe(function (data) {
                _this.promoter = data.promoter[0];
            }, function (error) {
            });
            this.userService.getRating({ promoterId: this.promoter[0] })
                .subscribe(function (data) {
                console.log('d', data);
                _this.proRating = data.rating;
            }, function (error) {
            });
        }
    };
    return EventOrganizer;
}());
tslib_1.__decorate([
    Input('promoter'),
    tslib_1.__metadata("design:type", Object)
], EventOrganizer.prototype, "promoter", void 0);
tslib_1.__decorate([
    Input('loaded'),
    tslib_1.__metadata("design:type", Object)
], EventOrganizer.prototype, "loaded", void 0);
tslib_1.__decorate([
    Input('hideRating'),
    tslib_1.__metadata("design:type", Boolean)
], EventOrganizer.prototype, "hideRating", void 0);
EventOrganizer = tslib_1.__decorate([
    Component({
        selector: 'event-organizer',
        styleUrls: [
            'event.organizer.style.scss'
        ],
        templateUrl: 'event.organizer.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [FormBuilder, UserService, AlertService])
], EventOrganizer);
export { EventOrganizer };
//# sourceMappingURL=event.organizer.component.js.map