import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { UserService } from '../../shared/services/user.service';
import { AlertService } from '../../alerts/alert.service';
import * as _ from 'lodash';
@Component({
	selector: 'event-organizer',
	styleUrls: [
		'event.organizer.style.scss'
	],
	templateUrl: 'event.organizer.template.html'
})
export class EventOrganizer {
	@Input('promoter') promoter: any = '';
	@Input('loaded') loaded: any = undefined;
	@Input('hideRating') hideRating: boolean = false;

	public rating: any = [
		{ 'name': '5', 'value': '5' },
		{ 'name': '4', 'value': '4' },
		{ 'name': '3', 'value': '3' },
		{ 'name': '2', 'value': '2' },
		{ 'name': '1', 'value': '1' }
	];
	public ratingForm: FormGroup;
	public proRating: number;
	public loading: boolean = true;

	constructor(public _fb: FormBuilder, public userService: UserService, public alertService: AlertService) {
	}

	ngOnInit() {
		this.ratingForm = this._fb.group({
			ratings: new FormControl("", Validators.compose([Validators.required]))
		});
		if (!_.isUndefined(this.loaded)) {
			this.promoter = this.loaded;
				this.userService.getRating({promoterId: this.loaded._id})
				.subscribe(
				data => {
					this.proRating = data.rating;
				},
				error => {
				});
		}
		if (_.isUndefined(this.loaded)) {
			this.userService.getPromoter(this.promoter)
				.subscribe(
				data => {
					this.promoter = data.promoter[0];
					this.loading = false;
				},
				error => {
				});

			this.userService.getRating({promoterId: this.promoter[0]})
				.subscribe(
				data => {
					console.log('d', data)
					this.proRating = data.rating;
				},
				error => {
				});
		}

	}

	public rateOrganizer = (): void => {
		this.userService.ratePromoter({ userId: this.userService.user.uid, rating: parseInt(this.ratingForm.value.ratings), promoterId: this.promoter._id })
			.subscribe(
			data => {
				this.alertService.success('Rating has been made');
			},
			error => {
				this.alertService.error('There has been an error');
			});
	}
}
