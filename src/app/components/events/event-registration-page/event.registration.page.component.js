import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { HeaderService } from '../../shared/services/header.service';
import { EventService } from '../../shared/services/events.service';
import { UserService } from '../../shared/services/user.service';
import { TicketService } from '../../shared/services/ticket.service';
import * as moment from 'moment';
var EventRegistrationPage = (function () {
    function EventRegistrationPage(route, router, _fb, eventService, hs, ticketService, userService) {
        var _this = this;
        this.route = route;
        this.router = router;
        this._fb = _fb;
        this.eventService = eventService;
        this.hs = hs;
        this.ticketService = ticketService;
        this.userService = userService;
        this.submitted = false;
        this.showMap = true;
        this.confirmation = false;
        this.credits = 0;
        this.hasCredits = false;
        this.showCredits = false;
        this.toggleCredits = function () {
            if (_this.hasCredits) {
                _this.showCredits = (_this.showCredits) ? false : true;
            }
        };
        this.register = function (model, isValid, type) {
            _this.submitted = true;
            if (isValid) {
                var userDetails = {
                    email: model.email,
                    firstName: model.firstname,
                    lastName: model.lastname,
                    address: model.address,
                    city: model.city,
                    country: model.country,
                    province: model.province,
                    zip: model.zip
                };
            }
        };
        this.viewTickets = function () {
            _this.router.navigate(['/account/my-tickets']);
        };
        this.toggleMap = function () {
            _this.showMap = (_this.showMap) ? false : true;
        };
    }
    EventRegistrationPage.prototype.mailFormat = function (control) {
        var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (control.value != "" && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
            return { "incorrectMailFormat": true };
        }
        return null;
    };
    EventRegistrationPage.prototype.nameFormat = function (control) {
        var NAME_REGEXP = /^[a-zA-Z ]{1,30}$/;
        if (control.value != "" && (control.value.length <= 3 || !NAME_REGEXP.test(control.value))) {
            return { "incorrectName": true };
        }
        return null;
    };
    EventRegistrationPage.prototype.ngOnInit = function () {
        var _this = this;
        var events = JSON.parse(localStorage.getItem('events')) || [];
        var id = this.route.snapshot.params['id'];
        var ticketId = this.route.snapshot.params['tid'];
        var ticketQty = +this.route.snapshot.params['tqty'];
        this.eventService.getById(id)
            .subscribe(function (data) {
            _this.event = data;
            _this.hs.headerType = 'double';
            _this.hs.headerTitle = data.basicDetails.name;
            _this.hs.subTitle = moment(data.basicDetails.startDate).format('MMM DD YYYY hh:mm A') + ' @ ' + data.basicDetails.venue;
            var ticket = _this.event.tickets.filter(function (ticket) {
                return ticket._id === ticketId;
            });
            if (ticket.length) {
                _this.ticket = ticket[0];
                _this.ticket.qty = ticketQty;
                _this.ticket.price = _this.ticket.cost * _this.ticket.qty;
            }
            _this.hasCredits = (_this.credits > _this.ticket.price) ? true : false;
        }, function (error) {
        });
        this.regForm = this._fb.group({
            email: new FormControl("", Validators.compose([Validators.required, this.mailFormat])),
            firstname: new FormControl("", Validators.compose([Validators.required, this.nameFormat])),
            lastname: new FormControl("", Validators.compose([Validators.required, this.nameFormat])),
            address: ['', Validators.required],
            city: ['', Validators.required],
            country: ['', Validators.required],
            province: ['', Validators.required],
            zip: ['', Validators.required],
        });
    };
    EventRegistrationPage.prototype.ngAfterViewInit = function () {
        var _this = this;
        console.log('that', this);
        var that = this;
        setTimeout(function () {
            paypal.Button.render({
                env: 'sandbox',
                client: {
                    sandbox: 'AUqUQsH6g_Fp6CdfWi4D6Hq3kK0AupyLiWRbVg0Uhx-AZ7KuzYcNYaBEMsEvGuvHQXi_Z3kohxCg9Cmn',
                    production: 'xxxxxxxxx'
                },
                payment: function () {
                    console.log('this', _this);
                    var env = 'sandbox';
                    var client = {
                        sandbox: 'AUqUQsH6g_Fp6CdfWi4D6Hq3kK0AupyLiWRbVg0Uhx-AZ7KuzYcNYaBEMsEvGuvHQXi_Z3kohxCg9Cmn'
                    };
                    var transaction = {
                        transactions: [
                            {
                                amount: { total: _this.ticket.price.toString(), currency: _this.ticket.currency }
                            }
                        ]
                    };
                    return paypal.rest.payment.create(env, client, transaction);
                },
                commit: true,
                onAuthorize: function (data, actions) {
                    return actions.payment.execute().then(function () {
                        console.log('PAYMENT SUCCESS', data, actions);
                        actions.payment.get().then(function (data) {
                            _this.confInfo = data.payer.payer_info;
                            _this.confirmation = true;
                            _this.ticketService.purchaseTickets(_this.route.snapshot.params['id'], _this.userService.user.uid, _this.ticket._id, _this.ticket.qty, _this.ticket.cost, _this.event.owner)
                                .subscribe(function (data) {
                                console.log(data);
                            }, function (error) {
                            });
                        });
                    });
                },
                onCancel: function (data, actions) {
                    console.log('PAYMENT CANCEL');
                },
                onError: function (data, actions) {
                    console.log('PAYMENT ERROR');
                }
            }, '#paypal-button');
        }, 0);
    };
    return EventRegistrationPage;
}());
EventRegistrationPage = tslib_1.__decorate([
    Component({
        selector: 'event-registration-page',
        entryComponents: [],
        styleUrls: [
            'event.registration.page.style.scss'
        ],
        providers: [],
        templateUrl: 'event.registration.page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [ActivatedRoute, Router, FormBuilder, EventService, HeaderService, TicketService, UserService])
], EventRegistrationPage);
export { EventRegistrationPage };
//# sourceMappingURL=event.registration.page.component.js.map