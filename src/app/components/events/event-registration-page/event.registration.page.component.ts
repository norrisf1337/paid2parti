declare var paypal: any
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HeaderService } from '../../shared/services/header.service';
import { EventService } from '../../shared/services/events.service';
import { UserService } from '../../shared/services/user.service';
import { TicketService } from '../../shared/services/ticket.service';
import * as moment from 'moment';

@Component({
  selector: 'event-registration-page',
  entryComponents: [],
  styleUrls: [
    'event.registration.page.style.scss'
  ],
  providers: [],
  templateUrl: 'event.registration.page.template.html'
})
export class EventRegistrationPage {
  public ticket;
  public event;
  public regForm;
  public submitted: boolean = false;
  public showMap: boolean = true;
  public confirmation: boolean = false;
  public confInfo: any;
  public credits: number = 0;
  public hasCredits: boolean = false;
  public showCredits: boolean = false;
  constructor(private route: ActivatedRoute, private router: Router, public _fb: FormBuilder, private eventService: EventService, public hs: HeaderService, public ticketService: TicketService, public userService: UserService) {
  }

  public mailFormat(control) {

    var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (control.value != "" && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
      return { "incorrectMailFormat": true };
    }

    return null;
  }

  public nameFormat(control) {

    var NAME_REGEXP = /^[a-zA-Z ]{1,30}$/;

    if (control.value != "" && (control.value.length <= 3 || !NAME_REGEXP.test(control.value))) {
      return { "incorrectName": true };
    }

    return null;
  }

  ngOnInit() {
    let events: any[] = JSON.parse(localStorage.getItem('events')) || [];
    let id = this.route.snapshot.params['id'];
    let ticketId = this.route.snapshot.params['tid'];
    let ticketQty: number = +this.route.snapshot.params['tqty'];

    this.eventService.getById(id)
      .subscribe(
      data => {
        this.event = data;
        this.hs.headerType = 'double';
        this.hs.headerTitle = data.basicDetails.name;
        this.hs.subTitle = moment(data.basicDetails.startDate).format('MMM DD YYYY hh:mm A') + ' @ ' + data.basicDetails.venue;


        let ticket = this.event.tickets.filter(ticket => {
          return ticket._id === ticketId;
        });

        if (ticket.length) {
          this.ticket = ticket[0];
          this.ticket.qty = ticketQty;
          this.ticket.price = this.ticket.cost * this.ticket.qty;
        }
        this.hasCredits = (this.credits > this.ticket.price) ? true : false;
      },
      error => {
      });


    this.regForm = this._fb.group({
      email: new FormControl("", Validators.compose([Validators.required, this.mailFormat])),
      firstname: new FormControl("", Validators.compose([Validators.required, this.nameFormat])),
      lastname: new FormControl("", Validators.compose([Validators.required, this.nameFormat])),
      address: ['', <any>Validators.required],
      city: ['', <any>Validators.required],
      country: ['', <any>Validators.required],
      province: ['', <any>Validators.required],
      zip: ['', <any>Validators.required],
    });

  }
  ngAfterViewInit() {
    console.log('that', this)
    let that = this;
    setTimeout(() => {
      paypal.Button.render({

        env: 'sandbox', // Optional: specify 'sandbox' environment

        client: {
          sandbox: 'AUqUQsH6g_Fp6CdfWi4D6Hq3kK0AupyLiWRbVg0Uhx-AZ7KuzYcNYaBEMsEvGuvHQXi_Z3kohxCg9Cmn',
          production: 'xxxxxxxxx'
        },

        payment: () => {
          console.log('this', this)
          let env = 'sandbox';
          let client = {
            sandbox: 'AUqUQsH6g_Fp6CdfWi4D6Hq3kK0AupyLiWRbVg0Uhx-AZ7KuzYcNYaBEMsEvGuvHQXi_Z3kohxCg9Cmn'
          };

          let transaction = {
            transactions: [
              {
                amount: { total: this.ticket.price.toString(), currency: this.ticket.currency }
              }
            ]
          };

          return paypal.rest.payment.create(env, client, transaction);
        },
        commit: true, // Optional: show a 'Pay Now' button in the checkout flow
        onAuthorize: (data, actions) => {
          return actions.payment.execute().then(() => {
            console.log('PAYMENT SUCCESS', data, actions);
            actions.payment.get().then((data) => {
              this.confInfo = data.payer.payer_info;
              this.confirmation = true;

              this.ticketService.purchaseTickets(this.route.snapshot.params['id'], this.userService.user.uid, this.ticket._id, this.ticket.qty, this.ticket.cost, this.event.owner)
                .subscribe(
                data => {
                  console.log(data);
                },
                error => {
                });
            });
          });
        },
        onCancel: function(data, actions) {
          console.log('PAYMENT CANCEL');
        },
        onError: function(data, actions) {
          console.log('PAYMENT ERROR');
        }
      }, '#paypal-button');
    }, 0)

  }

  public toggleCredits = () => {
    if (this.hasCredits) {
      this.showCredits = (this.showCredits) ? false : true;
    }
  }

  public register = (model, isValid: boolean, type: string) => {
    this.submitted = true;
    if (isValid) {
      let userDetails = {
        email: model.email,
        firstName: model.firstname,
        lastName: model.lastname,
        address: model.address,
        city: model.city,
        country: model.country,
        province: model.province,
        zip: model.zip
      }
    }
  }

  public viewTickets = () => {
    this.router.navigate(['/account/my-tickets']);
  }
  public toggleMap = () => {
    this.showMap = (this.showMap) ? false : true;
  }
}
