import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { EventService } from '../../shared/services/events.service';
var EventListGrid = (function () {
    function EventListGrid(eventService) {
        this.eventService = eventService;
        this.gridCount = 0;
        this.events = [];
    }
    EventListGrid.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this.eventService);
        this.eventService.getAll()
            .subscribe(function (data) {
            _this.events = data;
        }, function (error) {
        });
    };
    return EventListGrid;
}());
tslib_1.__decorate([
    Input('count'),
    tslib_1.__metadata("design:type", Object)
], EventListGrid.prototype, "gridCount", void 0);
EventListGrid = tslib_1.__decorate([
    Component({
        selector: 'event-loop',
        templateUrl: 'event.listing.grid.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [EventService])
], EventListGrid);
export { EventListGrid };
//# sourceMappingURL=event.listing.grid.component.js.map