import { Component, Input, OnInit } from '@angular/core';
import { EventService } from '../../shared/services/events.service';
import * as moment from 'moment/moment';

@Component({
  selector: 'event-loop',
  templateUrl: 'event.listing.grid.template.html'
})
export class EventListGrid implements OnInit {
	@Input('count') gridCount = 0;
	public events = [];

  constructor(public eventService: EventService){

  }

	ngOnInit(){
      console.log(this.eventService);
      this.eventService.getAll()
        .subscribe(
          data => {
            this.events = data;
          },
          error => {
   	      });
      }
}
