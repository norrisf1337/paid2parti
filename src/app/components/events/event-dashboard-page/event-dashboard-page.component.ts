import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HeaderService } from '../../shared/services/header.service';
import { EventService } from '../../shared/services/events.service';
import * as _ from 'lodash';

@Component({
  selector: 'event-dashboard',
  entryComponents: [],
  styleUrls: [
    'event-dashboard-page.style.scss'
  ],
  providers: [],
  templateUrl: 'event-dashboard-page.template.html'
})
export class EventDashboardPage {
	public tab: string = 'upcoming';
  public status: any;
  public events: any = {
    active: [],
    pending: [],
    published: [],
    finished: [],
  };
  public eventLoaded: boolean = false;
  constructor(private route: ActivatedRoute, private router: Router, public eventService: EventService, public hs: HeaderService) {
    this.hs.headerType = 'single';
    this.hs.headerTitle = 'Event Managment Dashboard';

  }

  ngOnInit() {
    this.tab = "published";
    this.eventService.getMyEvents()
      .subscribe(
      data => {
        this.events = {
          active: _.filter(data, function(o: any) { return o.status === 'active'; }),
          pending: _.filter(data, function(o: any) { return o.status === 'draft'; }),
          published: _.filter(data, function(o: any) { return o.status === 'published'; }),
          finished: _.filter(data, function(o: any) { return o.status === 'finished'; }),
        };
        this.eventLoaded = true;
      },
      error => {
      });
  }

	public changeTab = (tab: string): void => {
		this.tab = tab;
	}
}
