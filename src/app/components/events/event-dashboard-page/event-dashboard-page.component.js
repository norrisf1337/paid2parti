import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HeaderService } from '../../shared/services/header.service';
import { EventService } from '../../shared/services/events.service';
import * as _ from 'lodash';
var EventDashboardPage = (function () {
    function EventDashboardPage(route, router, eventService, hs) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.eventService = eventService;
        this.hs = hs;
        this.tab = 'upcoming';
        this.events = {
            active: [],
            pending: [],
            published: [],
            finished: [],
        };
        this.changeTab = function (tab) {
            _this.tab = tab;
        };
        this.hs.headerType = 'single';
        this.hs.headerTitle = 'Event Managment Dashboard';
    }
    EventDashboardPage.prototype.ngOnInit = function () {
        var _this = this;
        this.eventService.getMyEvents()
            .subscribe(function (data) {
            _this.events = {
                active: _.filter(data, function (o) { return o.status === 'active'; }),
                pending: _.filter(data, function (o) { return o.status === 'draft'; }),
                published: _.filter(data, function (o) { return o.status === 'published'; }),
                finished: _.filter(data, function (o) { return o.status === 'finished'; }),
            };
            console.log('event', _this.events);
        }, function (error) {
        });
        this.tab = "published";
    };
    return EventDashboardPage;
}());
EventDashboardPage = tslib_1.__decorate([
    Component({
        selector: 'event-dashboard',
        entryComponents: [],
        styleUrls: [
            'event-dashboard-page.style.scss'
        ],
        providers: [],
        templateUrl: 'event-dashboard-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [ActivatedRoute, Router, EventService, HeaderService])
], EventDashboardPage);
export { EventDashboardPage };
//# sourceMappingURL=event-dashboard-page.component.js.map