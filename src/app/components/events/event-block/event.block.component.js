import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { Emitter } from '../../shared/services/emitter.service';
var EventBlock = (function () {
    // public favorited: boolean = false;
    function EventBlock(userService, emitter) {
        var _this = this;
        this.userService = userService;
        this.emitter = emitter;
        this.favorited = false;
        this.share = false;
        this.setFavorite = function (id) {
            if (_this.userService.isLogged()) {
                if (!_this.favorited) {
                    _this.userService.setFavorite(id)
                        .subscribe(function (data) {
                        _this.favorited = true;
                    }, function (error) {
                    });
                }
                else {
                    _this.userService.removeFavorite(id)
                        .subscribe(function (data) {
                        _this.favorited = false;
                    }, function (error) {
                    });
                }
            }
            else {
                _this.emitter.emit('register', {});
            }
        };
        this.toggleShare = function () {
            _this.share = (_this.share) ? false : true;
        };
    }
    EventBlock.prototype.ngOnInit = function () {
    };
    EventBlock.prototype.getBestPrice = function (tickets) {
        return '$25';
    };
    return EventBlock;
}());
tslib_1.__decorate([
    Input('eventdetails'),
    tslib_1.__metadata("design:type", Object)
], EventBlock.prototype, "eventdetails", void 0);
tslib_1.__decorate([
    Input('favorite'),
    tslib_1.__metadata("design:type", Object)
], EventBlock.prototype, "favorited", void 0);
EventBlock = tslib_1.__decorate([
    Component({
        selector: 'event-block',
        styleUrls: [
            'event.block.style.scss'
        ],
        templateUrl: 'event.block.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [UserService, Emitter])
], EventBlock);
export { EventBlock };
//# sourceMappingURL=event.block.component.js.map