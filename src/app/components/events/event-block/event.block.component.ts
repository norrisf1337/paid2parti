import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../shared/services/user.service';
import { Emitter } from '../../shared/services/emitter.service';


@Component({
	selector: 'event-block',
	styleUrls: [
		'event.block.style.scss'
	],
	templateUrl: 'event.block.template.html'
})
export class EventBlock {
	@Input('eventdetails') eventdetails;
	@Input('favorite') favorited = false;
	public share: boolean = false;
	// public favorited: boolean = false;
	constructor(public userService: UserService, public emitter: Emitter) {
	}

	ngOnInit() {

	}

	getBestPrice(tickets) {
		return '$25';
	}

	public setFavorite = (id) => {
		if (this.userService.isLogged()) {
			if (!this.favorited) {
				this.userService.setFavorite(id)
					.subscribe(
					data => {
						this.favorited = true;
					},
					error => {
					})
			} else {
				this.userService.removeFavorite(id)
					.subscribe(
					data => {
						this.favorited = false;
					},
					error => {
					})
			}

		} else {
			this.emitter.emit('register', {});
		}

	}

	public toggleShare = () => {
		this.share = (this.share) ? false : true;
	}
}
