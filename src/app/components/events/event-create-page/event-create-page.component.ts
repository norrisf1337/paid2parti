import { Component, OnInit, Directive, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as moment from 'moment';
//import { FileUploader } from 'ng2-file-upload/ng2-file-upload';

import { UserService } from '../../shared/services/user.service';
import { EventService } from '../../shared/services/events.service';
import { AlertService } from '../../alerts';
import { API } from '../../shared/services/api.service';
import { HeaderService } from '../../shared/services/header.service';
import { GooglePlaceService } from '../../places-two/shared/ng2-google-place.service';
import {MdSelectChange} from '@angular/material';

@Component({
	selector: 'event-create',
	styleUrls: [
		'event-create-page.style.scss'
	],
	providers: [],
	templateUrl: 'event-create-page.template.html'
})
export class EventCreatePage {
	@ViewChild('addInput') addInput;
	@ViewChild('loc') locInput;
	public disableOv = true;
	public ticket: ticket;
	public event: any;
	public edit: boolean = false;
	public createForm: FormGroup;
	public ticketForm: FormGroup;
	public promoterForm: FormGroup;
	public submitted: boolean = false;
	public ticketsSold: boolean = false;
	public chosenOption: string;
	public hasBaseDropZoneOver: boolean = false;
	public hasAnotherDropZoneOver: boolean = false;
	public tickets: Array<ticket> = [];
	public showPromoter: boolean = false;
	public showTicket: boolean = false;
	public framework = 'Angular 2';
	public eventId: string;
	public tmpAddress: any = {
		street: '',
		city: '',
		province: '',
		postal: '',
		country: ''
	};

	public categories = [
		{ 'name': 'Dances & Parties', 'value': 'Dances' },
		{ 'name': 'Sports', 'value': 'Sports' },
		{ 'name': 'Theatre', 'value': 'Theatre' },
		{ 'name': 'Business & Networking', 'value': 'Business' },
		{ 'name': 'Concerts & Music', 'value': 'Concerts' }
	];

	public currencies = [
		{ 'name': 'CAD', 'value': 'CAD' },
		{ 'name': 'USD', 'value': 'USD' }
	];
	public uploadedImage = 'false';

	public organizers = [
		{'_id': undefined, 'email':'Add new promoter'}
	];
		public street_number;
public street;
public city;
public state;
public district;
public country;
public postal_code;
public lat;
public lng;
public adr_address;
public name;
public place_id;
public types;
public url;
public utc_offset;
public vicinity;
public photos;
public airport;
public CountryCodes;

	constructor(public _fb: FormBuilder, private API: API, private userService: UserService, private EventService: EventService, public hs: HeaderService, public alertService: AlertService, public gps: GooglePlaceService, private route: ActivatedRoute, private router: Router) {
		this.hs.resetHeader();
		this.hs.setHeader('single', 'Create Event', '');
		this.eventId = this.userService.generateUid();

		if (window.location.pathname.indexOf('edit') > -1) {
			this.edit = true;
			this.hs.setHeader('single', 'Edit Event', '');
		}
		console.log(this);
	}
	ngOnInit() {
		console.log('rt', this);
		this.createForm = this._fb.group({
			owner: new FormControl(this.userService.user.uid, Validators.compose([Validators.required])),
			name: new FormControl("", Validators.compose([Validators.required])),
			description: new FormControl("", Validators.compose([Validators.required])),
			venue: new FormControl("", Validators.compose([Validators.required])),
			address: new FormControl(this.tmpAddress.street, Validators.compose([Validators.required])),
			country: new FormControl(this.tmpAddress.country, Validators.compose([Validators.required])),
			city: new FormControl(this.tmpAddress.city, Validators.compose([Validators.required])),
			postal: new FormControl(this.tmpAddress.postal, Validators.compose([Validators.required])),
			province: new FormControl(this.tmpAddress.province, Validators.compose([Validators.required])),
			lat: new FormControl(""),
			lng: new FormControl(""),
			category: new FormControl("", Validators.compose([Validators.required])),
			organizer: new FormControl("", Validators.compose([Validators.required])),
			startDate: new FormControl("", Validators.compose([Validators.required])),
			endDate: new FormControl("", Validators.compose([Validators.required])),
			startTime: new FormControl("", Validators.compose([Validators.required])),
			endTime: new FormControl("", Validators.compose([Validators.required])),
			tickets: new FormControl(this.tickets, Validators.compose([Validators.required])),
			image: new FormControl(false),
		});

		if (this.edit) {
			let id: string = this.route.snapshot.params['id'];
			this.getPromoters();
			this.EventService.getById(id)
				.subscribe(
				data => {
					let event = data;
					console.log('event', data);
					let tempObj: any = {
						_id: event._id,
						owner: event.owner,
						name: event.basicDetails.name || '',
						description: event.basicDetails.description || '',
						venue: event.basicDetails.venue || '',
						address: event.location.address || '',
						country: event.location.country || '',
						city: event.location.city || '',
						postal: event.location.postal || '',
						province: event.location.province || '',
						lat: event.location.lat || '',
						lng: event.location.lng || '',
						startTime: '',
						endTime: '',
						startDate: '',
						endDate: '',
						tickets: event.tickets || '',
						image: event.basicDetails.image || 'false',
						category: '',
					}

					if (data.basicDetails.startDate) {
						tempObj.startDate = new Date(data.basicDetails.startDate);
					}
					if (data.basicDetails.endDate) {
						tempObj.endDate = new Date(data.basicDetails.endDate);
					}
					if (data.basicDetails.startTime) {
						tempObj.startTime = new Date(data.basicDetails.startTime)
					}
					if (data.basicDetails.endTime) {
						tempObj.endTime = new Date(data.basicDetails.endTime)
					}

					if (!_.isUndefined(data.basicDetails.category) && data.basicDetails.category !== '') {
						tempObj.category = _.find(this.categories, function(o) {
							console.log(o);
							return o.value === data.basicDetails.category;
						});
					}

					if (!_.isUndefined(data.organizer) && data.organizer !== '') {
						tempObj.organizer = _.find(this.organizers, function(o) {
							console.log(o);
							return o._id === data.organizer[0];
						});
					}


					console.log('to', tempObj);
					this.eventId = event._id;
					this.tickets = event.tickets;
					this.createForm.patchValue(tempObj);
					console.log(this.createForm);
					this.uploadedImage = event.basicDetails.image || 'false';
				},
				error => {
				});
		}

		this.ticketForm = this._fb.group({
			event_Id: new FormControl(this.eventId, Validators.compose([Validators.required])),
			cost: new FormControl("", Validators.compose([Validators.required])),
			description: new FormControl("", Validators.compose([Validators.required])),
			qty: new FormControl("", Validators.compose([Validators.required])),
			currency: new FormControl("", Validators.compose([Validators.required])),
			category: new FormControl("Paid", Validators.compose([Validators.required])),
		});

		this.promoterForm = this._fb.group({
			_id: new FormControl(this.userService.generateUid()),
			owner: new FormControl(this.userService.user.uid, Validators.compose([Validators.required])),
			first: new FormControl("", Validators.compose([Validators.required])),
			last: new FormControl("", Validators.compose([Validators.required])),
			email: new FormControl("", Validators.compose([Validators.required])),
			description: new FormControl("", Validators.compose([])),
			phone: new FormControl("", Validators.compose([])),
			organization: new FormControl("", Validators.compose([])),
			affiliation: new FormControl("", Validators.compose([])),
			socialFb: new FormControl("", Validators.compose([])),
			socialTw: new FormControl("", Validators.compose([])),
			socialLi: new FormControl("", Validators.compose([])),
			socialG: new FormControl("", Validators.compose([])),
			socialIns: new FormControl("", Validators.compose([])),

		});



	}

	public addTicket = () => {
		console.log(this.ticketForm.valid);
		if (this.ticketForm.valid) {
			this.ticketForm.value._id = this.userService.generateUid();
			this.ticketForm.value.currency = this.ticketForm.value.currency.value;
			this.ticketForm.value.available = this.ticketForm.value.qty
			this.ticketForm.value.sold = 0;
			this.tickets.push(this.ticketForm.value);
			this.createForm.patchValue({ tickets: this.tickets });
			this.toggleTicket();
		}
	}

	public addPromoter = () => {
		console.log(this.promoterForm.valid, this.promoterForm);
		if (this.promoterForm.valid) {
			this.promoterForm.value.name = this.promoterForm.value.first + ' ' + this.promoterForm.value.last
			this.organizers.push(this.promoterForm.value);

			console.log(this.promoterForm.value);
			this.userService.createPromoter(this.promoterForm.value)
				.subscribe(
				data => {
					if (data.success) {
						this.alertService.success('Promoter added successfully.');
						this.togglePromoter();
					}
				},
				error => {
					this.alertService.error(error);
				});
		}
	}

	public publish = (type) => {
		let event = {
			_id: this.eventId,
			owner: this.userService.user.uid,
			status: type,
			basicDetails: {
				name: this.createForm.value.name,
				description: this.createForm.value.description,
				venue: this.createForm.value.venue,
				image: this.createForm.value.image,
				thumbnail: '',
				category: this.createForm.value.category.value,
				startDate: this.createForm.value.startDate,
				endDate: this.createForm.value.endDate,
				startTime: this.createForm.value.startTime,
				endTime: this.createForm.value.endTime
			},
			location: {
				address: this.createForm.value.address,
				city: this.createForm.value.city,
				province: this.createForm.value.province,
				postal: this.createForm.value.postal,
				latitude: this.createForm.value.lat,
				longitude: this.createForm.value.lng,
				country: this.createForm.value.country,
			},
			organizer: this.createForm.value.organizer || undefined,
			tickets: this.createForm.value.tickets,
			ticketsSold: this.ticketsSold,
			earnings: 0,
			attendees: []

		}
		if (this.edit) {
			this.EventService.create(event, true)
				.subscribe(
				data => {
					if (data.success) {
						this.alertService.success('Event has been created');
					}
				},
				error => {
					this.alertService.error(error);
				});
		} else {
			this.EventService.create(event, false)
				.subscribe(
				data => {
					if (data.success) {
						this.alertService.success('Event has been created');
					}
				},
				error => {
					this.alertService.error(error);
				});
		}

	}

	public togglePromoter = () => {
		this.showTicket = false;
		this.showPromoter = (this.showPromoter) ? false : true;
	}

	public toggleTicket = () => {
		this.showPromoter = false;
		this.showTicket = (this.showTicket) ? false : true;
	}

	public removeImage = () => {
		this.uploadedImage = 'false';
	}

	public dateChange = (e) => {
		console.log(e);
	}

	uploadFile: any = false;
	options: Object = {
		url: 'http://localhost:8080/upload',
		customHeaders: { 'contentType': 'multipart/form-data' },
		allowedExtensions: ['image/png', 'image/jpg']
	};
	sizeLimit = 2000000;

	geoOptions = {

	}

	public getAddress = (e) => {
		let address = (!_.isNull(this.gps.sublocality_level_2(e.address_components))) ? this.gps.sublocality_level_2(e.address_components).long_name + ' ' + this.gps.street_number(e.address_components) + ' ' + this.gps.street(e.address_components) : this.gps.street_number(e.address_components) + ' ' + this.gps.street(e.address_components);
		let vicinity = this.gps.city(e.address_components);
		let country = this.gps.country(e.address_components);
		let province = this.gps.state(e.address_components);
		let postal = this.gps.postal_code(e.address_components);
		this.createForm.patchValue({
			address: address,
			city: vicinity,
			province: province,
			country: country,
			postal: postal,
			lat: e.geometry.location.lat(),
			lng: e.geometry.location.lng()
		});
		this.addInput.focus();

	}

	public getPromoters = () => {
		this.userService.getPromoters()
			.subscribe(
			data => {
				if (data.success) {
					_.forEach(data.promoter, (o, i) => {
						this.organizers.push(o);
					});
				}
			},
			error => {
				this.alertService.error(error);
			});
	}

	public promoterChange = (event) => {
		console.log(event)
		if (event.value === undefined) {
			this.togglePromoter();
		} else {
			this.showPromoter = false;
		}
	}

	handleUpload(data): void {
		console.log(data)
		if (data) {
			console.log('d2', data)
			this.uploadedImage = data.originalName;
			this.createForm.patchValue({
				image: this.uploadedImage
			});

		}
	}

	fileOverBase(e: any): void {
		this.hasBaseDropZoneOver = e;
	}

	beforeUpload(uploadingFile): void {
		if (uploadingFile.size > this.sizeLimit) {
			uploadingFile.setAbort();
			alert('File is too large');
		}
	}
}
