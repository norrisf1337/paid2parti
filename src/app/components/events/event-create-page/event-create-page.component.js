import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
//import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { UserService } from '../../shared/services/user.service';
import { EventService } from '../../shared/services/events.service';
import { AlertService } from '../../alerts';
import { API } from '../../shared/services/api.service';
import { HeaderService } from '../../shared/services/header.service';
import { GooglePlaceService } from '../../places-two/shared/ng2-google-place.service';
var EventCreatePage = (function () {
    function EventCreatePage(_fb, API, userService, EventService, hs, alertService, gps, route, router) {
        var _this = this;
        this._fb = _fb;
        this.API = API;
        this.userService = userService;
        this.EventService = EventService;
        this.hs = hs;
        this.alertService = alertService;
        this.gps = gps;
        this.route = route;
        this.router = router;
        this.disableOv = true;
        this.edit = false;
        this.submitted = false;
        this.ticketsSold = false;
        this.hasBaseDropZoneOver = false;
        this.hasAnotherDropZoneOver = false;
        this.tickets = [];
        this.showPromoter = false;
        this.showTicket = false;
        this.framework = 'Angular 2';
        this.tmpAddress = {
            street: '',
            city: '',
            province: '',
            postal: '',
            country: ''
        };
        this.categories = [
            { 'name': 'Dances & Parties', 'value': 'Dances' },
            { 'name': 'Sports', 'value': 'Sports' },
            { 'name': 'Theatre', 'value': 'Theatre' },
            { 'name': 'Business & Networking', 'value': 'Business' },
            { 'name': 'Concerts & Music', 'value': 'Concerts' }
        ];
        this.currencies = [
            { 'name': 'CAD', 'value': 'CAD' },
            { 'name': 'USD', 'value': 'USD' }
        ];
        this.uploadedImage = 'false';
        this.organizers = [
            { '_id': undefined, 'email': 'Add new promoter' }
        ];
        this.addTicket = function () {
            console.log(_this.ticketForm.valid);
            if (_this.ticketForm.valid) {
                _this.ticketForm.value._id = _this.userService.generateUid();
                _this.ticketForm.value.currency = _this.ticketForm.value.currency.value;
                _this.ticketForm.value.available = _this.ticketForm.value.qty;
                _this.ticketForm.value.sold = 0;
                _this.tickets.push(_this.ticketForm.value);
                _this.createForm.patchValue({ tickets: _this.tickets });
                _this.toggleTicket();
            }
        };
        this.addPromoter = function () {
            console.log(_this.promoterForm.valid, _this.promoterForm);
            if (_this.promoterForm.valid) {
                _this.promoterForm.value.name = _this.promoterForm.value.first + ' ' + _this.promoterForm.value.last;
                _this.organizers.push(_this.promoterForm.value);
                console.log(_this.promoterForm.value);
                _this.userService.createPromoter(_this.promoterForm.value)
                    .subscribe(function (data) {
                    if (data.success) {
                        _this.alertService.success('Promoter added successfully.');
                        _this.togglePromoter();
                    }
                }, function (error) {
                    _this.alertService.error(error);
                });
            }
        };
        this.publish = function (type) {
            var event = {
                _id: _this.eventId,
                owner: _this.userService.user.uid,
                status: type,
                basicDetails: {
                    name: _this.createForm.value.name,
                    description: _this.createForm.value.description,
                    venue: _this.createForm.value.venue,
                    image: _this.createForm.value.image,
                    thumbnail: '',
                    category: _this.createForm.value.category.value,
                    startDate: _this.createForm.value.startDate,
                    endDate: _this.createForm.value.endDate,
                    startTime: _this.createForm.value.startTime,
                    endTime: _this.createForm.value.endTime
                },
                location: {
                    address: _this.createForm.value.address,
                    city: _this.createForm.value.city,
                    province: _this.createForm.value.province,
                    postal: _this.createForm.value.postal,
                    latitude: _this.createForm.value.lat,
                    longitude: _this.createForm.value.lng,
                    country: _this.createForm.value.country,
                },
                organizer: _this.createForm.value.organizer || undefined,
                tickets: _this.createForm.value.tickets,
                ticketsSold: _this.ticketsSold,
                earnings: 0,
                attendees: []
            };
            if (_this.edit) {
                _this.EventService.create(event, true)
                    .subscribe(function (data) {
                    if (data.success) {
                        _this.alertService.success('Event has been created');
                    }
                }, function (error) {
                    _this.alertService.error(error);
                });
            }
            else {
                _this.EventService.create(event, false)
                    .subscribe(function (data) {
                    if (data.success) {
                        _this.alertService.success('Event has been created');
                    }
                }, function (error) {
                    _this.alertService.error(error);
                });
            }
        };
        this.togglePromoter = function () {
            _this.showTicket = false;
            _this.showPromoter = (_this.showPromoter) ? false : true;
        };
        this.toggleTicket = function () {
            _this.showPromoter = false;
            _this.showTicket = (_this.showTicket) ? false : true;
        };
        this.removeImage = function () {
            _this.uploadedImage = 'false';
        };
        this.dateChange = function (e) {
            console.log(e);
        };
        this.uploadFile = false;
        this.options = {
            url: 'http://localhost:8080/upload',
            customHeaders: { 'contentType': 'multipart/form-data' },
            allowedExtensions: ['image/png', 'image/jpg']
        };
        this.sizeLimit = 2000000;
        this.geoOptions = {};
        this.getAddress = function (e) {
            var address = (!_.isNull(_this.gps.sublocality_level_2(e.address_components))) ? _this.gps.sublocality_level_2(e.address_components).long_name + ' ' + _this.gps.street_number(e.address_components) + ' ' + _this.gps.street(e.address_components) : _this.gps.street_number(e.address_components) + ' ' + _this.gps.street(e.address_components);
            var vicinity = _this.gps.city(e.address_components);
            var country = _this.gps.country(e.address_components);
            var province = _this.gps.state(e.address_components);
            var postal = _this.gps.postal_code(e.address_components);
            _this.createForm.patchValue({
                address: address,
                city: vicinity,
                province: province,
                country: country,
                postal: postal,
                lat: e.geometry.location.lat(),
                lng: e.geometry.location.lng()
            });
            _this.addInput.focus();
        };
        this.getPromoters = function () {
            _this.userService.getPromoters()
                .subscribe(function (data) {
                if (data.success) {
                    _.forEach(data.promoter, function (o, i) {
                        _this.organizers.push(o);
                    });
                }
            }, function (error) {
                _this.alertService.error(error);
            });
        };
        this.promoterChange = function (event) {
            console.log(event);
            if (event.value === undefined) {
                _this.togglePromoter();
            }
            else {
                _this.showPromoter = false;
            }
        };
        this.hs.resetHeader();
        this.hs.setHeader('single', 'Create Event', '');
        this.eventId = this.userService.generateUid();
        if (window.location.pathname.indexOf('edit') > -1) {
            this.edit = true;
            this.hs.setHeader('single', 'Edit Event', '');
        }
        console.log(this);
    }
    EventCreatePage.prototype.ngOnInit = function () {
        var _this = this;
        console.log('rt', this);
        this.createForm = this._fb.group({
            owner: new FormControl(this.userService.user.uid, Validators.compose([Validators.required])),
            name: new FormControl("", Validators.compose([Validators.required])),
            description: new FormControl("", Validators.compose([Validators.required])),
            venue: new FormControl("", Validators.compose([Validators.required])),
            address: new FormControl(this.tmpAddress.street, Validators.compose([Validators.required])),
            country: new FormControl(this.tmpAddress.country, Validators.compose([Validators.required])),
            city: new FormControl(this.tmpAddress.city, Validators.compose([Validators.required])),
            postal: new FormControl(this.tmpAddress.postal, Validators.compose([Validators.required])),
            province: new FormControl(this.tmpAddress.province, Validators.compose([Validators.required])),
            lat: new FormControl(""),
            lng: new FormControl(""),
            category: new FormControl("", Validators.compose([Validators.required])),
            organizer: new FormControl("", Validators.compose([Validators.required])),
            startDate: new FormControl("", Validators.compose([Validators.required])),
            endDate: new FormControl("", Validators.compose([Validators.required])),
            startTime: new FormControl("", Validators.compose([Validators.required])),
            endTime: new FormControl("", Validators.compose([Validators.required])),
            tickets: new FormControl(this.tickets, Validators.compose([Validators.required])),
            image: new FormControl(false),
        });
        if (this.edit) {
            var id = this.route.snapshot.params['id'];
            this.getPromoters();
            this.EventService.getById(id)
                .subscribe(function (data) {
                var event = data;
                console.log('event', data);
                var tempObj = {
                    _id: event._id,
                    owner: event.owner,
                    name: event.basicDetails.name || '',
                    description: event.basicDetails.description || '',
                    venue: event.basicDetails.venue || '',
                    address: event.location.address || '',
                    country: event.location.country || '',
                    city: event.location.city || '',
                    postal: event.location.postal || '',
                    province: event.location.province || '',
                    lat: event.location.lat || '',
                    lng: event.location.lng || '',
                    startTime: '',
                    endTime: '',
                    startDate: '',
                    endDate: '',
                    tickets: event.tickets || '',
                    image: event.basicDetails.image || 'false',
                    category: '',
                };
                if (data.basicDetails.startDate) {
                    tempObj.startDate = new Date(data.basicDetails.startDate);
                }
                if (data.basicDetails.endDate) {
                    tempObj.endDate = new Date(data.basicDetails.endDate);
                }
                if (data.basicDetails.startTime) {
                    tempObj.startTime = new Date(data.basicDetails.startTime);
                }
                if (data.basicDetails.endTime) {
                    tempObj.endTime = new Date(data.basicDetails.endTime);
                }
                if (!_.isUndefined(data.basicDetails.category) && data.basicDetails.category !== '') {
                    tempObj.category = _.find(_this.categories, function (o) {
                        console.log(o);
                        return o.value === data.basicDetails.category;
                    });
                }
                if (!_.isUndefined(data.organizer) && data.organizer !== '') {
                    tempObj.organizer = _.find(_this.organizers, function (o) {
                        console.log(o);
                        return o._id === data.organizer[0];
                    });
                }
                console.log('to', tempObj);
                _this.eventId = event._id;
                _this.tickets = event.tickets;
                _this.createForm.patchValue(tempObj);
                console.log(_this.createForm);
                _this.uploadedImage = event.basicDetails.image || 'false';
            }, function (error) {
            });
        }
        this.ticketForm = this._fb.group({
            event_Id: new FormControl(this.eventId, Validators.compose([Validators.required])),
            cost: new FormControl("", Validators.compose([Validators.required])),
            description: new FormControl("", Validators.compose([Validators.required])),
            qty: new FormControl("", Validators.compose([Validators.required])),
            currency: new FormControl("", Validators.compose([Validators.required])),
            category: new FormControl("Paid", Validators.compose([Validators.required])),
        });
        this.promoterForm = this._fb.group({
            _id: new FormControl(this.userService.generateUid()),
            owner: new FormControl(this.userService.user.uid, Validators.compose([Validators.required])),
            first: new FormControl("", Validators.compose([Validators.required])),
            last: new FormControl("", Validators.compose([Validators.required])),
            email: new FormControl("", Validators.compose([Validators.required])),
            description: new FormControl("", Validators.compose([])),
            phone: new FormControl("", Validators.compose([])),
            organization: new FormControl("", Validators.compose([])),
            affiliation: new FormControl("", Validators.compose([])),
            socialFb: new FormControl("", Validators.compose([])),
            socialTw: new FormControl("", Validators.compose([])),
            socialLi: new FormControl("", Validators.compose([])),
            socialG: new FormControl("", Validators.compose([])),
            socialIns: new FormControl("", Validators.compose([])),
        });
    };
    EventCreatePage.prototype.handleUpload = function (data) {
        console.log(data);
        if (data) {
            console.log('d2', data);
            this.uploadedImage = data.originalName;
            this.createForm.patchValue({
                image: this.uploadedImage
            });
        }
    };
    EventCreatePage.prototype.fileOverBase = function (e) {
        this.hasBaseDropZoneOver = e;
    };
    EventCreatePage.prototype.beforeUpload = function (uploadingFile) {
        if (uploadingFile.size > this.sizeLimit) {
            uploadingFile.setAbort();
            alert('File is too large');
        }
    };
    return EventCreatePage;
}());
tslib_1.__decorate([
    ViewChild('addInput'),
    tslib_1.__metadata("design:type", Object)
], EventCreatePage.prototype, "addInput", void 0);
tslib_1.__decorate([
    ViewChild('loc'),
    tslib_1.__metadata("design:type", Object)
], EventCreatePage.prototype, "locInput", void 0);
EventCreatePage = tslib_1.__decorate([
    Component({
        selector: 'event-create',
        styleUrls: [
            'event-create-page.style.scss'
        ],
        providers: [],
        templateUrl: 'event-create-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [FormBuilder, API, UserService, EventService, HeaderService, AlertService, GooglePlaceService, ActivatedRoute, Router])
], EventCreatePage);
export { EventCreatePage };
//# sourceMappingURL=event-create-page.component.js.map