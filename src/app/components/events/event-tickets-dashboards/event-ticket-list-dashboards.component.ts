import { Component } from '@angular/core';

@Component({
  selector: 'ticket-list-dashboards',
  styleUrls: [
    'event-ticket-list-dashboards.style.scss'
  ],
  templateUrl: 'event-ticket-list-dashboards.template.html'
})
export class EventTicketListDashboard {
}
