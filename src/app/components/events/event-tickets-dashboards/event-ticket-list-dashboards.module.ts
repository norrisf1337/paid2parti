import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventTicketListDashboard } from './event-ticket-list-dashboards.component';


@NgModule({
  imports: [CommonModule],
  declarations: [
    EventTicketListDashboard
  ],
  exports: [EventTicketListDashboard]
})
export class EventTicketListDashboardsModule {}
