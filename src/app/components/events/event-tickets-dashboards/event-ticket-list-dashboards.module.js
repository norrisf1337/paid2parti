import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventTicketListDashboard } from './event-ticket-list-dashboards.component';
var EventTicketListDashboardsModule = (function () {
    function EventTicketListDashboardsModule() {
    }
    return EventTicketListDashboardsModule;
}());
EventTicketListDashboardsModule = tslib_1.__decorate([
    NgModule({
        imports: [CommonModule],
        declarations: [
            EventTicketListDashboard
        ],
        exports: [EventTicketListDashboard]
    })
], EventTicketListDashboardsModule);
export { EventTicketListDashboardsModule };
//# sourceMappingURL=event-ticket-list-dashboards.module.js.map