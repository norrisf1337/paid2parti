import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var EventTicketListDashboard = (function () {
    function EventTicketListDashboard() {
    }
    return EventTicketListDashboard;
}());
EventTicketListDashboard = tslib_1.__decorate([
    Component({
        selector: 'ticket-list-dashboards',
        styleUrls: [
            'event-ticket-list-dashboards.style.scss'
        ],
        templateUrl: 'event-ticket-list-dashboards.template.html'
    })
], EventTicketListDashboard);
export { EventTicketListDashboard };
//# sourceMappingURL=event-ticket-list-dashboards.component.js.map