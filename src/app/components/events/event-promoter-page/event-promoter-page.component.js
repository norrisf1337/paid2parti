import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
var EventPromoterPage = (function () {
    function EventPromoterPage(_fb) {
        var _this = this;
        this._fb = _fb;
        this.promoters = [];
        this.addPromoter = function () {
            _this.promoterForm.value.name = _this.promoterForm.value.first + ' ' + _this.promoterForm.value.last;
            console.log(_this.promoterForm.value);
            _this.promoters.push(_this.promoterForm.value);
        };
    }
    EventPromoterPage.prototype.ngOnInit = function () {
        this.promoterForm = this._fb.group({
            first: new FormControl("", Validators.compose([Validators.required])),
            last: new FormControl("", Validators.compose([Validators.required])),
            email: new FormControl("", Validators.compose([Validators.required])),
            description: new FormControl("", Validators.compose([Validators.required])),
            phone: new FormControl("", Validators.compose([Validators.required])),
            organization: new FormControl("", Validators.compose([Validators.required])),
            affiliation: new FormControl("", Validators.compose([Validators.required])),
            socialFb: new FormControl("", Validators.compose([Validators.required])),
            socialTw: new FormControl("", Validators.compose([Validators.required])),
            socialLi: new FormControl("", Validators.compose([Validators.required])),
            socialG: new FormControl("", Validators.compose([Validators.required])),
            socialIns: new FormControl("", Validators.compose([Validators.required])),
        });
    };
    return EventPromoterPage;
}());
EventPromoterPage = tslib_1.__decorate([
    Component({
        selector: 'event-promoter',
        entryComponents: [],
        styleUrls: [
            'event-promoter-page.style.scss'
        ],
        providers: [],
        templateUrl: 'event-promoter-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [FormBuilder])
], EventPromoterPage);
export { EventPromoterPage };
//# sourceMappingURL=event-promoter-page.component.js.map