import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Component({
	selector: 'event-promoter',
	entryComponents: [],
	styleUrls: [
		'event-promoter-page.style.scss'
	],
	providers: [],
	templateUrl: 'event-promoter-page.template.html'
})
export class EventPromoterPage {
	public promoterForm: FormGroup;
	public promoters: any = [];
	constructor(public _fb: FormBuilder) {
	}

	ngOnInit() {

		this.promoterForm = this._fb.group({
			first: new FormControl("", Validators.compose([Validators.required])),
			last: new FormControl("", Validators.compose([Validators.required])),
			email: new FormControl("", Validators.compose([Validators.required])),
			description: new FormControl("", Validators.compose([Validators.required])),
			phone: new FormControl("", Validators.compose([Validators.required])),
			organization: new FormControl("", Validators.compose([Validators.required])),
			affiliation: new FormControl("", Validators.compose([Validators.required])),
			socialFb: new FormControl("", Validators.compose([Validators.required])),
			socialTw: new FormControl("", Validators.compose([Validators.required])),
			socialLi: new FormControl("", Validators.compose([Validators.required])),
			socialG: new FormControl("", Validators.compose([Validators.required])),
			socialIns: new FormControl("", Validators.compose([Validators.required])),

		});
	}

	public addPromoter = () => {
		this.promoterForm.value.name = this.promoterForm.value.first + ' ' + this.promoterForm.value.last
		console.log(this.promoterForm.value);
		this.promoters.push(this.promoterForm.value);
	}
}
