import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EventService } from '../../shared/services/events.service';
import { HeaderService } from '../../shared/services/header.service';
import { API } from '../../shared/services/api.service';
import * as _ from 'lodash';
import { GooglePlaceService } from '../../places-two/shared/ng2-google-place.service';
var EventList = (function () {
    function EventList(_fb, eventService, api, hs, gps, route, router) {
        var _this = this;
        this._fb = _fb;
        this.eventService = eventService;
        this.api = api;
        this.hs = hs;
        this.gps = gps;
        this.route = route;
        this.router = router;
        this.showFilter = false;
        this.categories = [
            { 'name': 'Dances & Parties', 'value': 'Dances' },
            { 'name': 'Sports', 'value': 'Sports' },
            { 'name': 'Theatre', 'value': 'Theatre' },
            { 'name': 'Business & Networking', 'value': 'Business' },
            { 'name': 'Concerts & Music', 'value': 'Concerts' }
        ];
        this.price = [
            { 'name': 'Under $50', 'value': '50' },
            { 'name': 'Under $100', 'value': '100' },
            { 'name': 'Under $150', 'value': '150' },
            { 'name': 'Under $200', 'value': '200' }
        ];
        this.events = undefined;
        this.loaded = false;
        this.pagOptions = {};
        this.filterOptions = {};
        this.showMore = false;
        this.geoOptions = {};
        this.geo = undefined;
        this.getAddress = function (e) {
            if (_.isUndefined(_this.geo)) {
                _this.geo = {};
            }
            _this.geo.city = _this.gps.city(e.address_components);
            _this.geo.country = _this.gps.country(e.address_components);
            _this.geo.lat = e.geometry.location.lat();
            _this.geo.lng = e.geometry.location.lng();
        };
        this.callEndpoint = function (page) {
            var offset = (page === 0 || page === 1) ? 0 : (page - 1) * _this.pagOptions.maxPerPage;
            _this.api.get(_this.pagOptions.endpoint, { paged: true, offset: offset, limit: _this.pagOptions.maxPerPage, filters: _this.pagOptions.filters })
                .subscribe(function (data) {
                if (!_.isUndefined(_this.pagOptions.callback)) {
                    _this.pagOptions.callback(data.events);
                    _this.pagOptions.total = data.count;
                    _this.pagOptions.currentPage = (page === 0) ? 1 : page;
                    _this.loaded = true;
                }
            }, function (error) {
            });
        };
        this.pageCallback = function (data) {
            _this.events = data;
            //document.getElementById("eco").scrollIntoView();
            document.body.scrollTop = 0;
        };
        this.toggleFilter = function () {
            _this.showFilter = (_this.showFilter) ? false : true;
        };
        this.filterEvents = function () {
            if (!_.isUndefined(_this.pagOptions.filters)) {
                _this.pagOptions.filters = {};
            }
            if (_this.FilterForm.value.location === '' && !_.isUndefined(_this.geo)) {
                _this.geo = undefined;
            }
            _this.pagOptions.filters = {
                geo: _this.geo || _this.FilterForm.value.location,
                keywords: _this.FilterForm.value.keywords,
                price: _this.FilterForm.value.price,
                category: _this.FilterForm.value.category
            };
            _this.loaded = false;
            _this.callEndpoint(0);
        };
    }
    EventList.prototype.ngOnInit = function () {
        this.filterOptions.limit = 12;
        var rname = this.route;
        if (rname.component.name === 'Home') {
            this.filterOptions.limit = 6;
            this.showMore = true;
        }
        this.pagOptions = {
            currentPage: 1,
            maxPerPage: this.filterOptions.limit,
            endpoint: '/api/events',
            total: undefined,
            callEndpoint: this.callEndpoint,
            callback: this.pageCallback,
            filters: {}
        };
        if (rname.snapshot.params.id) {
            this.pagOptions.filters.category = rname.snapshot.params.id;
        }
        this.callEndpoint(0);
        this.FilterForm = this._fb.group({
            keywords: new FormControl("", Validators.compose([])),
            location: new FormControl("", Validators.compose([])),
            price: new FormControl("", Validators.compose([])),
            category: new FormControl("", Validators.compose([])),
        });
    };
    return EventList;
}());
EventList = tslib_1.__decorate([
    Component({
        selector: 'events',
        styleUrls: [
            'event.listing.style.scss'
        ],
        templateUrl: 'event.listing.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [FormBuilder, EventService, API, HeaderService, GooglePlaceService, ActivatedRoute, Router])
], EventList);
export { EventList };
//# sourceMappingURL=event.listing.component.js.map