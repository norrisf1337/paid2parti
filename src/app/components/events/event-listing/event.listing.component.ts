import { Component, OnInit, Directive } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { EventService } from '../../shared/services/events.service';
import { HeaderService } from '../../shared/services/header.service';
import { API } from '../../shared/services/api.service';
import * as _ from 'lodash';
import { GooglePlaceService } from '../../places-two/shared/ng2-google-place.service';

@Component({
	selector: 'events',
	styleUrls: [
		'event.listing.style.scss'
	],
	templateUrl: 'event.listing.template.html'
})
export class EventList {
	public showFilter = false;
	public categories = [
		{ 'name': 'Dances & Parties', 'value': 'Dances' },
		{ 'name': 'Sports', 'value': 'Sports' },
		{ 'name': 'Theatre', 'value': 'Theatre' },
		{ 'name': 'Business & Networking', 'value': 'Business' },
		{ 'name': 'Concerts & Music', 'value': 'Concerts' }
	];
	public price = [
		{ 'name': 'Under $50', 'value': '50' },
		{ 'name': 'Under $100', 'value': '100' },
		{ 'name': 'Under $150', 'value': '150' },
		{ 'name': 'Under $200', 'value': '200' }

	];
	public FilterForm: FormGroup;
	public events: any = undefined;
	public loaded: boolean = false;
	public pagOptions: any = {};
	public filterOptions: any = {};
	public showMore: boolean = false;
	public geoOptions = {

	}
	public street_number;
public street;
public city;
public state;
public district;
public country;
public postal_code;
public lat;
public lng;
public adr_address;
public name;
public place_id;
public types;
public url;
public utc_offset;
public vicinity;
public photos;
public airport;
public CountryCodes;
	public geo: any = undefined;
	constructor(public _fb: FormBuilder, public eventService: EventService, public api: API, public hs: HeaderService, public gps: GooglePlaceService, public route: ActivatedRoute, private router: Router) {
	}

	ngOnInit() {
		this.filterOptions.limit = 12;
		let rname: any = this.route

		if (rname.component.name === 'Home') {
			this.filterOptions.limit = 6;
			this.showMore = true;
		}

		this.pagOptions = {
			currentPage: 1,
			maxPerPage: this.filterOptions.limit,
			endpoint: '/api/events',
			total: undefined,
			callEndpoint: this.callEndpoint,
			callback: this.pageCallback,
			filters: {}
		};

		if (rname.snapshot.params.id) {
			this.pagOptions.filters.category = rname.snapshot.params.id;
		}

		this.callEndpoint(0);

		this.FilterForm = this._fb.group({
			keywords: new FormControl("", Validators.compose([])),
			location: new FormControl("", Validators.compose([])),
			price: new FormControl("", Validators.compose([])),
			category: new FormControl("", Validators.compose([])),
		});
	}

	public getAddress = (e) => {
		if (_.isUndefined(this.geo)) {
			this.geo = {};
		}
		this.geo.city = this.gps.city(e.address_components);
		this.geo.country = this.gps.country(e.address_components);
		this.geo.lat = e.geometry.location.lat()
		this.geo.lng = e.geometry.location.lng()

	}

	public callEndpoint = (page) => {
		let offset = (page === 0 || page === 1) ? 0 : (page - 1) * this.pagOptions.maxPerPage;
		this.api.get(this.pagOptions.endpoint, { paged: true, offset: offset, limit: this.pagOptions.maxPerPage, filters:this.pagOptions.filters })
			.subscribe(
			data => {
				if (!_.isUndefined(this.pagOptions.callback)) {
					this.pagOptions.callback(data.events)
					this.pagOptions.total = data.count;
					this.pagOptions.currentPage = (page === 0) ? 1 : page;
					this.loaded = true;
				}
			},
			error => {
			})
	}

	public pageCallback = (data) => {
		this.events = data;
		//document.getElementById("eco").scrollIntoView();
		document.body.scrollTop = 0;
	}

	public toggleFilter = () => {
		this.showFilter = (this.showFilter) ? false : true;
	}

	public filterEvents = (): void => {
		if(!_.isUndefined(this.pagOptions.filters)) {
			this.pagOptions.filters = {};
		}

		if(this.FilterForm.value.location === '' && !_.isUndefined(this.geo)) {
			this.geo = undefined;
		}

		this.pagOptions.filters = {
			geo: this.geo || this.FilterForm.value.location,
			keywords: this.FilterForm.value.keywords,
			price: this.FilterForm.value.price,
			category: this.FilterForm.value.category
		};
		this.loaded = false;
		this.callEndpoint(0)
	}
}
