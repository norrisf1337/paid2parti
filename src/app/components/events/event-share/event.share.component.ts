import { Component, Input } from '@angular/core';
import { ShareButton, ShareProvider } from "ng2-sharebuttons";

@Component({
  selector: 'share',
  styleUrls: [
    'event.share.style.scss'
  ],
  templateUrl: 'event.share.template.html'
})
export class EventShare {
	@Input('size') size: string = 'l';
  public description: string = 'this and that';
  public shareTitle: string = "Sharing is caring";
  public fbInner: string = '<span class="socicon socicon-facebook ' + this.size +'"></span>';
  public twitterInner: string = '<span class="socicon socicon-twitter ' + this.size +'"></span>';
  public inInner: string = '<span class="socicon socicon-linkedin ' + this.size +'"></span>';
  public googleInner: string = '<span class="socicon socicon-googleplus ' + this.size +'"></span>';
  public twButton = new ShareButton(
    ShareProvider.TWITTER,
    '<span class="socicon socicon-twitter"></span>',
    'socIcon'
  );
  public fbButton = new ShareButton(
    ShareProvider.FACEBOOK,
    '<span class="socicon socicon-facebook"></span>',
    'socIcon'
  );
  public gButton = new ShareButton(
    ShareProvider.GOOGLEPLUS,
    '<span class="socicon socicon-googleplus"></span>',
    'socIcon'

  );
  public liButton = new ShareButton(
    ShareProvider.LINKEDIN,
    '<span class="socicon socicon-linkedin"></span>',
    'socIcon'
  );

  ngOnInit() {

  }


}
