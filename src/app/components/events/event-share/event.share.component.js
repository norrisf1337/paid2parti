import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { ShareButton, ShareProvider } from "ng2-sharebuttons";
var EventShare = (function () {
    function EventShare() {
        this.size = 'l';
        this.description = 'this and that';
        this.shareTitle = "Sharing is caring";
        this.fbInner = '<span class="socicon socicon-facebook ' + this.size + '"></span>';
        this.twitterInner = '<span class="socicon socicon-twitter ' + this.size + '"></span>';
        this.inInner = '<span class="socicon socicon-linkedin ' + this.size + '"></span>';
        this.googleInner = '<span class="socicon socicon-googleplus ' + this.size + '"></span>';
        this.twButton = new ShareButton(ShareProvider.TWITTER, '<span class="socicon socicon-twitter"></span>', 'socIcon');
        this.fbButton = new ShareButton(ShareProvider.FACEBOOK, '<span class="socicon socicon-facebook"></span>', 'socIcon');
        this.gButton = new ShareButton(ShareProvider.GOOGLEPLUS, '<span class="socicon socicon-googleplus"></span>', 'socIcon');
        this.liButton = new ShareButton(ShareProvider.LINKEDIN, '<span class="socicon socicon-linkedin"></span>', 'socIcon');
    }
    EventShare.prototype.ngOnInit = function () {
    };
    return EventShare;
}());
tslib_1.__decorate([
    Input('size'),
    tslib_1.__metadata("design:type", String)
], EventShare.prototype, "size", void 0);
EventShare = tslib_1.__decorate([
    Component({
        selector: 'share',
        styleUrls: [
            'event.share.style.scss'
        ],
        templateUrl: 'event.share.template.html'
    })
], EventShare);
export { EventShare };
//# sourceMappingURL=event.share.component.js.map