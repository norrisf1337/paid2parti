import { Component, Input } from '@angular/core';

@Component({
  selector: 'event-loop-dashboards',
  templateUrl: 'event-listing-grid-dashboards.template.html',
  styleUrls:['event-listing-grid-dashboards.style.scss']
})
export class EventListGridDashboards {
	 @Input('event') event = [];
}
