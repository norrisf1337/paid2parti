import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
var EventListGridDashboards = (function () {
    function EventListGridDashboards() {
        this.event = [];
    }
    return EventListGridDashboards;
}());
tslib_1.__decorate([
    Input('event'),
    tslib_1.__metadata("design:type", Object)
], EventListGridDashboards.prototype, "event", void 0);
EventListGridDashboards = tslib_1.__decorate([
    Component({
        selector: 'event-loop-dashboards',
        templateUrl: 'event-listing-grid-dashboards.template.html',
        styleUrls: ['event-listing-grid-dashboards.style.scss']
    })
], EventListGridDashboards);
export { EventListGridDashboards };
//# sourceMappingURL=event-listing-grid-dashboards.component.js.map