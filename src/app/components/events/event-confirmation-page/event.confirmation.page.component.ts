import { Component } from '@angular/core';

@Component({
  selector: 'event-confirmation-page',
  entryComponents: [],
  styleUrls: [
    'event.confirmation.page.style.scss'
  ],
  providers: [],
  templateUrl: 'event.confirmation.page.template.html'
})
export class EventConfirmationPage {
}
