import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var EventConfirmationPage = (function () {
    function EventConfirmationPage() {
    }
    return EventConfirmationPage;
}());
EventConfirmationPage = tslib_1.__decorate([
    Component({
        selector: 'event-confirmation-page',
        entryComponents: [],
        styleUrls: [
            'event.confirmation.page.style.scss'
        ],
        providers: [],
        templateUrl: 'event.confirmation.page.template.html'
    })
], EventConfirmationPage);
export { EventConfirmationPage };
//# sourceMappingURL=event.confirmation.page.component.js.map