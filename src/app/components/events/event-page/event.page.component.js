import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
var EventPage = (function () {
    function EventPage(hs) {
        this.hs = hs;
        this.hs.resetHeader();
        this.hs.setHeader('single', 'Events', '');
    }
    return EventPage;
}());
EventPage = tslib_1.__decorate([
    Component({
        selector: 'event-page',
        entryComponents: [],
        styleUrls: [
            'event.page.style.scss'
        ],
        providers: [],
        templateUrl: 'event.page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService])
], EventPage);
export { EventPage };
//# sourceMappingURL=event.page.component.js.map