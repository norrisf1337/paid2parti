import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';

@Component({
  selector: 'event-page',
  entryComponents: [],
  styleUrls: [
    'event.page.style.scss'
  ],
  providers: [],
  templateUrl: 'event.page.template.html'
})
export class EventPage {
	constructor(public hs: HeaderService) {
		this.hs.resetHeader();
		this.hs.setHeader('single', 'Events', '');
	}
}
