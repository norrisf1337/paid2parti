import { Component, Input, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { UserService } from '../../shared/services/user.service';
import { Emitter } from '../../shared/services/emitter.service';

@Component({
  selector: 'ticket-list',
  styleUrls: [
    'event.ticket.list.style.scss'
  ],
  templateUrl: 'event.ticket.list.template.html'
})
export class EventTicketList {
	@Input('tickets') tickets;
	@Input('isCreate') isCreate = false;
	public inputQty = 0;

	constructor(public router: Router, public route: ActivatedRoute, public userService: UserService, public emitter: Emitter){

	}

	ngOnInit(){
		console.log('tickets', this.tickets);
	}

	buyTickets = (id: string, qty: number) => {
		console.log(id);
		if (this.userService.isLogged()) {
			if(qty && !isNaN(qty) && qty > 0){;
			this.router.navigate(['register/', id, qty], { relativeTo: this.route });
			}
		} else {
			this.emitter.emit('register',{});
		}

	}

	removeTicket = (ticket) => {
		this.tickets.splice(0,1);
	}
}
