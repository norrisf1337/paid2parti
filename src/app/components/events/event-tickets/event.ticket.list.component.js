import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../shared/services/user.service';
import { Emitter } from '../../shared/services/emitter.service';
var EventTicketList = (function () {
    function EventTicketList(router, route, userService, emitter) {
        var _this = this;
        this.router = router;
        this.route = route;
        this.userService = userService;
        this.emitter = emitter;
        this.isCreate = false;
        this.inputQty = 0;
        this.buyTickets = function (id, qty) {
            console.log(id);
            if (_this.userService.isLogged()) {
                if (qty && !isNaN(qty) && qty > 0) {
                    ;
                    _this.router.navigate(['register/', id, qty], { relativeTo: _this.route });
                }
            }
            else {
                _this.emitter.emit('register', {});
            }
        };
        this.removeTicket = function (ticket) {
            _this.tickets.splice(0, 1);
        };
    }
    EventTicketList.prototype.ngOnInit = function () {
        console.log('tickets', this.tickets);
    };
    return EventTicketList;
}());
tslib_1.__decorate([
    Input('tickets'),
    tslib_1.__metadata("design:type", Object)
], EventTicketList.prototype, "tickets", void 0);
tslib_1.__decorate([
    Input('isCreate'),
    tslib_1.__metadata("design:type", Object)
], EventTicketList.prototype, "isCreate", void 0);
EventTicketList = tslib_1.__decorate([
    Component({
        selector: 'ticket-list',
        styleUrls: [
            'event.ticket.list.style.scss'
        ],
        templateUrl: 'event.ticket.list.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [Router, ActivatedRoute, UserService, Emitter])
], EventTicketList);
export { EventTicketList };
//# sourceMappingURL=event.ticket.list.component.js.map