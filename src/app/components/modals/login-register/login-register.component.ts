import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
// import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
// import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
import {MdDialog, MdDialogRef} from '@angular/material';
import { UserService } from '../../shared/services/user.service';
import { FormService } from '../../shared/services/form.service';
import { AuthenticationService } from '../../authentication/auth.service';
import { AlertService } from '../../alerts/';
import { CookieService } from 'angular2-cookie/core';
import * as _ from 'lodash';

/**
 * A Sample of how simple it is to create a new window, with its own injects.
 */
@Component({
  selector: 'login-register',
  styleUrls: [
    'login-register.style.scss'
  ],
  templateUrl: 'login-register.template.html'
})
export class LoginModal implements OnInit {
  public regForm: FormGroup;
  public loginForm: FormGroup;
  public submitted: boolean = false;
  public events: any[] = [];
  public wrongAnswer: boolean;
  public refId: string = undefined;
  public interests = [
    { 'name': 'Dances & Parties', 'value': 'Dances' },
    { 'name': 'Sports', 'value': 'Sports' },
    { 'name': 'Theatre', 'value': 'Theatre' },
    { 'name': 'Business & Networking', 'value': 'Business' },
    { 'name': 'Concerts & Music', 'value': 'Concerts' }
  ];
  public exists: boolean = false;

  constructor(public dialogRef: MdDialogRef<LoginModal>, public _fb: FormBuilder, public UserService: UserService, public alertService: AlertService, public router: Router, private authenticationService: AuthenticationService, public formService: FormService, public CookieService: CookieService) {
    // this.dialog.context.size = 'lg';
    // this.context = dialog.context;
    this.wrongAnswer = true;
    //let hasRef = this.CookieService.get('ref');
    console.log('ref', this.CookieService.get('ref'));
  }

  ngOnInit() {
    // the short way
    this.regForm = this._fb.group({
      emails: this._fb.group({
        email: new FormControl("", Validators.compose([Validators.required, this.formService.mailFormat])),
        emailConfirm: new FormControl("", Validators.compose([Validators.required, this.formService.mailFormat]))
      }, { validator: this.formService.matchingPasswords('email', 'emailConfirm') }),
      passwords: this._fb.group({
        password: new FormControl("", Validators.compose([Validators.required, this.formService.passFormat])),
        passwordConfirm: new FormControl("", Validators.compose([Validators.required, this.formService.passFormat]))
      }, { validator: this.formService.matchingPasswords('password', 'passwordConfirm') }),
      firstname: new FormControl("", Validators.compose([Validators.required, this.formService.nameFormat])),
      lastname: new FormControl("", Validators.compose([Validators.required, this.formService.nameFormat])),
      city: ['', <any>Validators.required],
      country: ['', <any>Validators.required],
      interests: this.toFormGroup(this.interests)
    });

    this.loginForm = this._fb.group({
      email: ['', [<any>Validators.required, this.formService.mailFormat]],
      password: ['', [<any>Validators.required, <any>Validators.minLength(8)]],
    });

    // subscribe to form changes
    //this.subcribeToFormChanges();
  }

  subcribeToFormChanges() {
    const regFormStatusChanges$ = this.regForm.statusChanges;
    // const regFormValueChanges$ = this.regForm.valueChanges;

    // regFormStatusChanges$.subscribe(x => this.events.push({ event: 'STATUS_CHANGED', object: this.regForm }));
    // regFormValueChanges$.subscribe(x => this.events.push({ event: 'VALUE_CHANGED', object: this.regForm }));
    //regFormStatusChanges$.subscribe(x => console.log(this.regForm));
  }

  public setInterests = (interests: any) => {
    let userInterests = [];
    let defaultInterests = [];
    _.forEach(interests, (o, i) => {
      if (o) {
        userInterests.push(i);
      }
      defaultInterests.push(i);
    });

    return (_.isEmpty(userInterests)) ? defaultInterests : userInterests;
  };

  public register = (model, isValid: boolean) => {

    this.submitted = true;
    if (isValid) {
      this.exists = false;
      let id = this.UserService.generateUid();
      let newUser: User = {
        _id: id,
        credentials: {
          username: model.emails.email,
          password: model.passwords.password,
          email: model.emails.email,
          admin: false,
        },
        personal: {
          firstName: model.firstname,
          lastName: model.lastname,
          city: model.city,
          country: model.country,
          interests: this.setInterests(model.interests),
        },
        account: {
          depositAccount: '',
          refId: this.CookieService.get('ref') || localStorage.getItem('refId'),
          fundsHeld: 0,
          fundsAvailable: 0,
        },
        filterPrefs: {
        }
      };

      this.UserService.create(newUser)
        .subscribe(
        data => {
          if (data.success) {
            this.UserService.user.isLogged = true;
            this.UserService.user.uid = newUser._id;
            localStorage.setItem('currentUser', JSON.stringify({token: data.token, user:{id: newUser._id}}));
            this.dialogRef.close();
          }
          if (!data.success) {
            if (data.error && data.exists) {
              this.exists = true;
            } else if (data.error) {
              this.alertService.error(data.error)
            }
          }
        },
        error => {
          this.alertService.error(error);
        });
    }
  }

login(model, isValid: boolean) {
  this.UserService.login(model.email, model.password)
    .subscribe(
    data => {
      if (data.success) {
        console.log(data);
        localStorage.setItem('currentUser', JSON.stringify({token: data.token, user:{id: data.user.id}}));
        this.UserService.user.uid = data.user.id;
        this.UserService.user.isLogged = true;
        this.UserService.user.refId = data.user.refId;
        this.dialogRef.close();
      } else {

      }
    },
    error => {
      this.alertService.error(error);
    });
}

  public toFormGroup = (interests) => {
  let group: any = {};

  interests.forEach(question => {
    group[question.value] = new FormControl(true);
  });

  return new FormGroup(group);
}

  public closeModal = () => {
  this.dialogRef.close();
}

beforeDismiss(): boolean {
  return true;
}

beforeClose(): boolean {
  return this.wrongAnswer;
}
}
