import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
// import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
// import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { MdDialogRef } from '@angular/material';
import { UserService } from '../../shared/services/user.service';
import { FormService } from '../../shared/services/form.service';
import { AuthenticationService } from '../../authentication/auth.service';
import { AlertService } from '../../alerts/';
import { CookieService } from 'angular2-cookie/core';
import * as _ from 'lodash';
/**
 * A Sample of how simple it is to create a new window, with its own injects.
 */
var LoginModal = (function () {
    function LoginModal(dialogRef, _fb, UserService, alertService, router, authenticationService, formService, CookieService) {
        var _this = this;
        this.dialogRef = dialogRef;
        this._fb = _fb;
        this.UserService = UserService;
        this.alertService = alertService;
        this.router = router;
        this.authenticationService = authenticationService;
        this.formService = formService;
        this.CookieService = CookieService;
        this.submitted = false;
        this.events = [];
        this.refId = undefined;
        this.interests = [
            { 'name': 'Dances & Parties', 'value': 'Dances' },
            { 'name': 'Sports', 'value': 'Sports' },
            { 'name': 'Theatre', 'value': 'Theatre' },
            { 'name': 'Business & Networking', 'value': 'Business' },
            { 'name': 'Concerts & Music', 'value': 'Concerts' }
        ];
        this.exists = false;
        this.setInterests = function (interests) {
            var userInterests = [];
            var defaultInterests = [];
            _.forEach(interests, function (o, i) {
                if (o) {
                    userInterests.push(i);
                }
                defaultInterests.push(i);
            });
            return (_.isEmpty(userInterests)) ? defaultInterests : userInterests;
        };
        this.register = function (model, isValid) {
            _this.submitted = true;
            if (isValid) {
                _this.exists = false;
                var id = _this.UserService.generateUid();
                var newUser_1 = {
                    _id: id,
                    credentials: {
                        username: model.emails.email,
                        password: model.passwords.password,
                        email: model.emails.email,
                        admin: false,
                    },
                    personal: {
                        firstName: model.firstname,
                        lastName: model.lastname,
                        city: model.city,
                        country: model.country,
                        interests: _this.setInterests(model.interests),
                    },
                    account: {
                        depositAccount: '',
                        refId: _this.CookieService.get('ref') || localStorage.getItem('refId'),
                        fundsHeld: 0,
                        fundsAvailable: 0,
                    },
                    filterPrefs: {}
                };
                _this.UserService.create(newUser_1)
                    .subscribe(function (data) {
                    if (data.success) {
                        _this.UserService.user.isLogged = true;
                        _this.UserService.user.uid = newUser_1._id;
                        localStorage.setItem('currentUser', JSON.stringify({ token: data.token, user: { id: newUser_1._id } }));
                        _this.dialogRef.close();
                    }
                    if (!data.success) {
                        if (data.error && data.exists) {
                            _this.exists = true;
                        }
                        else if (data.error) {
                            _this.alertService.error(data.error);
                        }
                    }
                }, function (error) {
                    _this.alertService.error(error);
                });
            }
        };
        this.toFormGroup = function (interests) {
            var group = {};
            interests.forEach(function (question) {
                group[question.value] = new FormControl(true);
            });
            return new FormGroup(group);
        };
        this.closeModal = function () {
            _this.dialogRef.close();
        };
        // this.dialog.context.size = 'lg';
        // this.context = dialog.context;
        this.wrongAnswer = true;
        //let hasRef = this.CookieService.get('ref');
        console.log('ref', this.CookieService.get('ref'));
    }
    LoginModal.prototype.ngOnInit = function () {
        // the short way
        this.regForm = this._fb.group({
            emails: this._fb.group({
                email: new FormControl("", Validators.compose([Validators.required, this.formService.mailFormat])),
                emailConfirm: new FormControl("", Validators.compose([Validators.required, this.formService.mailFormat]))
            }, { validator: this.formService.matchingPasswords('email', 'emailConfirm') }),
            passwords: this._fb.group({
                password: new FormControl("", Validators.compose([Validators.required, this.formService.passFormat])),
                passwordConfirm: new FormControl("", Validators.compose([Validators.required, this.formService.passFormat]))
            }, { validator: this.formService.matchingPasswords('password', 'passwordConfirm') }),
            firstname: new FormControl("", Validators.compose([Validators.required, this.formService.nameFormat])),
            lastname: new FormControl("", Validators.compose([Validators.required, this.formService.nameFormat])),
            city: ['', Validators.required],
            country: ['', Validators.required],
            interests: this.toFormGroup(this.interests)
        });
        this.loginForm = this._fb.group({
            email: ['', [Validators.required, this.formService.mailFormat]],
            password: ['', [Validators.required, Validators.minLength(8)]],
        });
        // subscribe to form changes
        //this.subcribeToFormChanges();
    };
    LoginModal.prototype.subcribeToFormChanges = function () {
        var regFormStatusChanges$ = this.regForm.statusChanges;
        // const regFormValueChanges$ = this.regForm.valueChanges;
        // regFormStatusChanges$.subscribe(x => this.events.push({ event: 'STATUS_CHANGED', object: this.regForm }));
        // regFormValueChanges$.subscribe(x => this.events.push({ event: 'VALUE_CHANGED', object: this.regForm }));
        //regFormStatusChanges$.subscribe(x => console.log(this.regForm));
    };
    LoginModal.prototype.login = function (model, isValid) {
        var _this = this;
        this.UserService.login(model.email, model.password)
            .subscribe(function (data) {
            if (data.success) {
                console.log(data);
                localStorage.setItem('currentUser', JSON.stringify({ token: data.token, user: { id: data.user.id } }));
                _this.UserService.user.uid = data.user.id;
                _this.UserService.user.isLogged = true;
                _this.UserService.user.refId = data.user.refId;
                _this.dialogRef.close();
            }
            else {
            }
        }, function (error) {
            _this.alertService.error(error);
        });
    };
    LoginModal.prototype.beforeDismiss = function () {
        return true;
    };
    LoginModal.prototype.beforeClose = function () {
        return this.wrongAnswer;
    };
    return LoginModal;
}());
LoginModal = tslib_1.__decorate([
    Component({
        selector: 'login-register',
        styleUrls: [
            'login-register.style.scss'
        ],
        templateUrl: 'login-register.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [MdDialogRef, FormBuilder, UserService, AlertService, Router, AuthenticationService, FormService, CookieService])
], LoginModal);
export { LoginModal };
//# sourceMappingURL=login-register.component.js.map