import { Component } from '@angular/core';

@Component({
  selector: 'faq-item',
  styleUrls: [
    'faq-item.style.scss'
  ],
  templateUrl: 'faq-item.template.html'
})
export class FaqItem {
}
