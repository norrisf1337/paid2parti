import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var FaqItem = (function () {
    function FaqItem() {
    }
    return FaqItem;
}());
FaqItem = tslib_1.__decorate([
    Component({
        selector: 'faq-item',
        styleUrls: [
            'faq-item.style.scss'
        ],
        templateUrl: 'faq-item.template.html'
    })
], FaqItem);
export { FaqItem };
//# sourceMappingURL=faq-item.component.js.map