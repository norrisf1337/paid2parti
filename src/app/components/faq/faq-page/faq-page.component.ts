import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';

@Component({
  selector: 'faq',
  entryComponents: [],
  styleUrls: [
    'faq-page.style.scss'
  ],
  providers: [],
  templateUrl: 'faq-page.template.html'
})
export class FaqPage {
	constructor (public hs: HeaderService) {
		this.hs.resetHeader();
		this.hs.setHeader('single', 'Frequently Asked Questions', '');
	}
}
