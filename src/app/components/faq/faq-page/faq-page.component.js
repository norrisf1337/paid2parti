import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
var FaqPage = (function () {
    function FaqPage(hs) {
        this.hs = hs;
        this.hs.resetHeader();
        this.hs.setHeader('single', 'Frequently Asked Questions', '');
    }
    return FaqPage;
}());
FaqPage = tslib_1.__decorate([
    Component({
        selector: 'faq',
        entryComponents: [],
        styleUrls: [
            'faq-page.style.scss'
        ],
        providers: [],
        templateUrl: 'faq-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService])
], FaqPage);
export { FaqPage };
//# sourceMappingURL=faq-page.component.js.map