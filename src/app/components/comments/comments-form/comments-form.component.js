import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormService } from '../../shared/services/form.service';
import { UserService } from '../../shared/services/user.service';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from '../../alerts/';
var CommentsForm = (function () {
    function CommentsForm(_fb, formService, userService, alertService) {
        var _this = this;
        this._fb = _fb;
        this.formService = formService;
        this.userService = userService;
        this.alertService = alertService;
        this.comment = function (data, valid) {
            if (valid) {
                data.promoterId = 'fb4b5736-54e1-ac82-014c-076d20317a0f';
                _this.userService.addComment(data)
                    .subscribe(function (data) {
                    console.log(data);
                    _this.alertService.success('Comment Added');
                }, function (error) {
                    _this.alertService.error('Unable to deliver message, please try again later');
                });
            }
        };
    }
    CommentsForm.prototype.ngOnInit = function () {
        this.commentForm = this._fb.group({
            name: new FormControl("", Validators.compose([Validators.required, this.formService.nameFormat])),
            email: new FormControl("", Validators.compose([Validators.required, this.formService.mailFormat])),
            message: new FormControl("", Validators.compose([Validators.required])),
        });
    };
    return CommentsForm;
}());
CommentsForm = tslib_1.__decorate([
    Component({
        selector: 'comments-form',
        entryComponents: [],
        styleUrls: [
            'comments-form.style.scss'
        ],
        providers: [],
        templateUrl: 'comments-form.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [FormBuilder, FormService, UserService, AlertService])
], CommentsForm);
export { CommentsForm };
//# sourceMappingURL=comments-form.component.js.map