import { Component, OnInit } from '@angular/core';
import { FormService } from '../../shared/services/form.service';
import { UserService } from '../../shared/services/user.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from '../../alerts/'

@Component({
  selector: 'comments-form',
  entryComponents: [],
  styleUrls: [
    'comments-form.style.scss'
  ],
  providers: [],
  templateUrl: 'comments-form.template.html'
})
export class CommentsForm {
	public commentForm: FormGroup;
  constructor(public _fb: FormBuilder, public formService: FormService, public userService: UserService, public alertService: AlertService) {
	}

	ngOnInit() {
    this.commentForm = this._fb.group({
      name: new FormControl("", Validators.compose([Validators.required, this.formService.nameFormat])),
      email: new FormControl("", Validators.compose([Validators.required, this.formService.mailFormat])),
      message: new FormControl("", Validators.compose([Validators.required])),
    });
	}

  public comment = (data: any, valid: boolean) => {
    if (valid) {
      data.promoterId = 'fb4b5736-54e1-ac82-014c-076d20317a0f';
      this.userService.addComment(data)
        .subscribe(
        data => {
          console.log(data);
          this.alertService.success('Comment Added');
        },
        error => {
          this.alertService.error('Unable to deliver message, please try again later');
        })
    }
  }
}
