import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { API } from '../../shared/services/api.service';
import * as _ from 'lodash';
var Comments = (function () {
    function Comments(api) {
        var _this = this;
        this.api = api;
        this.pagOptions = {};
        this.loaded = false;
        this.init = function () {
            _this.pagOptions = {
                currentPage: 1,
                maxPerPage: 4,
                endpoint: '/api/promoter/comments',
                total: undefined,
                callEndpoint: _this.callEndpoint,
                callback: _this.pageCallback
            };
            _this.callEndpoint(0);
        };
        this.callEndpoint = function (page) {
            var offset = (page === 0 || page === 1) ? 0 : (page - 1) * _this.pagOptions.maxPerPage;
            _this.api.get(_this.pagOptions.endpoint, { promoterId: 'fb4b5736-54e1-ac82-014c-076d20317a0f', paged: true, offset: offset, limit: _this.pagOptions.maxPerPage })
                .subscribe(function (data) {
                if (!_.isUndefined(_this.pagOptions.callback)) {
                    _this.pagOptions.callback(data.comments);
                    _this.pagOptions.total = data.count;
                    _this.pagOptions.currentPage = (page === 0) ? 1 : page;
                    _this.loaded = true;
                }
            }, function (error) {
            });
        };
        this.pageCallback = function (data) {
            _this.comments = data;
        };
        console.log('comm', this);
        this.init();
    }
    return Comments;
}());
Comments = tslib_1.__decorate([
    Component({
        selector: 'comments',
        entryComponents: [],
        styleUrls: [
            'comments.style.scss'
        ],
        providers: [],
        templateUrl: 'comments.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [API])
], Comments);
export { Comments };
//# sourceMappingURL=comments.component.js.map