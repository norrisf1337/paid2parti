import { Component } from '@angular/core';
import { API } from '../../shared/services/api.service';
import * as _ from 'lodash';

@Component({
  selector: 'comments',
  entryComponents: [],
  styleUrls: [
    'comments.style.scss'
  ],
  providers: [],
  templateUrl: 'comments.template.html'
})
export class Comments {
	public comments;
	constructor (public api: API) {
		console.log('comm', this)
		this.init();
	}
	public pagOptions: any = {};
	public loaded: boolean = false;

	public init = () => {
		this.pagOptions = {
			currentPage: 1,
			maxPerPage: 4,
			endpoint: '/api/promoter/comments',
			total: undefined,
			callEndpoint: this.callEndpoint,
			callback: this.pageCallback
		};
		this.callEndpoint(0);
	}

	public callEndpoint = (page) => {
		let offset = (page === 0 || page === 1 ) ? 0 : (page - 1) * this.pagOptions.maxPerPage;
		this.api.get(this.pagOptions.endpoint, {promoterId: 'fb4b5736-54e1-ac82-014c-076d20317a0f', paged: true, offset: offset, limit: this.pagOptions.maxPerPage})
		.subscribe(
        data => {
          if (!_.isUndefined(this.pagOptions.callback)) {
          	this.pagOptions.callback(data.comments)
          	this.pagOptions.total = data.count;
          	this.pagOptions.currentPage = (page === 0) ? 1 : page;
          	this.loaded = true;
          }
        },
        error => {
        })
	}

	public pageCallback = (data) => {
		this.comments = data;
	}
}
