import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
var ProfileDashboardPage = (function () {
    function ProfileDashboardPage(hs) {
        this.hs = hs;
        this.hs.resetHeader();
        this.hs.setHeader('single', 'Profile Dashboard', '');
    }
    return ProfileDashboardPage;
}());
ProfileDashboardPage = tslib_1.__decorate([
    Component({
        selector: 'profile-dashboard',
        entryComponents: [],
        styleUrls: [
            'profile-dashboard-page.style.scss'
        ],
        providers: [],
        templateUrl: 'profile-dashboard-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService])
], ProfileDashboardPage);
export { ProfileDashboardPage };
//# sourceMappingURL=profile-dashboard-page.component.js.map