import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';

@Component({
  selector: 'profile-dashboard',
  entryComponents: [],
  styleUrls: [
    'profile-dashboard-page.style.scss'
  ],
  providers: [],
  templateUrl: 'profile-dashboard-page.template.html'
})
export class ProfileDashboardPage {
	constructor (public hs: HeaderService){
		this.hs.resetHeader();
		this.hs.setHeader('single', 'Profile Dashboard', '');
	}
}
