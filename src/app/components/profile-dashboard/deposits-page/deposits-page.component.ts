import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { FormService } from '../../shared/services/form.service';
import { HeaderService } from '../../shared/services/header.service';
import { UserService } from '../../shared/services/user.service';
import { AlertService } from '../../alerts/';

@Component({
  selector: 'deposits',
  entryComponents: [],
  styleUrls: [
    'deposits-page.style.scss'
  ],
  providers: [],
  templateUrl: 'deposits-page.template.html'
})

export class DepositsPage {

  public email: string = '';
  public depositForm;
  constructor(public hs: HeaderService, public _fb: FormBuilder, public formService: FormService, public userService: UserService, public alertService: AlertService) {
		this.hs.resetHeader();
		this.hs.setHeader('single', 'Deposit Info', '');
	}

  ngOnInit() {
    this.userService.getDetails('account.depositAccount')
      .subscribe(
      data => {
        console.log('d', data);
        this.email = data.account.depositAccount || '';
        this.depositForm.patchValue({ email: this.email });
      },
      error => {
      })
    this.depositForm = this._fb.group({
      email: [this.email, [<any>Validators.required, this.formService.mailFormat]],
    });

  }

  public submitDeposit = (data: any, valid: boolean) => {
    if (valid) {
      this.userService.setDetails({ account: { depositAccount: data.email } })
        .subscribe(
        data => {
          this.alertService.success('Deposit information updated.');
        },
        error => {
          this.alertService.error('There was a problem updating deposit information, Please contact support or try again later.');
        })
    }
  }
}
