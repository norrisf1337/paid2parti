import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormService } from '../../shared/services/form.service';
import { HeaderService } from '../../shared/services/header.service';
import { UserService } from '../../shared/services/user.service';
import { AlertService } from '../../alerts/';
var DepositsPage = (function () {
    function DepositsPage(hs, _fb, formService, userService, alertService) {
        var _this = this;
        this.hs = hs;
        this._fb = _fb;
        this.formService = formService;
        this.userService = userService;
        this.alertService = alertService;
        this.email = '';
        this.submitDeposit = function (data, valid) {
            if (valid) {
                _this.userService.setDetails({ account: { depositAccount: data.email } })
                    .subscribe(function (data) {
                    _this.alertService.success('Deposit information updated.');
                }, function (error) {
                    _this.alertService.error('There was a problem updating deposit information, Please contact support or try again later.');
                });
            }
        };
        this.hs.resetHeader();
        this.hs.setHeader('single', 'Deposit Info', '');
    }
    DepositsPage.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.getDetails('account.depositAccount')
            .subscribe(function (data) {
            console.log('d', data);
            _this.email = data.account.depositAccount || '';
            _this.depositForm.patchValue({ email: _this.email });
        }, function (error) {
        });
        this.depositForm = this._fb.group({
            email: [this.email, [Validators.required, this.formService.mailFormat]],
        });
    };
    return DepositsPage;
}());
DepositsPage = tslib_1.__decorate([
    Component({
        selector: 'deposits',
        entryComponents: [],
        styleUrls: [
            'deposits-page.style.scss'
        ],
        providers: [],
        templateUrl: 'deposits-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService, FormBuilder, FormService, UserService, AlertService])
], DepositsPage);
export { DepositsPage };
//# sourceMappingURL=deposits-page.component.js.map