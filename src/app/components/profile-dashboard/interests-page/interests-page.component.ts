import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';

@Component({
  selector: 'interests',
  entryComponents: [],
  styleUrls: [
    'interests-page.style.scss'
  ],
  providers: [],
  templateUrl: 'interests-page.template.html'
})
export class InterestsPage {
	constructor (public hs: HeaderService){
		this.hs.resetHeader();
		this.hs.setHeader('single', 'Interests', '');
	}
}
