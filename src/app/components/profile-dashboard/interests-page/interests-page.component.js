import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
var InterestsPage = (function () {
    function InterestsPage(hs) {
        this.hs = hs;
        this.hs.resetHeader();
        this.hs.setHeader('single', 'Interests', '');
    }
    return InterestsPage;
}());
InterestsPage = tslib_1.__decorate([
    Component({
        selector: 'interests',
        entryComponents: [],
        styleUrls: [
            'interests-page.style.scss'
        ],
        providers: [],
        templateUrl: 'interests-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService])
], InterestsPage);
export { InterestsPage };
//# sourceMappingURL=interests-page.component.js.map