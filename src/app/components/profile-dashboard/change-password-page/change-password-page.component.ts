import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
import { UserService } from '../../shared/services/user.service';
import { FormService } from '../../shared/services/form.service';
import { AlertService } from '../../alerts/';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'change-password',
  entryComponents: [],
  styleUrls: [
    'change-password-page.style.scss'
  ],
  providers: [],
  templateUrl: 'change-password-page.template.html'
})
export class PasswordPage {
  public passwordForm: FormGroup;
	constructor(public hs: HeaderService, public _fb: FormBuilder, public userService: UserService, public formService: FormService, public alertService: AlertService) {
		this.hs.resetHeader();
		this.hs.setHeader('single', 'Change Password', '');
	}

  ngOnInit() {

    this.passwordForm = this._fb.group({
      currentPass: new FormControl("", Validators.compose([Validators.required, this.formService.passFormat])),
      newPass: new FormControl("", Validators.compose([Validators.required, this.formService.passFormat])),
      newPassConfirm: new FormControl("", Validators.compose([Validators.required, this.formService.passFormat])),
    },{ validator: this.formService.matchingPasswords('newPass', 'newPassConfirm') });
  }



  public changePass = (values: any, valid: boolean) => {
    if (valid) {
      this.userService.setPassword({ previousPass: values.currentPass, newPass: values.newPass })
        .subscribe(
        data => {
          this.alertService.success('Password updated successfully.');
        },
        error => {
          this.alertService.error('There was a problem updating password, Please contact support or try again later.');
        })
    }
  }
}
