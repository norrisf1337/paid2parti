import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
import { UserService } from '../../shared/services/user.service';
import { FormService } from '../../shared/services/form.service';
import { AlertService } from '../../alerts/';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
var PasswordPage = (function () {
    function PasswordPage(hs, _fb, userService, formService, alertService) {
        var _this = this;
        this.hs = hs;
        this._fb = _fb;
        this.userService = userService;
        this.formService = formService;
        this.alertService = alertService;
        this.changePass = function (values, valid) {
            if (valid) {
                _this.userService.setPassword({ previousPass: values.currentPass, newPass: values.newPass })
                    .subscribe(function (data) {
                    _this.alertService.success('Password updated successfully.');
                }, function (error) {
                    _this.alertService.error('There was a problem updating password, Please contact support or try again later.');
                });
            }
        };
        this.hs.resetHeader();
        this.hs.setHeader('single', 'Change Password', '');
    }
    PasswordPage.prototype.ngOnInit = function () {
        this.passwordForm = this._fb.group({
            currentPass: new FormControl("", Validators.compose([Validators.required, this.formService.passFormat])),
            newPass: new FormControl("", Validators.compose([Validators.required, this.formService.passFormat])),
            newPassConfirm: new FormControl("", Validators.compose([Validators.required, this.formService.passFormat])),
        }, { validator: this.formService.matchingPasswords('newPass', 'newPassConfirm') });
    };
    return PasswordPage;
}());
PasswordPage = tslib_1.__decorate([
    Component({
        selector: 'change-password',
        entryComponents: [],
        styleUrls: [
            'change-password-page.style.scss'
        ],
        providers: [],
        templateUrl: 'change-password-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService, FormBuilder, UserService, FormService, AlertService])
], PasswordPage);
export { PasswordPage };
//# sourceMappingURL=change-password-page.component.js.map