import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
import { FormService } from '../../shared/services/form.service';
import { UserService } from '../../shared/services/user.service';
import { AlertService } from '../../alerts/';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'personal-details',
  entryComponents: [],
  styleUrls: [
    'personal-details-page.style.scss'
  ],
  providers: [],
  templateUrl: 'personal-details-page.template.html'
})
export class PersonalDetailsPage {
  public regForm;
	constructor(public hs: HeaderService, public _fb: FormBuilder, private formService: FormService, private userService: UserService, public alertService: AlertService) {
		this.hs.resetHeader();
		this.hs.setHeader('single', 'Personal Details', '');
	}

  ngOnInit() {

    this.regForm = this._fb.group({
      emails: this._fb.group({
        email: new FormControl("", Validators.compose([Validators.required, this.formService.mailFormat])),
        emailConfirm: new FormControl("", Validators.compose([Validators.required, this.formService.mailFormat]))
      }, { validator: this.formService.matchingPasswords('email', 'emailConfirm') }),
      firstname: new FormControl("", Validators.compose([this.formService.nameFormat])),
      lastname: new FormControl("", Validators.compose([this.formService.nameFormat])),
      city: [''],
      country: ['']
    });

    this.userService.getDetails('credentials.email personal')
      .subscribe(
      data => {
        console.log('d', data);
        let tempObj = {
          emails: {
            email: data.credentials.email || '',
            emailConfirm: data.credentials.email || ''
          },
          firstname: data.personal.firstName || '',
          lastname: data.personal.lastName || '',
          city: data.personal.city || '',
          country: data.personal.country || '',
        }

        this.regForm.patchValue(tempObj);

      },
      error => {
      })
  }

  public update = (data: any, valid: boolean) => {
    if (valid) {
      let newUser = {
        credentials: {
          email: data.emails.email,
        },
        personal: {
          firstName: data.firstname,
          lastName: data.lastname,
          city: data.city,
          country: data.country
        }
      };
      this.userService.setDetails(newUser)
        .subscribe(
        data => {
          this.alertService.success('Personal details updated.');
        },
        error => {
          this.alertService.error('There was a problem updating details, Please contact support or try again later.');
        })
    }
  }
}
