import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HeaderService } from '../../shared/services/header.service';
import { FormService } from '../../shared/services/form.service';
import { UserService } from '../../shared/services/user.service';
import { AlertService } from '../../alerts/';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
var PersonalDetailsPage = (function () {
    function PersonalDetailsPage(hs, _fb, formService, userService, alertService) {
        var _this = this;
        this.hs = hs;
        this._fb = _fb;
        this.formService = formService;
        this.userService = userService;
        this.alertService = alertService;
        this.update = function (data, valid) {
            if (valid) {
                var newUser = {
                    credentials: {
                        email: data.emails.email,
                    },
                    personal: {
                        firstName: data.firstname,
                        lastName: data.lastname,
                        city: data.city,
                        country: data.country
                    }
                };
                _this.userService.setDetails(newUser)
                    .subscribe(function (data) {
                    _this.alertService.success('Personal details updated.');
                }, function (error) {
                    _this.alertService.error('There was a problem updating details, Please contact support or try again later.');
                });
            }
        };
        this.hs.resetHeader();
        this.hs.setHeader('single', 'Personal Details', '');
    }
    PersonalDetailsPage.prototype.ngOnInit = function () {
        var _this = this;
        this.regForm = this._fb.group({
            emails: this._fb.group({
                email: new FormControl("", Validators.compose([Validators.required, this.formService.mailFormat])),
                emailConfirm: new FormControl("", Validators.compose([Validators.required, this.formService.mailFormat]))
            }, { validator: this.formService.matchingPasswords('email', 'emailConfirm') }),
            firstname: new FormControl("", Validators.compose([this.formService.nameFormat])),
            lastname: new FormControl("", Validators.compose([this.formService.nameFormat])),
            city: [''],
            country: ['']
        });
        this.userService.getDetails('credentials.email personal')
            .subscribe(function (data) {
            console.log('d', data);
            var tempObj = {
                emails: {
                    email: data.credentials.email || '',
                    emailConfirm: data.credentials.email || ''
                },
                firstname: data.personal.firstName || '',
                lastname: data.personal.lastName || '',
                city: data.personal.city || '',
                country: data.personal.country || '',
            };
            _this.regForm.patchValue(tempObj);
        }, function (error) {
        });
    };
    return PersonalDetailsPage;
}());
PersonalDetailsPage = tslib_1.__decorate([
    Component({
        selector: 'personal-details',
        entryComponents: [],
        styleUrls: [
            'personal-details-page.style.scss'
        ],
        providers: [],
        templateUrl: 'personal-details-page.template.html'
    }),
    tslib_1.__metadata("design:paramtypes", [HeaderService, FormBuilder, FormService, UserService, AlertService])
], PersonalDetailsPage);
export { PersonalDetailsPage };
//# sourceMappingURL=personal-details-page.component.js.map