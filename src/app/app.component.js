import * as tslib_1 from "tslib";
import { Component, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Menus } from './components/menu/menu.component';
import { Footer } from './components/footer/footer.component';
import { AlertComponent } from './components/alerts/alert.component';
import { UserService } from './components/shared/services/user.service';
import { NotificationService } from './components/shared/services/notification.service';
import * as _ from 'lodash';
import { CookieService } from 'angular2-cookie/core';
var jwt = require('jwt-decode');
var App = (function () {
    function App(userService, router, route, noteService, CookieService) {
        //this.init();
        this.userService = userService;
        this.router = router;
        this.route = route;
        this.noteService = noteService;
        this.CookieService = CookieService;
        this.home = false;
        this.loading = true;
    }
    App.prototype.ngOnInit = function () {
        var _this = this;
        var hasRef = this.CookieService.get('ref');
        this.router.events.subscribe(function (evt) {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            document.body.scrollTop = 0;
        });
        if (localStorage.getItem('currentUser')) {
            var currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (!_.isUndefined(currentUser.token)) {
                var decoded = jwt(currentUser.token);
                if (decoded.exp < new Date().getTime()) {
                    console.log('this');
                    this.userService.user.uid = currentUser.user.id;
                    this.userService.user.isLogged = true;
                    // this.noteService.create({
                    // 	id: this.userService.generateUid(),
                    // 	recipient_id: this.userService.user.uid,
                    // 	sender_id: 'p2p',
                    // 	conversation: false,
                    // 	conversation_id: '',
                    // 	messages: 'This is a test message, testing, testing',
                    // 	created: moment().format('MMM DD YYYY hh:mm A'),
                    // 	last_updated: moment().format('MMM DD YYYY hh:mm A'),
                    // 	type: 'General',
                    // 	read: false
                    // })
                    // 	.subscribe(
                    // 	data => {
                    // 		if (data.success) {
                    // 		}
                    // 	},
                    // 	error => {
                    // 	});
                }
                else {
                    this.router.navigate(['/']);
                }
            }
        }
        else {
            console.log('refs', hasRef);
            this.subscription = this.route
                .queryParams
                .subscribe(function (params) {
                if (!_.isUndefined(params['ref'])) {
                    console.log(params['ref']);
                    _this.CookieService.put('ref', params['ref']);
                    localStorage.setItem('refId', params['ref']);
                }
            });
        }
    };
    return App;
}());
App = tslib_1.__decorate([
    Component({
        selector: 'app',
        templateUrl: './app.template.html',
        encapsulation: ViewEncapsulation.None,
        styleUrls: [
            'app.style.scss'
        ],
        entryComponents: [Menus, Footer, AlertComponent]
    }),
    tslib_1.__metadata("design:paramtypes", [UserService, Router, ActivatedRoute, NotificationService, CookieService])
], App);
export { App };
//# sourceMappingURL=app.component.js.map