const nodemailer = require('nodemailer');
import { MailGenerator } from './template-generator/generator'

export class PartiMailer {

    public mailTemplate: any;
    public mailOptions = {
        from: '"p2p" <p2p@p2p.com>', // sender address
        to: this.to, // list of receivers
        subject: '', // Subject line
        html: '' // html body
    };
    public mailGenerator = new MailGenerator();
    constructor(public to, public type, public details) {
        this.to = to;
        this.type = type;
        this.details = details;

        this.init();
        console.log(this);
    }

    // create reusable transporter object using the default SMTP transport
    public transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'norris@norrisfrancis.ca',
            pass: 'bigups911'
        }
    });

    public setMailOptions = (type) => {

    }

    public sendMail = () => {
        this.transporter.sendMail(this.mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
        });
    }

    public init = () => {
        if (this.type === 'userInvite') {
            this.mailOptions.html = this.mailGenerator.generateInvite('Hello', 'this is an invitiation', 'Sign Up Now', 'http://localhost:8080?ref=' + this.details.uid);
            this.mailOptions.subject = 'P2P Inv';
            this.sendMail();
        }
        if (this.type === 'contact') {
            this.mailOptions.html = this.mailGenerator.generateContact(this.details);
            this.mailOptions.subject = 'P2P Contact Form';
            this.mailOptions.from = '"'+this.details.name +'" <'+this.details.email+'>';
            this.mailOptions.to = 'norris@norrisfrancis.ca';
            this.sendMail();
        }
    }
}
