var nodemailer = require('nodemailer');
import { MailGenerator } from './template-generator/generator';
var PartiMailer = (function () {
    function PartiMailer(to, type, details) {
        var _this = this;
        this.to = to;
        this.type = type;
        this.details = details;
        this.mailOptions = {
            from: '"p2p" <p2p@p2p.com>',
            to: this.to,
            subject: '',
            html: '' // html body
        };
        this.mailGenerator = new MailGenerator();
        // create reusable transporter object using the default SMTP transport
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'norris@norrisfrancis.ca',
                pass: 'bigups911'
            }
        });
        this.setMailOptions = function (type) {
        };
        this.sendMail = function () {
            _this.transporter.sendMail(_this.mailOptions, function (error, info) {
                if (error) {
                    return console.log(error);
                }
                console.log('Message %s sent: %s', info.messageId, info.response);
            });
        };
        this.init = function () {
            if (_this.type === 'userInvite') {
                _this.mailOptions.html = _this.mailGenerator.generateInvite('Hello', 'this is an invitiation', 'Sign Up Now', 'http://localhost:8080?ref=' + _this.details.uid);
                _this.mailOptions.subject = 'P2P Inv';
                _this.sendMail();
            }
            if (_this.type === 'contact') {
                _this.mailOptions.html = _this.mailGenerator.generateContact(_this.details);
                _this.mailOptions.subject = 'P2P Contact Form';
                _this.mailOptions.from = '"' + _this.details.name + '" <' + _this.details.email + '>';
                _this.mailOptions.to = 'norris@norrisfrancis.ca';
                _this.sendMail();
            }
        };
        this.to = to;
        this.type = type;
        this.details = details;
        this.init();
        console.log(this);
    }
    return PartiMailer;
}());
export { PartiMailer };
//# sourceMappingURL=mailer.js.map