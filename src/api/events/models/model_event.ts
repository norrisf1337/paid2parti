import { default as eventTicket } from './model_eventTicket';
import * as _ from 'lodash';
// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

const Event = new mongoose.Schema({
  _id: String,
  owner: String,
  status: String,
  basicDetails: {
    name: String,
    description: String,
    venue: String,
    image: String,
    thumbnail: String,
    category: String,
    startDate: String,
    endDate: String,
    startTime: String,
    endTime: String
  },
  location: {
    address: String,
    city: String,
    province: String,
    geo: {
        type: { type: String },
        coordinates: [Number],
    },
    latitude: Number,
    longitude: Number,
    postal: String,
    country: String
  },
  organizer: [{ type: String, ref: 'promoter' }],
  tickets: [{ type: String, ref: 'eventTickets' }],
  attendees:[Object],
  ticketsSold: Boolean,
  earnings: {type:Number, default:0},

});

Event.index({'basicDetails.name': 'text','basicDetails.description': 'text', 'basicDetails.venue': 'text', 'basicDetails.category': 'text', 'basicDetails.startTime': 'text','location.address': 'text','location.city': 'text', 'location.province': 'text','location.country': 'text'},{'name':'eventIndex'});
Event.index({'location.geo': '2dsphere'});

Event.methods.updateTickets = function(tickets, cb) {
  let pro = [];

  function onInsert(err, docs) {
    if (err) {
        console.log(err)
    } else {
        console.info('%d potatoes were successfully stored.', docs.length);
        cb();
    }
  }

  console.log('t', this);
  console.log('tickets', tickets);
  console.log('et', eventTicket);
  //eventTicket.insertMany(tickets, onInsert).upsert(true);
  _.forEach(tickets, (o,i) => {
    let ticket = eventTicket.findOneAndUpdate({'_id': o._id}, o, {upsert:true, new:true}, function(err, doc){
      if (err) console.log(err)
  //res.json({ success: true, doc:doc });
    });
    pro.push(ticket);
  });

  Promise.all(pro).then(() => {
    cb();
  });

};
export default mongoose.model('event', Event);

