// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const eventPromoter = new mongoose.Schema ({
  event_Id: String,
  cost: Number,
  description: String,
  qty: Number,
  currency: String,
  cat: String,
  _id: String,
  available: Number,
  sold: Number
});

export default mongoose.model('eventPromoters', eventPromoter);

