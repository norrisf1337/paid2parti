import * as express from 'express';
import * as _ from 'lodash';
import { default as Event } from '../models/model_event';
let router = express.Router();
let jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens


router.post('/', function(req, res, next) {
  console.log(req.body);
  let query: any = {};
  let options: any = {};
	let limit = req.body.limit || 0;
  let skip = req.body.offset || 0;
  let keywords = req.body.filters.keywords || undefined;
  let category = req.body.filters.category || undefined;
  let price = req.body.filters.price || undefined;
  let geo = req.body.filters.geo || undefined;

  query.status = 'published';

  if (!_.isUndefined(keywords) && keywords !== '') {

    if (!_.isUndefined(geo) && typeof geo === 'string') {
      keywords += ' ' + geo;
    }

    if (!_.isUndefined(geo) && typeof geo !== 'string') {
      query['$or'] = [
        { 'basicDetails.name': { "$regex": keywords, "$options": 'i' } },
        { 'basicDetails.venue': { "$regex": keywords, "$options": 'i' } },
        { 'basicDetails.description': { "$regex": keywords, "$options": 'i' } },
        { 'basicDetails.category': { "$regex": keywords, "$options": 'i' } },
        { 'basicDetails.startTime': { "$regex": keywords, "$options": 'i' } },
        { 'location.city': { "$regex": keywords, "$options": 'i' } },
        { 'location.province': { "$regex": keywords, "$options": 'i' } },
        { 'location.country': { "$regex": keywords, "$options": 'i' } }
      ];
    } else {
      query['$text'] = { '$search': keywords, '$caseSensitive': false };
    }
  }

  if (!_.isUndefined(category && category !== '')) {
    query['basicDetails.category'] = category;
  }

  if (!_.isUndefined(req.body.filters)) {
    if (!_.isUndefined(geo) && typeof geo === 'string') {
      if (!_.isUndefined(query.$text)) {
        query.$text.$search += ' ' + geo;
      } else {
        query['$text'] = { '$search': geo, '$caseSensitive': false };
      }
    }
    if (!_.isUndefined(geo) && typeof geo !== 'string') {
      console.log('opt');
      query['location.geo'] = {
        $geoNear:
        {
          $geometry: {
            type: "Point",
            coordinates: [geo.lng, geo.lat]
          },
          $maxDistance: 20000
        }
      }
    }
  }


  //options

  if (!_.isUndefined(req.body.limit)) {
    options['limit'] = req.body.limit;
  }

  if (!_.isUndefined(req.body.offset)) {
    options['skip'] = req.body.offset;
  }

  options['sort'] = { 'basicDetails.startTime': -1 };

  console.log('q', query);

  Event.find(query, {}, options, function(err, events) {
    if (err) console.log(err);

    Event.count(query, function(err, count) {
      if (err) {
        return next(err);
      }
      res.send({ count: count, events: events });
    });
  });

});

router.post('/user', function(req, res) {
  Event.find({ 'owner': req.body.id }, function(err, events) {
    res.json(events);
  }).populate('tickets');
});

router.post('/insert', (req, res) => {

  Event.find({}, function(err, events) {
    if (err) {
      console.log(err)
    }
    _.forEach(events, (o, i) => {
      Event.findOneAndUpdate({ '_id': o._id }, { 'location.geo.type': 'Point', 'location.geo.coordinates': [o.location.longitude, o.location.latitude] }, { upsert: true }, function(err, doc) {
        if (err) {
          console.log(err)
        }
        //res.json({ success: true, doc:doc });
      });
    });
  })


  // 	let events = [
  //   {
  //     "_id": "32efiiyuf-e6c1-219b-534f-23145",
  //     "__v": 0,
  //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
  //     "status": "published",
  //     "basicDetails": {
  //       "endTime": "2017-02-22T16:00:59.966Z",
  //       "startTime": "2017-02-23T02:00:56.064Z",
  //       "endDate": "2017-03-03T05:00:00.000Z",
  //       "startDate": "2017-03-03T05:00:00.000Z",
  //       "category": "Dances",
  //       "thumbnail": "",
  //       "image": "create-event-small.jpeg",
  //       "venue": "Testing a venue name",
  //       "description": "This is a description for the above event this and that and some more",
  //       "name": "Testing an event and populating the site"
  //     },
  //     "location": {
  //       "country": "Canada",
  //       "longitude": -79.111807,
  //       "latitude": 43.812724,
  //       "postal": "L1W 3M3",
  //       "province": "Ontario",
  //       "city": "Pickering",
  //       "address": "775 Hampton Court"
  //     },
  //     "organizer": "700ce97b-50d9-b44a-e740-4dd257f9b541",
  //     "tickets": [
  //       "47835d12-74e5-1f3c-7f97-b537bf4c4d48"
  //     ],
  //     "ticketsSold": false,
  //     "earnings": 0,
  //     "attendees": []
  //   },
  //   {
  //     "_id": "ed49hjk5-2c0e-4613-d34f-23145",
  //     "__v": 0,
  //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
  //     "status": "published",
  //     "basicDetails": {
  //       "endTime": "2017-02-23T03:00:41.001Z",
  //       "startTime": "2017-02-22T23:00:35.359Z",
  //       "endDate": "2017-03-10T05:00:00.000Z",
  //       "startDate": "2017-03-10T05:00:00.000Z",
  //       "category": "Sports",
  //       "thumbnail": "",
  //       "image": "soccor-stadium.jpeg",
  //       "venue": "Some big soccer stadium",
  //       "description": "This is the second match up between these teams, Team 1 seeks revenge after a big loss during thier last meeting.",
  //       "name": "Team 1 vs Team 2"
  //     },
  //     "location": {
  //       "country": "Canada",
  //       "longitude": -79.38935320000002,
  //       "latitude": 43.6414378,
  //       "postal": "M5V 1J1",
  //       "province": "Ontario",
  //       "city": "Toronto",
  //       "address": "1 Blue Jays Way"
  //     },
  //     "organizer": "262078c2-7052-7a19-baf0-649e374e99df",
  //     "tickets": [
  //       "77756e33-79e5-57c4-7b6b-e46a1ef07c20",
  //       "05fd5d03-996e-fcbf-45b7-01989030ed8f",
  //       "c8cff8bc-9710-4e09-58ec-f35727b33668"
  //     ],
  //     "ticketsSold": false,
  //     "earnings": 0,
  //     "attendees": []
  //   },
  //   {
  //     "_id": "004626ee-940f-ejhkc-034f-23145",
  //     "__v": 0,
  //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
  //     "status": "published",
  //     "basicDetails": {
  //       "endTime": "2017-02-23T02:00:43.939Z",
  //       "startTime": "2017-02-22T22:00:36.730Z",
  //       "endDate": "2017-03-22T04:00:00.000Z",
  //       "startDate": "2017-03-22T04:00:00.000Z",
  //       "category": "Dances",
  //       "thumbnail": "",
  //       "image": "concert-image-2.jpeg",
  //       "venue": "Rileys Pub",
  //       "description": "DJ Something or Another spins hot tracks all night on the ones and twos, be prepared for the best concert ever.",
  //       "name": "DJ Something or Another"
  //     },
  //     "location": {
  //       "country": "Canada",
  //       "longitude": -78.8597891,
  //       "latitude": 43.898611,
  //       "postal": "L1H 1B6",
  //       "province": "Ontario",
  //       "city": "Oshawa",
  //       "address": "104 King Street East"
  //     },
  //     "organizer": "ac07e073-20b0-e0a8-55d3-04fe5a20a763",
  //     "tickets": [
  //       "750a0c2b-86c3-7bfb-b064-97ebeb5923b7"
  //     ],
  //     "ticketsSold": false,
  //     "earnings": 0,
  //     "attendees": []
  //   },
  //     {
  //     "_id": "32ebnm50f-e6c1-219b-534f-23145",
  //     "__v": 0,
  //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
  //     "status": "published",
  //     "basicDetails": {
  //       "endTime": "2017-02-22T16:00:59.966Z",
  //       "startTime": "2017-02-23T02:00:56.064Z",
  //       "endDate": "2017-03-03T05:00:00.000Z",
  //       "startDate": "2017-03-03T05:00:00.000Z",
  //       "category": "Dances",
  //       "thumbnail": "",
  //       "image": "create-event-small.jpeg",
  //       "venue": "Testing a venue name",
  //       "description": "This is a description for the above event this and that and some more",
  //       "name": "Testing an event and populating the site"
  //     },
  //     "location": {
  //       "country": "Canada",
  //       "longitude": -79.111807,
  //       "latitude": 43.812724,
  //       "postal": "L1W 3M3",
  //       "province": "Ontario",
  //       "city": "Pickering",
  //       "address": "775 Hampton Court"
  //     },
  //     "organizer": "700ce97b-50d9-b44a-e740-4dd257f9b541",
  //     "tickets": [
  //       "47835d12-74e5-1f3c-7f97-b537bf4c4d48"
  //     ],
  //     "ticketsSold": false,
  //     "earnings": 0,
  //     "attendees": []
  //   },
  //   {
  //     "_id": "evbna95-2c0e-4613-d34f-23145",
  //     "__v": 0,
  //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
  //     "status": "published",
  //     "basicDetails": {
  //       "endTime": "2017-02-23T03:00:41.001Z",
  //       "startTime": "2017-02-22T23:00:35.359Z",
  //       "endDate": "2017-03-10T05:00:00.000Z",
  //       "startDate": "2017-03-10T05:00:00.000Z",
  //       "category": "Sports",
  //       "thumbnail": "",
  //       "image": "soccor-stadium.jpeg",
  //       "venue": "Some big soccer stadium",
  //       "description": "This is the second match up between these teams, Team 1 seeks revenge after a big loss during thier last meeting.",
  //       "name": "Team 1 vs Team 2"
  //     },
  //     "location": {
  //       "country": "Canada",
  //       "longitude": -79.38935320000002,
  //       "latitude": 43.6414378,
  //       "postal": "M5V 1J1",
  //       "province": "Ontario",
  //       "city": "Toronto",
  //       "address": "1 Blue Jays Way"
  //     },
  //     "organizer": "262078c2-7052-7a19-baf0-649e374e99df",
  //     "tickets": [
  //       "77756e33-79e5-57c4-7b6b-e46a1ef07c20",
  //       "05fd5d03-996e-fcbf-45b7-01989030ed8f",
  //       "c8cff8bc-9710-4e09-58ec-f35727b33668"
  //     ],
  //     "ticketsSold": false,
  //     "earnings": 0,
  //     "attendees": []
  //   },
  //   {
  //     "_id": "004ersdee-940f-e3cc-034f-23145",
  //     "__v": 0,
  //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
  //     "status": "published",
  //     "basicDetails": {
  //       "endTime": "2017-02-23T02:00:43.939Z",
  //       "startTime": "2017-02-22T22:00:36.730Z",
  //       "endDate": "2017-03-22T04:00:00.000Z",
  //       "startDate": "2017-03-22T04:00:00.000Z",
  //       "category": "Dances",
  //       "thumbnail": "",
  //       "image": "concert-image-2.jpeg",
  //       "venue": "Rileys Pub",
  //       "description": "DJ Something or Another spins hot tracks all night on the ones and twos, be prepared for the best concert ever.",
  //       "name": "DJ Something or Another"
  //     },
  //     "location": {
  //       "country": "Canada",
  //       "longitude": -78.8597891,
  //       "latitude": 43.898611,
  //       "postal": "L1H 1B6",
  //       "province": "Ontario",
  //       "city": "Oshawa",
  //       "address": "104 King Street East"
  //     },
  //     "organizer": "ac07e073-20b0-e0a8-55d3-04fe5a20a763",
  //     "tickets": [
  //       "750a0c2b-86c3-7bfb-b064-97ebeb5923b7"
  //     ],
  //     "ticketsSold": false,
  //     "earnings": 0,
  //     "attendees": []
  //   },
  //     {
  //     "_id": "32ef67f-e6c1-219b-534f-23145",
  //     "__v": 0,
  //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
  //     "status": "published",
  //     "basicDetails": {
  //       "endTime": "2017-02-22T16:00:59.966Z",
  //       "startTime": "2017-02-23T02:00:56.064Z",
  //       "endDate": "2017-03-03T05:00:00.000Z",
  //       "startDate": "2017-03-03T05:00:00.000Z",
  //       "category": "Dances",
  //       "thumbnail": "",
  //       "image": "create-event-small.jpeg",
  //       "venue": "Testing a venue name",
  //       "description": "This is a description for the above event this and that and some more",
  //       "name": "Testing an event and populating the site"
  //     },
  //     "location": {
  //       "country": "Canada",
  //       "longitude": -79.111807,
  //       "latitude": 43.812724,
  //       "postal": "L1W 3M3",
  //       "province": "Ontario",
  //       "city": "Pickering",
  //       "address": "775 Hampton Court"
  //     },
  //     "organizer": "700ce97b-50d9-b44a-e740-4dd257f9b541",
  //     "tickets": [
  //       "47835d12-74e5-1f3c-7f97-b537bf4c4d48"
  //     ],
  //     "ticketsSold": false,
  //     "earnings": 0,
  //     "attendees": []
  //   },
  //   {
  //     "_id": "edqwea95-2c0e-4613-d34f-23145",
  //     "__v": 0,
  //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
  //     "status": "published",
  //     "basicDetails": {
  //       "endTime": "2017-02-23T03:00:41.001Z",
  //       "startTime": "2017-02-22T23:00:35.359Z",
  //       "endDate": "2017-03-10T05:00:00.000Z",
  //       "startDate": "2017-03-10T05:00:00.000Z",
  //       "category": "Sports",
  //       "thumbnail": "",
  //       "image": "soccor-stadium.jpeg",
  //       "venue": "Some big soccer stadium",
  //       "description": "This is the second match up between these teams, Team 1 seeks revenge after a big loss during thier last meeting.",
  //       "name": "Team 1 vs Team 2"
  //     },
  //     "location": {
  //       "country": "Canada",
  //       "longitude": -79.38935320000002,
  //       "latitude": 43.6414378,
  //       "postal": "M5V 1J1",
  //       "province": "Ontario",
  //       "city": "Toronto",
  //       "address": "1 Blue Jays Way"
  //     },
  //     "organizer": "262078c2-7052-7a19-baf0-649e374e99df",
  //     "tickets": [
  //       "77756e33-79e5-57c4-7b6b-e46a1ef07c20",
  //       "05fd5d03-996e-fcbf-45b7-01989030ed8f",
  //       "c8cff8bc-9710-4e09-58ec-f35727b33668"
  //     ],
  //     "ticketsSold": false,
  //     "earnings": 0,
  //     "attendees": []
  //   },
  //   {
  //     "_id": "0046oree-940f-e3cc-034f-23145",
  //     "__v": 0,
  //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
  //     "status": "published",
  //     "basicDetails": {
  //       "endTime": "2017-02-23T02:00:43.939Z",
  //       "startTime": "2017-02-22T22:00:36.730Z",
  //       "endDate": "2017-03-22T04:00:00.000Z",
  //       "startDate": "2017-03-22T04:00:00.000Z",
  //       "category": "Dances",
  //       "thumbnail": "",
  //       "image": "concert-image-2.jpeg",
  //       "venue": "Rileys Pub",
  //       "description": "DJ Something or Another spins hot tracks all night on the ones and twos, be prepared for the best concert ever.",
  //       "name": "DJ Something or Another"
  //     },
  //     "location": {
  //       "country": "Canada",
  //       "longitude": -78.8597891,
  //       "latitude": 43.898611,
  //       "postal": "L1H 1B6",
  //       "province": "Ontario",
  //       "city": "Oshawa",
  //       "address": "104 King Street East"
  //     },
  //     "organizer": "ac07e073-20b0-e0a8-55d3-04fe5a20a763",
  //     "tickets": [
  //       "750a0c2b-86c3-7bfb-b064-97ebeb5923b7"
  //     ],
  //     "ticketsSold": false,
  //     "earnings": 0,
  //     "attendees": []
  //   },  {
  //     "_id": "32effgh0f-e6c1-219b-534f-23145",
  //     "__v": 0,
  //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
  //     "status": "published",
  //     "basicDetails": {
  //       "endTime": "2017-02-22T16:00:59.966Z",
  //       "startTime": "2017-02-23T02:00:56.064Z",
  //       "endDate": "2017-03-03T05:00:00.000Z",
  //       "startDate": "2017-03-03T05:00:00.000Z",
  //       "category": "Dances",
  //       "thumbnail": "",
  //       "image": "create-event-small.jpeg",
  //       "venue": "Testing a venue name",
  //       "description": "This is a description for the above event this and that and some more",
  //       "name": "Testing an event and populating the site"
  //     },
  //     "location": {
  //       "country": "Canada",
  //       "longitude": -79.111807,
  //       "latitude": 43.812724,
  //       "postal": "L1W 3M3",
  //       "province": "Ontario",
  //       "city": "Pickering",
  //       "address": "775 Hampton Court"
  //     },
  //     "organizer": "700ce97b-50d9-b44a-e740-4dd257f9b541",
  //     "tickets": [
  //       "47835d12-74e5-1f3c-7f97-b537bf4c4d48"
  //     ],
  //     "ticketsSold": false,
  //     "earnings": 0,
  //     "attendees": []
  //   },
  //   {
  //     "_id": "ed49csdf5-2c0e-4613-d34f-23145",
  //     "__v": 0,
  //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
  //     "status": "published",
  //     "basicDetails": {
  //       "endTime": "2017-02-23T03:00:41.001Z",
  //       "startTime": "2017-02-22T23:00:35.359Z",
  //       "endDate": "2017-03-10T05:00:00.000Z",
  //       "startDate": "2017-03-10T05:00:00.000Z",
  //       "category": "Sports",
  //       "thumbnail": "",
  //       "image": "soccor-stadium.jpeg",
  //       "venue": "Some big soccer stadium",
  //       "description": "This is the second match up between these teams, Team 1 seeks revenge after a big loss during thier last meeting.",
  //       "name": "Team 1 vs Team 2"
  //     },
  //     "location": {
  //       "country": "Canada",
  //       "longitude": -79.38935320000002,
  //       "latitude": 43.6414378,
  //       "postal": "M5V 1J1",
  //       "province": "Ontario",
  //       "city": "Toronto",
  //       "address": "1 Blue Jays Way"
  //     },
  //     "organizer": "262078c2-7052-7a19-baf0-649e374e99df",
  //     "tickets": [
  //       "77756e33-79e5-57c4-7b6b-e46a1ef07c20",
  //       "05fd5d03-996e-fcbf-45b7-01989030ed8f",
  //       "c8cff8bc-9710-4e09-58ec-f35727b33668"
  //     ],
  //     "ticketsSold": false,
  //     "earnings": 0,
  //     "attendees": []
  //   },
  //   {
  //     "_id": "004626ee-9sdff-e3cc-034f-23145",
  //     "__v": 0,
  //     "owner": "9f29fd74-5749-af1f-a999-c9b26132174d",
  //     "status": "published",
  //     "basicDetails": {
  //       "endTime": "2017-02-23T02:00:43.939Z",
  //       "startTime": "2017-02-22T22:00:36.730Z",
  //       "endDate": "2017-03-22T04:00:00.000Z",
  //       "startDate": "2017-03-22T04:00:00.000Z",
  //       "category": "Dances",
  //       "thumbnail": "",
  //       "image": "concert-image-2.jpeg",
  //       "venue": "Rileys Pub",
  //       "description": "DJ Something or Another spins hot tracks all night on the ones and twos, be prepared for the best concert ever.",
  //       "name": "DJ Something or Another"
  //     },
  //     "location": {
  //       "country": "Canada",
  //       "longitude": -78.8597891,
  //       "latitude": 43.898611,
  //       "postal": "L1H 1B6",
  //       "province": "Ontario",
  //       "city": "Oshawa",
  //       "address": "104 King Street East"
  //     },
  //     "organizer": "ac07e073-20b0-e0a8-55d3-04fe5a20a763",
  //     "tickets": [
  //       "750a0c2b-86c3-7bfb-b064-97ebeb5923b7"
  //     ],
  //     "ticketsSold": false,
  //     "earnings": 0,
  //     "attendees": []
  //   }
  // ];

  // Event.insertMany(events);
})

export { router };
