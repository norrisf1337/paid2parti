import * as express from 'express';
import { default as Event } from '../models/model_event';
let router = express.Router();
let jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens


router.post('/', function(req, res) {
  Event.findOne({'_id':req.body.id}, function(err, event) {
    res.json(event);
  }).populate('tickets');
});

//Create an Event
router.post('/create', function(req, res) {



Event.findOneAndUpdate({'_id': req.body._id}, req.body, {upsert:true, new:true}, function(err, doc){
    if (err) return res.send(500, { error: err });

    doc.updateTickets(req.body.tickets, () => {
      res.json({ success: true, doc:doc });
    });
    //res.json({ success: true, doc:doc });
  });
});

//Create an Event
router.post('/remove', function(req, res) {
  Event.findOneAndRemove({'_id': req.body._id}, function(err, doc){
    if (err) return res.send(500, { error: err });
    res.json({ success: true});
  });
});


router.post('/ticket/remove', function(req, res) {

Event.findOneAndUpdate({'_id': req.body._id}, req.body,{'active':false}, function(err, doc){
    if (err) return res.send(500, { error: err });
  });
});

router.route('/update/:id')
.post(function(req, res) {
  console.log('id is', req.body.params);

  res.json({ success: true });

});


export { router };
