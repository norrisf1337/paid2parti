// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10;

const User = new mongoose.Schema({
  _id: String,
  credentials: {
    username: String,
    password: String,
    email: String,
    admin: Boolean,
  },
  personal: {
    firstName: String,
    lastName: String,
    city: String,
    country: String,
    interests: [String]
  },
  account: {
    depositAccount: String,
    refId: { type: String, ref: 'users' },
    fundsAvailable: [{ type: String, ref: 'funds' }],
    credits: { type: Number, default:0 },
  },
  filterPrefs: {
    Location: {
      lat: Number,
      long: Number
    }
  },
  events: [{ type: String, ref: 'event' }],
  favorites: [{ type: String, ref: 'event' }]
});

User.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('credentials.password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.credentials.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.credentials.password = hash;
            next();
        });
    });
});

User.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.credentials.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};


export default mongoose.model('users', User);

