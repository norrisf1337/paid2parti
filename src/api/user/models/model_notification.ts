// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const Notification = new mongoose.Schema({
  _id: String,
  recipient_id: String,
  sender_id: String,
  conversation: Boolean,
  conversation_id: String,
  messages: String,
  created: String,
  last_updated: String,
  type: String,
  read: Boolean
});

export default mongoose.model('notification', Notification);

