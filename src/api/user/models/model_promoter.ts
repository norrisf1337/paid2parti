// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const Promoter = new mongoose.Schema({
  _id: String,
  first: String,
  owner: String,
  last: String,
  email: String,
  description: String,
  phone: String,
  organization: String,
  affiliation: String,
  socialFb: String,
  socialTw: String,
  socialLi: String,
  socialG: String,
  socialIns: String
});

export default mongoose.model('promoter', Promoter);

