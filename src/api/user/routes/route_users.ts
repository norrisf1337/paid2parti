import * as express from 'express';
import { default as User } from '../models/model_user';
import { default as Promoter } from '../models/model_promoter';
import * as _ from 'lodash';
let router = express.Router();
let jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

router.post('/create/', function(req, res) {
    // create new user from request
    let user = new User(req.body);

    //save user
    user.save(function(err) {
    if (err) res.json({ success: false, error:'Something went wrong. Please contact a system administrator' });

    //set token
    var token = jwt.sign(user, 'robblekabobblehuh', {
      expiresIn: 360 // expires in 6 hours
    });

    res.json({ success: true, token:token });
  });
});

//Create a Promoter
router.post('/promoter/', function(req, res) {
  Promoter.find({'_id':req.body.id}, function(err, promoter){
    if (err) return res.json({ success: false, error:'Unable to retrive promoters' });
    res.json({ success: true, promoter: promoter });
  });
});


//Create a Promoter
router.post('/promoters/', function(req, res) {
  Promoter.find({'owner':req.body.uid}, function(err, promoters){
    if (err) return res.json({ success: false, error:'Unable to retrive promoters' });
    res.json({ success: true, promoter: promoters });
  });
});


//Create a Promoter
router.post('/promoters/create', function(req, res) {
  Promoter.findOneAndUpdate({'_id': req.body._id}, req.body, {upsert:true}, function(err, doc){
    if (err) return res.json({ success: false, error:'Unable to create promoter' });
    res.json({ success: true });
  });
});

export { router };
