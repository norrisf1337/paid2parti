
import * as express from 'express';
import * as _ from 'lodash';
import * as moment from 'moment';
let router = express.Router();
let jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
import { default as User } from '../models/model_user';
import { default as Transactions } from '../../funds/models/model_funds';
import { default as Notification } from '../models/model_notification';
import * as Promise from 'bluebird';
import { PartiMailer } from '../../../backend/mailer/mailer';



router.post('/', function(req, res) {
    User.find({ "credentials.username": req.body.email }, function(err, user) {
        res.json(user);
    });
});

router.post('/details', function(req, res) {
    console.log(req.body);
    User.findOne({ '_id': req.body.uid }, req.body.details, function(err, docs) {
        console.log(docs);
        res.json(docs);
    });
});

router.post('/details/update', function(req, res) {
    console.log(req.body);
    User.findOneAndUpdate({ '_id': req.body.uid }, req.body.details, function(err, doc) {
        if (err) return res.json({ success: false, error: 'Unable to update details. Please try again later.' });
        res.json({ success: true });
    });
});

router.post('/password', function(req, res) {
    User.findOne({
        "_id": req.body.uid
    }, function(err, user) {

        if (err) throw err;

        if (!user) {
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        } else if (user) {

            // check if password matches
            user.comparePassword(req.body.details.previousPass, function(err, isMatch) {
                if (err) res.json({ success: false, message: 'Authentication failed. Wrong password.' });
                user.credentials.password = req.body.details.newPass;
                user.save(function(err) {
                    if (err) res.json({ success: false, error: 'Something went wrong. Please contact a system administrator' });

                    res.json({ success: true });
                });


            });
        }

    });
});

router.post('/favorites/set', function(req, res) {
    User.findOneAndUpdate(
        { '_id': req.body.uid },
        { $push: { "favorites": req.body.eid } },
        { safe: true, upsert: true },
        function(err, favorites) {
            if (err) res.json({ sucess: false, error: err });
            res.json({ sucess: true, message: 'Event has been saved to favorites' });
        }
    )
});

router.post('/favorites/remove', function(req, res) {
    User.findOneAndUpdate(
        { '_id': req.body.uid },
        { $pull: { "favorites": req.body.eid } },
        function(err, favorites) {
            if (err) res.json({ sucess: false, error: 'Unable to remove favorite' });
            res.json({ sucess: true, message: 'Saved Item has been removed' });
        }
    )
});

router.post('/favorites/get', function(req, res) {
    console.log(req.body.uid);
    let limit = req.body.limit || 0;
    User.findOne({ "_id": req.body.uid }, {}, { limit: limit }, function(err, favorites) {
        console.log(favorites)
        res.json({ success: true, favorites: favorites.favorites });
    }).populate('favorites');
});

router.post('/notifications', function(req, res) {
    let limit = req.body.limit || 0;
    Notification.find({ "recipient_id": req.body.uid }, {}, { limit: limit }, function(err, notifications) {
        res.json({ success: true, notifications: notifications });
    });
});

router.post('/notifications/create', function(req: any, res: any) {
    console.log(req.body);
    let note = new Notification(req.body.notification);

    //save user
    note.save(function(err) {
        if (err) res.json({ success: false, error: 'Something went wrong. Please contact a system administrator' });
    });

});

router.post('/notifications/mark', function(req: any, res: any) {
    // Notification.findOneAndUpdate({ "id": req.body.nid }, { $set: { read: true } }, function(err, notification) {
    //     res.json({ success: true, status:'updated' });
    // });
});

router.post('/notifications/delete', function(req: any, res: any) {
    // Notification.remove({ "id": req.body.nid }, function(err, notification) {
    //     res.json({ success: true, status:'updated' });
    // });
});

router.post('/funds', function(req: any, res: any) {
    let promises = [];
    let funds = {
        available: 0,
        holding: 0,
    }

    let credit = null;
    let funding = Transactions.find({ "owner": req.body.uid }, 'transaction_type date funds.user eventId', (err, transactions) => {

        _.forEach(transactions, (o, i) => {
            if (moment(o.eventId.basicDetails.endDate).add(7, 'days').format('MM-DD-YYYY') <= moment(new Date).format('MM-DD-YYYY')) {
                funds.available += o.funds.user;
            } else {
                funds.holding += o.funds.user;
            }
        });

        funds.holding = parseFloat(funds.holding.toFixed(2));
        funds.available = parseFloat(funds.available.toFixed(2));


    }).populate('eventId', 'basicDetails.endDate');

    promises.push(funding);

    let credits = User.findOne({ "_id": req.body.uid }, 'account.credits', (err, cred) => {
        credit = cred.account.credits;
        console.log(cred);
    })

    promises.push(credits);

    Promise.all(promises).then(() => {
        res.json({ success: true, transactions: funds, credits: credit });
    });


});

router.post('/invite', function(req, res) {
    _.forEach(req.body.emails, (o,i) => {
        let mailer = new PartiMailer(o, 'userInvite', {uid: req.body.uid});
    });

    res.json({ success: req.body});
});


export { router };
