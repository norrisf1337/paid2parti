import * as express from 'express';
import { Ticket } from '../models/model_tickets';
import { default as User } from '../../user/models/model_user';
import { default as Transaction } from '../../funds/models/model_funds';
import { default as Event } from '../../events/models/model_event';
import { default as EventTicket } from '../../events/models/model_eventTicket';
import * as _ from 'lodash';
let router = express.Router();
let jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
let Promise = require("bluebird");
router.post('/', function(req, res) {
  let limit = req.body.limit || 0;
  Ticket.find({ 'client': req.body.uid }, {}, { limit: limit }, function(err, tickets) {
    res.json({ sucess: true, tickets: tickets });
  }).populate('event').populate('ticket')

});

router.post('/purchased', function(req, res) {
  Ticket.find({ 'event': req.body.eventId, }, function(err, tickets) {
    res.json({ sucess: true, tickets: tickets });
  }).populate('event').populate('ticket');

});

router.post('/add', function(req, res) {
  let tickets = [];
  let funds = [];
  let qty = req.body.qty;
  let errors = [];
  console.log(typeof qty);

  let calcCredits = (cost) => {
    let b = (cost / 100) * 2;
    return b;
  };

  let calcFunds = (cost) => {
    return {
      p2p: (cost / 100) * 5,
      user: cost - (cost / 100) * 2
    }
  };

  for (let i = 0; i < qty; i++) {

    let ticket = new Ticket(req.body);
    let tickett = ticket.save(function(err) {
      if (err) errors.push({'ticket': err});
    });

    tickets.push(tickett);

    let transaction = new Transaction({
      transaction_type: 'Deposit',
      client: req.body.client,
      owner: req.body.owner,
      eventId: req.body.event,
      ticketId: req.body.ticket,
      creditsGiven: calcCredits(req.body.cost),
      funds: calcFunds(req.body.cost)
    });

    let transactions = transaction.save(function(err) {
      if (err) errors.push({'transaction': err});
    });

    tickets.push(transactions);
  }

  let updateTicket = EventTicket.update({'_id': req.body.ticket },
    { $inc: { 'available': -Math.abs(req.body.qty), sold: Math.abs(req.body.qty) } }, function(err, doc) {
      if (err) errors.push({'tUpdate': err});
    });

  tickets.push(updateTicket);

   let updateUser = User.update({'_id': req.body.client },
    { $inc: { 'account.credits': Math.abs(calcCredits(req.body.cost * req.body.qty)) } }, function(err, doc) {
      if (err) errors.push({'user': err});
    });

  tickets.push(updateUser);


  Promise.all(tickets).then(() => {
    if (!_.isEmpty(errors)) {
      res.json({ sucess: false, err: errors });
    } else {
      res.json({ sucess: true });
    }

  });
});

router.post('/test', (req, res) => {
  let ref = req.body.ref;
  let cost = req.body.cost;

  User.find({'_id':req.body.ref}, (err, user) => {

  });
})

export { router };
