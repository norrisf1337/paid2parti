// get an instance of mongoose and mongoose.Schema
import * as moment from 'moment';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const TicketModel = new mongoose.Schema({
	client: [{ type: String, ref: 'users' }],
	event: [{ type: String, ref: 'event' }],
	ticket: [{ type: String, ref: 'eventTickets' }],
	qty: Number,
	date: { type: String, default: moment(new Date).format('MM-DD-YYYY') }
});

const Ticket = mongoose.model('tickets', TicketModel);

export { Ticket };
