import * as moment from 'moment';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const Transaction = new mongoose.Schema({
	transaction_type: String,
	client: { type: String, ref: 'users' },
	owner: { type: String, ref: 'users' },
	eventId: { type: String, ref: 'event' },
	ticketId: { type: String, ref: 'eventTickets' },
	creditsGiven: Number,
	funds: {
		p2p: Number,
		user: Number
	},
	date: { type: String, default: moment(new Date).format('MM-DD-YYYY') }
});

export default mongoose.model('transaction', Transaction);

