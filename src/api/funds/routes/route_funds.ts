import * as express from 'express';
import { Ticket } from '../../tickets/models/model_tickets';
import { default as Event } from '../../events/models/model_event';
import * as _ from 'lodash';
let router = express.Router();
let jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
router.post('/', function(req, res) {
  let limit = req.body.limit || 0;
  Ticket.find({'client':req.body.uid}, {}, {limit:limit}, function(err, tickets) {
    res.json({sucess: true, tickets:tickets});
  });

});

router.post('/purchased', function(req, res) {
Ticket.find({'eventId':req.body.eventId, }, function(err, tickets) {
    res.json({sucess: true, tickets:tickets});
  });

});

router.post('/add', function(req, res) {
  console.log('hit');
  // create new user from request

  let ticket = new Ticket(req.body.tickets);
    //save user
    ticket.save(function(err) {
      if (err) res.json({ success: false, error: 'Something went wrong. Please contact a system administrator' });
  });

  let tid = req.body.tickets.ticketId;

  Event.update({'id': req.body.tickets.eventId, 'tickets.id': tid},
    {$inc: {'tickets.$.available': -req.body.tickets.ticketDetails.length}}, function (err, doc){
      if (err) console.log(err);
      res.json({ success: true });
    });

});

export { router };
