import * as express from 'express';
import * as _ from 'lodash';
import { PartiMailer } from '../../../backend/mailer/mailer';
let router = express.Router();

router.post('/', function(req, res) {
    let mailer = new PartiMailer('', 'contact', req.body.data);
    res.json({ success: req.body});
});

export { router };
