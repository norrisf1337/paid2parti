import * as express from 'express';
import * as _ from 'lodash';
import { PartiMailer } from '../../../backend/mailer/mailer';
import { default as Comment } from '../models/model_comment';
import { default as Rating } from '../models/model_rating';
let router = express.Router();

router.post('/comments', function(req, res, next) {
	let limit = req.body.limit || 0;
	let offset = req.body.offset || 0;
	Comment.find({ 'promoterId': req.body.promoterId }, {}, { limit: limit, skip: offset, sort: { date: -1 } }, function(err, comments) {
		if (err) {
			return next(err);
		}

		Comment.count({}, function(err, count) {
			if (err) {
				return next(err);
			}

			res.send({ count: count, comments: comments });
		});
	});
});

router.post('/comments/add', function(req, res) {
	let comment = new Comment(req.body.data);
	comment.save(function(err) {
		if (err) res.json({ success: false, error: 'Something went wrong. Please contact a system administrator' });
		res.json({ success: true });
	});

});

router.post('/ratings', function(req, res) {
	console.log(req.body);
	Rating.find({ 'promoterId': req.body.promoterId }, function(err, ratings) {
		if (err) return res.send(500, { error: err });
		let rating = 0;
		console.log('rate', ratings)
		if (!_.isEmpty(ratings)) {
			ratings.map((x) => {
				rating += x.rating;
			});

			rating = rating / ratings.length;
		}

		res.json({ success: true, rating: rating });
	});
});

router.post('/rate', function(req, res) {
	Rating.findOneAndUpdate({ 'promoterId': req.body.promoterId, userId: req.body.userId }, req.body, { upsert: true }, function(err, doc) {
		if (err) return res.send(500, { error: err });

		res.json({ success: true, doc:doc });
	});
});

export { router };
