import * as moment from 'moment';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const Comment = new mongoose.Schema({
	promoterId: String,
	name: String,
	email: String,
	message: String,
	date: { type: String, default: moment(new Date).format('MM-DD-YYYY') }
});

export default mongoose.model('comment', Comment);

