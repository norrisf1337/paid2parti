import * as moment from 'moment';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const Rating = new mongoose.Schema({
	promoterId: String,
	userId: String,
	rating: Number,
	date: { type: String, default: moment(new Date).format('MM-DD-YYYY') }
});

export default mongoose.model('rating', Rating);

