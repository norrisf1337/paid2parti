import * as express from 'express';

/*Models*/
import { default as User } from './user/models/model_user';

/*Routes*/
import { router as userRoutes } from './user/routes/route_user';
import { router as usersRoutes } from './user/routes/route_users';
import { router as eventsRoutes } from './events/routes/route_events';
import { router as eventRoutes } from './events/routes/route_event';
import { router as ticketsRoutes } from './tickets/routes/route_tickets';
import { router as contactRoutes } from './contact/routes/route_contact';
import { router as promoterRoutes } from './promoter/routes/route_promoter';


let router = express.Router();
let jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

router.post('/authenticate', function(req, res) {
  console.log(req.body.email);
  // find the user
  User.findOne({
    "credentials.username": req.body.email
  }, function(err, user) {

    if (err) throw err;

    if (!user) {
      res.json({ success: false, message: 'Authentication failed. User not found.' });
    } else if (user) {

      // check if password matches
      user.comparePassword(req.body.password, function(err, isMatch) {
        if (err) res.json({ success: false, message: 'Authentication failed. Wrong password.' });
        console.log(req.body.password, isMatch); // -> Password123: true


        // if user is found and password is right
        // create a token
        var token = jwt.sign(user, 'robblekabobblehuh', {
          expiresIn: 360 // expires in 6 hours
        });

        // return the information including token as JSON
        res.json({
          success: true,
          message: 'Enjoy your token!',
          token: token,
          user: {
            id: user._id,
            refId: user.account.refId
          }
        });

      });
    }

  });
});

// router.use(function(req, res, next) {

//   // check header or url parameters or post parameters for token
//   var token = req.body.token || req.query.token || req.headers['x-access-token'];

//   // decode token
//   if (token) {

//     // verifies secret and checks exp
//     jwt.verify(token,'robblekabobblehuh', function(err, decoded) {
//       if (err) {
//         return res.json({ success: false, message: 'Failed to authenticate token.' });
//       } else {
//         // if everything is good, save to request for use in other routes
//         req['decoded'] = decoded;
//         next();
//       }
//     });

//   } else {

//     // if there is no token
//     // return an error
//     return res.status(403).send({
//         success: false,
//         message: 'No token provided.'
//     });

//   }
// });

router.use('/user', userRoutes);
router.use('/users', usersRoutes);
router.use('/events', eventsRoutes);
router.use('/event', eventRoutes);
router.use('/tickets', ticketsRoutes);
router.use('/contact', contactRoutes);
router.use('/promoter', promoterRoutes);
export { router };
