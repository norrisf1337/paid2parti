/*
 * Angular bootstraping
 */
import { platformBrowser } from '@angular/platform-browser';
import { decorateModuleRef } from './app/environment';
/*
 * App Module
 * our top level module that holds all of our components
 */
import { AppModuleNgFactory } from '../compiled/src/app/app.module.ngfactory';
/*
 * Bootstrap our Angular app with a top level NgModule
 */
export function main() {
    return platformBrowser()
        .bootstrapModuleFactory(AppModuleNgFactory)
        .then(decorateModuleRef)
        .catch(function (err) { return console.error(err); });
}
export function bootstrapDomReady() {
    document.addEventListener('DOMContentLoaded', main);
}
bootstrapDomReady();
//# sourceMappingURL=main.browser.aot.js.map